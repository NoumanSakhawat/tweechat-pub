import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const WIDTH = Dimensions.get('window').width;

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    flatList: {
        marginTop: 45,
    },
    contentContainerStyle: {

    },
    versionText: {
        fontSize: 16,
        color: "#898A8D",
        textAlign: "center",
        marginBottom: 16,
    },

});//end of styles

export const itemStyles = StyleSheet.create({
    primaryContainer: {
        paddingHorizontal: AppStyles.HORIZONTAL,
        paddingBottom: 36,
        // flexDirection: 'row',
        // alignItems: 'center',
    },
    text: {
        fontSize: 18,
        color: "#0F1117",
    },
    rightIconContainer: {
        position: "absolute",
        right: AppStyles.HORIZONTAL,
        alignItems: "center",
        justifyContent: "center",
    },

});//end of styles


export const headerStyles = StyleSheet.create({

    primaryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    leftContainer: {
        paddingLeft: AppStyles.HORIZONTAL / 2,
        paddingRight: AppStyles.HORIZONTAL / 2,
        flex: 1,
    },
    imageBack: {
        height: 24,
        width: 24,
    },
    titleContainer: {
        flex: 2,
    },
    title: {
        textAlign: 'left',
        fontSize: 20,
        color: "#0F1117",
        fontWeight: "bold",
    },
    rightContainer: {
        flex: 0,
        paddingRight: AppStyles.HORIZONTAL / 2,
    },


});//end of headerStyles