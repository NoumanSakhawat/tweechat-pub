import { CommonActions, StackActions } from '@react-navigation/native';
import React, { useEffect, useState, useRef, createRef } from 'react';
import { Keyboard, TouchableOpacity } from 'react-native';
import { Dimensions, FlatList, KeyboardAvoidingView, ScrollView, View, TextInput as RNTextInput, } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import FastImage from 'react-native-fast-image';
import Button from '../../components/Button';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import LocalAssets from '../../constants/LocalAssets';
import StaticData from '../../constants/StaticData';
import HelperFunctions, { emptyValidate, printText } from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import ROUTES from '../../routes/ROUTES';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { styles, headerStyles, itemStyles } from './styles';


const index = ({ navigation, route }) => {
    const [data,] = useState(StaticData.settingsData())
    const [version, setVersion] = useState('1.00')

    useEffect(async () => {
        const versionRes = DeviceInfo.getVersion();
        const buildNumber = DeviceInfo.getBuildNumber();


        setVersion(`${versionRes}${buildNumber}`);
    }, [])

    const _renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={itemStyles.primaryContainer}
                disabled>
                <Text style={itemStyles.text}>{item.name}</Text>

                <View style={itemStyles.rightIconContainer}>
                    <VectorIcon
                        name={"chevron-right"}
                        iconType={"MaterialCommunityIcons"}
                        color={"#898A8D"}
                        size={20} />
                </View>
            </TouchableOpacity>
        )
    };//end of _renderItem

    return (
        <View style={styles.primaryContainer}>
            <TweeHeader customHeader={() => _renderHeader(navigation)}
                bottomBorder={false} />

            <FlatList
                data={data}
                style={styles.flatList}
                contentContainerStyle={styles.contentContainerStyle}
                renderItem={_renderItem} />


            <Text style={styles.versionText}>{`${IMLocalized(`Tweechat version`)} ${version}`}</Text>
        </View>
    );
}//end of index

export default index;

const _renderHeader = (navigation) => {
    return (
        <View style={headerStyles.primaryContainer}>
            <TouchableOpacity onPress={() => {
                navigation.dispatch(CommonActions.goBack());
            }}
                style={headerStyles.leftContainer}>
                <FastImage
                    source={LocalAssets.ICON.videoPlayBack}
                    style={headerStyles.imageBack}
                    resizeMode={FastImage.resizeMode.contain} />
            </TouchableOpacity>

            <View style={headerStyles.titleContainer}>
                <Text style={headerStyles.title}>{IMLocalized(`Settings`)}</Text>
            </View>

            <View style={headerStyles.rightContainer} >
            </View>
        </View>
    )
};//end of _renderHeader
