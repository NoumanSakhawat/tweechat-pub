import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const WIDTH = Dimensions.get('window').width;

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
        alignItems: "center",
        justifyContent: "center",
    },
    onboardingImage: {
        height: WIDTH / 2,
        width: WIDTH - 80,
        alignSelf: "center",
    },

    heading: {
        color: "#0F1117",
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 40,
        paddingHorizontal: 20,
    },
    termPolicy: {
        color: "#606268",
        fontSize: 16,
        textAlign: "center",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 80,
    },
    continueButtonContainer: {
        marginTop: 24,
        marginHorizontal: AppStyles.HORIZONTAL,
    },
    continueButton: {
        marginTop: 24,
        marginHorizontal: AppStyles.HORIZONTAL,
        alignSelf: "center",
    },
    restoreButton: {
        marginTop: 8,
        marginHorizontal: AppStyles.HORIZONTAL,
        alignSelf: "center",

    },
    restoreButtonText: {
        textTransform: "capitalize",
        fontWeight: "700",
    },
});//end of styles