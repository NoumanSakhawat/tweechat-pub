import React from 'react';
import { View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Button from '../../components/Button';
import Text from '../../components/Text';
import colors from '../../constants/colors';
import LocalAssets from '../../constants/LocalAssets';
import { IMLocalized } from '../../locales/IMLocalization';
import ROUTES from '../../routes/ROUTES';
import { styles } from './styles';

const index = ({ navigation }) => {
    return (
        <View style={styles.primaryContainer}>
            <FastImage
                source={LocalAssets.ICON.onboarding}
                style={styles.onboardingImage}
                resizeMode={FastImage.resizeMode.contain} />

            <Text style={styles.heading}>{IMLocalized(`Take privacy with you.\n Be yourself in every message.`)}</Text>

            <Text style={styles.termPolicy}>{IMLocalized(`Terms & Privacy Policy`)}</Text>

            <Button
                text={IMLocalized(`Continue`)}
                containerStyle={styles.continueButton}
                width={"90%"}
                onPress={() => {
                    navigation.navigate(ROUTES.EnterNumber);
                    // navigation.navigate(ROUTES.Home);
                }}
            />

            <Button
                text={IMLocalized(`Restore backup`)}
                textStyle={styles.restoreButtonText}
                containerStyle={styles.restoreButton}
                width={"90%"}
                backgroundColor={'transparent'}
                textColor={colors.primary}
            />

        </View>
    );
}//end of index

export default index;