import { StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';


export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },


});//end of styles

export const itemStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
    },
    body: {
        flexDirection: 'row',
        paddingLeft: AppStyles.HORIZONTAL,
        paddingVertical: 20,
    },
    profilePictureContainer: {
        marginRight: 16,
    },
    textContainer: {
        paddingRight: AppStyles.HORIZONTAL * 3,
    },
    text: {
        color: "#2F3137",
        fontSize: 16,

    },

    timeago: {
        color: "rgba(15, 17, 23, 0.5)",
        fontSize: 12,
        marginTop: 4,

    },

    border: {
        ...AppStyles.border,
    },
});//end of itemStyles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    leftContainer: {
        paddingLeft: AppStyles.HORIZONTAL / 2,
        paddingRight: AppStyles.HORIZONTAL / 2,
        flex: 1,
    },
    leftIcon: {

    },
    titleContainer: {
        flex: 2,
    },
    title: {
        textAlign: 'left',
        fontSize: 20,
        color: "#0F1117",
        fontWeight: "bold",
    },
    rightContainer: {
        flex: 0,
        paddingRight: AppStyles.HORIZONTAL / 2,
    },

});//end of headerStyles

