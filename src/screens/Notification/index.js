import React, { useState } from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import StaticData from '../../constants/StaticData';
import HelperFunctions, { emptyValidate } from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { headerStyles, itemStyles, styles } from './styles';


const index = ({ navigation }) => {

    const [data, setData] = useState(StaticData.notifications());
    const [metadata, setMetadata] = useState(false);


    const _renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={[itemStyles.primaryContainer,
            item.isRead && {
                backgroundColor: "#EDF5FF",
            }
            ]}>
                <View style={itemStyles.body}>
                    <View style={itemStyles.profilePictureContainer}>
                        <ProfilePicture
                            source={item.user.profile}
                            hasBorder
                            size={34} />
                    </View>
                    <View style={itemStyles.textContainer}>
                        <Text style={itemStyles.text}>{item.body}</Text>

                        {emptyValidate(item.timeago) &&
                            <Text style={itemStyles.timeago}>{HelperFunctions.timeSince(item.timeago)}</Text>
                        }
                    </View>
                </View>

                <View style={itemStyles.border} />
            </TouchableOpacity>
        )
    };//end of _renderItem

    return (
        <View style={styles.primaryContainer}>
            <TweeHeader customHeader={() => _renderHeader(navigation)} bottomBorder />

            <FlatList
                data={data}
                extraData={metadata}
                keyExtractor={HelperFunctions.keyExtractor}
                renderItem={_renderItem} />

        </View>
    );
}//end of index

export default index;


const _renderHeader = (navigation) => {
    return (
        <View style={headerStyles.primaryContainer}>
            <TouchableOpacity onPress={() => {
                navigation.openDrawer();
            }}
                style={headerStyles.leftContainer}>
                <VectorIcon
                    name={"menu"}
                    size={30}
                    color={colors.primary}
                    style={headerStyles.leftIcon}
                />
            </TouchableOpacity>

            <View style={headerStyles.titleContainer}>
                <Text style={headerStyles.title}>{IMLocalized(`Notifications`)}</Text>
            </View>

            <View style={headerStyles.rightContainer} >
            </View>
        </View>
    )
};//end of _renderHeader