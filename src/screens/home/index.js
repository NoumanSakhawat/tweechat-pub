import React, { useEffect, useState } from 'react';
import { FlatList, RefreshControl, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Text from '../../components/Text';
import TweeHeader from '../../tweeComponents/TweeHeader';
import Stories from '../../UIComponents/Stories';
import Post from '../../UIComponents/Post';
import { styles } from './styles';
import ActionButton from 'react-native-action-button';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import ROUTES from '../../routes/ROUTES';
import StaticData from '../../constants/StaticData';
import HelperFunctions from '../../helper/HelperFunctions';
import AppStyles from '../../constants/AppStyles';

const index = ({ navigation }) => {

    const [data, setData] = useState([]);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        load();
    }, []);

    const load = () => {
        setRefreshing(true);
        const data = StaticData.post(10);
        setData(data);
        setRefreshing(false);
    };//end of load

    const onActionButtonPress = () => {
        navigation.navigate(ROUTES.AddPost)
    };//end of onActionButtonPress

    const postDetailPress = (item, index) => {
        navigation.navigate({
            name: ROUTES.CommentView,
            params: { post: item, postIndex: index },
        })
    };//end of postDetailPress

    return (
        <View style={styles.primaryContainer}>
            <TweeHeader
                menu
                menuPress={() => {
                    navigation.openDrawer();
                }}
                bottomBorder
            />
            <ScrollView nestedScrollEnabled={true}
                refreshControl={<RefreshControl onRefresh={load} refreshing={refreshing} />}>
                <Stories />

                <FlatList
                    data={data}
                    renderItem={({ item, index }) => {

                        return (
                            // <TouchableOpacity onPress={() => { postDetailPress(item, index) }}
                            //     onPressIn={() => { postDetailPress(item, index) }}
                            //     onPressOut={() => { postDetailPress(item, index) }}
                            //     activeOpacity={1}
                            // >
                            <Post
                                onUserProfilePress={(item) => {
                                    navigation.navigate(ROUTES.UserProfile)
                                }}
                                navigation={navigation}
                                item={item}
                                index={index}
                                viewAllCommentPress={() => { postDetailPress(item, index) }}
                                onPostPress={() => { postDetailPress(item, index) }}
                                onMediaItemPress={(isMultiple, multimediaItemIndex) => {
                                    const mulData = HelperFunctions.array_move(item.multimedia, multimediaItemIndex, 0);
                                    navigation.navigate({
                                        name: ROUTES.VideoPlay,
                                        params: {
                                            isMultiple,
                                            data: mulData,
                                            item,
                                            index
                                        }
                                    })
                                }}
                            />
                            // </TouchableOpacity>
                            // _renderItem(item, index, navigation)
                        )
                    }}
                    scrollEnabled={false}
                    nestedScrollEnabled={true}
                    contentContainerStyle={styles.contentContainerStyle}
                />


            </ScrollView>

            <ActionButton
                buttonColor={colors.primary}
                style={styles.actionButton}
                offsetX={AppStyles.HORIZONTAL}
                offsetY={16}
                onPress={onActionButtonPress}
                renderIcon={() => {
                    return (
                        <VectorIcon
                            name={"md-add-outline"}
                            size={24}
                            color={colors.white}
                        />
                    )
                }}
            />
        </View>
    )
}//end of index

export default index;