import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const WIDTH = Dimensions.get('window').width;

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    body: {
        flex: 1,
    },
    heading: {
        color: "#0F1117",
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 40,
        paddingHorizontal: 20,
    },

    changeNumberButton: {
        marginTop: 8,
        marginHorizontal: AppStyles.HORIZONTAL,
        alignSelf: "center",
        marginBottom: 0,
    },
    changeNumberButtonText: {
        textTransform: "capitalize",
        fontWeight: "700",
    },


    //CODE DATA FLATLIST
    codeFlatlist: {
        paddingHorizontal: AppStyles.HORIZONTAL,
        paddingLeft: AppStyles.HORIZONTAL / 2,
        maxWidth: WIDTH,
        alignSelf: "center",
        flexGrow: 0,
    },
    codeContentContainerStyle: {
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    //TEXT INPUT

    containerTextinput: {
        paddingVertical: 7,
        paddingHorizontal: 8,
        minHeight: 48,
        marginTop: 24,
    },
    inputContainerStyle: {
        borderColor: "#A7AAB1",
        borderWidth: 1,
        borderRadius: 8,
    },
    textinput: {
        fontWeight: "bold",
        fontSize: 24,
        textAlign: "center",
    },


    //RESEND CODE BUTTON
    resendCodeButton: {
        marginTop: AppStyles.HORIZONTAL,
        marginHorizontal: AppStyles.HORIZONTAL,
        alignSelf: "center",
        marginBottom: 0,
    },
    redendCodeButtonText: {
        textTransform: "capitalize",
        fontWeight: "400",

    },


    //BOTTOM BUTTON

    buttonContainer: {
        marginTop: 24,
        marginHorizontal: AppStyles.HORIZONTAL,
        marginBottom: 16,

        position: "absolute",
        bottom: 0,
        // width: "88%",

        width: Dimensions.get("screen").width - (AppStyles.HORIZONTAL * 2),
    },
    button: {
        marginHorizontal: AppStyles.HORIZONTAL,
        alignSelf: "center",
    },

});//end of styles