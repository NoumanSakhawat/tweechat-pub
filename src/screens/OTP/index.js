import React, { useEffect, useState, useRef, createRef } from 'react';
import { Keyboard } from 'react-native';
import { Dimensions, FlatList, KeyboardAvoidingView, ScrollView, View, TextInput as RNTextInput, } from 'react-native';
import Button from '../../components/Button';
import Text from '../../components/Text';
import colors from '../../constants/colors';
import HelperFunctions, { emptyValidate, printText } from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import ROUTES from '../../routes/ROUTES';
import { styles } from './styles';

const CODE_LENGTH = 6;

const WIDTH = Dimensions.get('window').width;
const BOX_SIZE = (WIDTH / (CODE_LENGTH)) - 15;

const index = ({ navigation, route }) => {
    const formattedNumber = route.params.formattedNumber;

    const [codeData, setCodeData] = useState([]);
    const [codeMetaData, setCodeMetaData] = useState(false);

    const [isFilled, setIsFilled] = useState(false);

    const [selectedIndex, setSelectedIndex] = useState(0);

    useEffect(async () => {
        const newCd = [];
        for (let i = 0; i < CODE_LENGTH; i++) {
            const obj = { id: i + 1 };
            obj[`input`] = '';
            obj[`inputError`] = '';
            obj[`enable`] = i === 0 ? true : false;
            obj[`ref`] = createRef();
            newCd.push(obj)
        }
        setCodeData(newCd);
        setCodeMetaData(!codeMetaData);

        await HelperFunctions.sleep(0.1);
        newCd[0].ref.current.focus();
    }, [])

    const formatText = (text) => {
        return text.replace(/[^+\d]/g, '');
    };//end of formatText

    const changeEditable = async (arr, index) => {

        let newCd = codeData;
        newCd[index].input = '';
        newCd[index].inputError = '';




        for (let i = 0; i < newCd.length; i++) {
            if (newCd[i].input === '') {
                newCd[i].enable = false;
            }
        };

        setCodeData(newCd);
        setCodeMetaData(!codeMetaData);

        let newCd1 = codeData;
        for (let i = 0; i < newCd1.length; i++) {
            if (i > 0) {
                if (newCd1[i - 1].input !== '') {
                    newCd1[i].enable = true;
                }
            }

        };
        setCodeData(newCd1);
        setCodeMetaData(!codeMetaData);


        // console.log('newCd1 ', newCd1);
    };//end of changeEditable

    const onKeyPress = async (nativeEvent, index) => {

        if (nativeEvent.key === 'Backspace') {
            //do action
            if (index !== 0) {

                await changeEditable([], index)

                // let newCd = codeData;
                // newCd[index].input = '';
                // newCd[index].inputError = '';
                // newCd = ;

                // setCodeData(newCd);
                // setCodeMetaData(!codeMetaData);



                setSelectedIndex(index - 1);
                await HelperFunctions.sleep(0.000001);

                codeData[index - 1].ref.current.focus();





            }
            changeText('', index)
            return
        }
        //other action
        if (!formatText(nativeEvent.key)) {
            return;
        }
        changeText(nativeEvent.key, index);
        sIndex = index + 1;

    };//end of onKeyPress

    const onFocus = (nativeEvent, index) => {

        setSelectedIndex(index);
        // for (let i = 0; i < codeData.length; i++) {
        //     if (!emptyValidate(codeData[i].input)) {
        //         codeData[i].ref.current.focus();
        //         break;
        //     }
        // }//end of LOOP

    };//end of onFocus

    const checkIsAllFilled = () => {
        let isF = true;
        for (let i = 0; i < codeData.length; i++) {
            if (!emptyValidate(codeData[i].input)) {
                // codeData[i].ref.current.focus();
                isF = false;
                break;
            }
        }

        setIsFilled(isF);
    };//end of checkIsAllFilled

    const changeText = async (text, index) => {

        const newCd = codeData;
        newCd[index].input = text;
        newCd[index].inputError = '';
        if (index < CODE_LENGTH - 1)
            newCd[index + 1].enable = true;
        setCodeData(newCd);

        setCodeMetaData(!codeMetaData);

        if (index !== CODE_LENGTH - 1) {
            if (emptyValidate(text)) {
                setSelectedIndex(index + 1);
                await HelperFunctions.sleep(0.000001);
                codeData[index + 1].ref.current.focus();
            }
        }
        checkIsAllFilled();


    };//end of changeText

    const sendCodeButtonPress = () => {

    };//end of sendCodeButtonPress

    const onContinuePress = () => {
        navigation.replace(ROUTES.SetupProfile);
    };//end of onContinuePress

    return (
        <View style={styles.primaryContainer}>

            <KeyboardAvoidingView style={{ flex: 1, }} behavior="padding" enabled>
                <ScrollView contentContainerStyle={{ flex: 1, }} scrollEnabled={false} nestedScrollEnabled keyboardDismissMode='interactive'
                    keyboardShouldPersistTaps='handled'>

                    <View style={styles.body}>

                        <Text style={styles.heading}>{`${IMLocalized(`Enter the code we sent to `)}\n${printText(formattedNumber)}`}</Text>

                        <Button
                            text={IMLocalized(`Change Phone Number`)}
                            textStyle={styles.changeNumberButtonText}
                            containerStyle={styles.changeNumberButton}
                            width={"90%"}
                            backgroundColor={'transparent'}
                            textColor={colors.primary}
                            onPress={() => { navigation.pop() && navigation.goBack() }}
                        />

                        <FlatList
                            data={codeData}
                            extraData={codeMetaData}
                            horizontal
                            scrollEnabled={false}
                            style={styles.codeFlatlist}
                            contentContainerStyle={styles.codeContentContainerStyle}
                            renderItem={({ item, index }) => {
                                // console.log(`Index is==> ${index} Status==> ${item.enable}`);
                                return (
                                    <View style={[styles.containerTextinput,
                                    styles.inputContainerStyle, {
                                        width: BOX_SIZE,
                                        marginLeft: index !== CODE_LENGTH ? 8 : 0,
                                        borderColor: index === selectedIndex ? colors.primary : "#A7AAB1",
                                    }]}>
                                        <RNTextInput
                                            ref={item.ref}
                                            value={item.input}
                                            // editable={editEnable(item,index,nextEnable)}
                                            editable={item.enable}
                                            // onChangeText={(text) => { changeText(text, index) }}
                                            baseColor={'#A7AAB1'}
                                            textColor={'#0F1117'}
                                            labelFontSize={12}
                                            lineType={"none"}
                                            maxLength={1}
                                            selectTextOnFocus
                                            formatText={formatText}
                                            keyboardType={Platform.OS === "android" ? "numeric" : "number-pad"}
                                            onKeyPress={({ nativeEvent }) => { onKeyPress(nativeEvent, index) }}
                                            onFocus={({ nativeEvent }) => { onFocus(nativeEvent, index) }}
                                            // onBlur={() => { setSelectedIndex(-1) }}
                                            style={styles.textinput} />
                                    </View>
                                )
                            }} />

                        <Button
                            text={IMLocalized(`Resend Code`)}
                            textStyle={styles.redendCodeButtonText}
                            containerStyle={styles.resendCodeButton}
                            width={"90%"}
                            backgroundColor={'transparent'}
                            textColor={'#2F3137'}
                            onPress={sendCodeButtonPress}
                        />

                    </View>

                    <View style={styles.buttonContainer}>
                        <Button
                            text={IMLocalized(`Continue`)}
                            containerStyle={styles.button}
                            width={"100%"}
                            onPress={onContinuePress}
                            disabled={!isFilled}
                            backgroundColor={isFilled ? colors.primary : "#A7AAB1"}
                        />
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>



        </View>
    );
}//end of index

export default index;