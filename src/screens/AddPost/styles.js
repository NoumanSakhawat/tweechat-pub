import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const bottomHeight = 80;

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    scrollview: {
        flex: 1,
        marginBottom: bottomHeight,
    },
    profilePictureContainer: {
        marginLeft: AppStyles.HORIZONTAL
    },
    body: {
        paddingHorizontal: AppStyles.HORIZONTAL,
        marginTop: 0,
        flexDirection: "row",
        marginBottom: bottomHeight,
    },
    textInput: {
        marginTop: -30,
        marginLeft: 4,
        fontSize: 16,
        marginLeft: AppStyles.HORIZONTAL * 2 + 12,
        // paddingBottom: bottomHeight,
    },
    accessory: {
        ...AppStyles.accessory,
    },
});//end of styles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
        flexDirection: "row",
        alignItems: "center",
    },
    cancelPrimaryContainer: {
        flex: 1,
    },
    cancelContainer: {
        marginLeft: AppStyles.HORIZONTAL,
        paddingVertical: 7,
    },
    cancelText: {
        color: colors.primary,
        fontSize: 16,
    },
    postPrimaryContainer: {
        flex: 1,
        alignItems: "flex-end",
    },
    postContainer: {
        borderRadius: 17,
        backgroundColor: "#C1DBFF",
        marginRight: AppStyles.HORIZONTAL,
        paddingHorizontal: 16,
        paddingVertical: 7,
    },
    postText: {
        color: colors.white,
        fontSize: 15,
    },
});//end of headerStyles

export const footerStyles = StyleSheet.create({
    primaryContainer: {
        position: "absolute",
        bottom: 0,
    },
    imageFlatlist: {
        paddingBottom: 9,
        flexGrow: 0,
        paddingTop: 2,
    },
    imagesHide: {
        width: Dimensions.get('window').width,
        height: 0,
    },
    imagesContentContainer: {
        paddingLeft: AppStyles.HORIZONTAL,
        paddingRight: 20,
    },
    imageContainer: {
        height: 60,
        width: 60,
        borderColor: "#EAECF0",
        borderWidth: 1,
        borderRadius: 8,
        alignItems: "center",
        justifyContent: "center",
        marginRight: 8,
    },
    image: {
        height: 60,
        width: 60,
    },
    cameraContainer: {
        height: 60,
        width: 60,
        borderColor: "#EAECF0",
        borderWidth: 1,
        borderRadius: 8,
        alignItems: "center",
        justifyContent: "center",
        marginRight: 8,
    },
    separator: {
        ...AppStyles.border,

    },
    bottomButtonContainer: {
        marginTop: 14,
        marginBottom: 14,
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 20,
        flex: 1,
    },


    iconPrimaryContainerRight: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end",
        // alignSelf: "flex-end",
        flex: 1,
    },
    rightIconContainer: {

    },
    iconContainer: {
        marginRight: 24,
    },
    icon: {
        height: 25,
        width: 25,
        tintColor: "rgba(76, 158, 235, 0.5)",
        resizeMode: "contain"
    },
    divider: {
        ...AppStyles.border,
        height: 30,
        width: 0.32,
        marginHorizontal: 15,
        backgroundColor: "#CED5DC",
    },
});//end of footerStyles

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export const imageStyles = StyleSheet.create({
    flatlist: {
        flexGrow: 0,
    },
    contentContainer: {
        marginTop: 20,
        paddingLeft: AppStyles.HORIZONTAL * 2 + 12,
    },
    primaryContainer: {

    },
    image: {
        minHeight: HEIGHT / 4,
        width: WIDTH - (AppStyles.HORIZONTAL * 3),
        borderColor: 'rgba(0,0,0,0.5)',
        borderWidth: 0.5,
        marginRight: 20,
        borderRadius: 8,
    },
    circleContainer: {
        position: "absolute",
        right: 30,
        top: 8,
        backgroundColor: colors.white,
        ...AppStyles.shadow,
        borderRadius: 20,
        padding: 3,
    },
});//end of imageStyles 

export const gifStyles = StyleSheet.create({
    flatlist: {
        flexGrow: 0,
    },
    contentContainer: {


    },
    primaryContainer: {
        // paddingLeft: AppStyles.HORIZONTAL,
        paddingLeft: AppStyles.HORIZONTAL * 2 + 12,
        marginTop: 20,
        // height: HEIGHT / 4,
        width: WIDTH - (AppStyles.HORIZONTAL * 3),
    },
    image: {
        minHeight: HEIGHT / 4,
        width: WIDTH - (AppStyles.HORIZONTAL * 3),
        borderColor: 'rgba(0,0,0,0.5)',
        borderWidth: 0.5,
        marginRight: 20,
        borderRadius: 8,
    },
    circleContainer: {
        position: "absolute",
        right: -10,
        top: 8,
        backgroundColor: colors.white,
        ...AppStyles.shadow,
        borderRadius: 20,
        padding: 3,

    },
});//end of gifStyles 


export const pollStyles = StyleSheet.create({
    primaryContainer: {
        borderColor: "#DADADA",
        borderWidth: 1,
        borderRadius: 8,
        padding: 12,
        paddingRight: 0,
        marginLeft: AppStyles.HORIZONTAL * 2 + 12,
        marginRight: 16,
    },
    itemPrimaryContainer: {
        flexDirection: "row",
        alignItems: "center",
    },
    // textinput: {
    //     width: "90%",
    // },


    //TEXT INPUT

    containerTextinput: {
        paddingVertical: 7,
        paddingHorizontal: 8,
        minHeight: 48,
        marginTop: 8,
        width: "90%",

    },
    inputContainerStyleError: {
        borderColor: "red",
    },
    inputContainerStyle: {
        borderColor: "#A7AAB1",
        borderWidth: 1,
        borderRadius: 8,
        justifyContent: "center",
    },
    textinput: {

    },
    textinputError: {
        color: "#DC3131",
    },

    closeIconContainer: {
        padding: 5,
        paddingLeft: 12,
        top: -5,
        position: "absolute",
        right: 0,
    },
    iconContainer: {
        padding: 5,
        paddingLeft: 12,
        position: "absolute",
        right: 0,
    },
    pollLengthText: {
        color: "#C4C4C4",
        fontSize: 12,
        marginTop: 8,
    },
    pollLengthValue: {
        color: colors.primary,
        fontSize: 12,
        marginTop: 8,
    },
})

export const locationStyles = StyleSheet.create({
    primaryContainer: {
        marginLeft: AppStyles.HORIZONTAL * 2 + 12,
        flexDirection: "row",
        alignItems: "center",
        marginTop: 10,
    },
    name: {
        color: "#868990",
        fontSize: 14,
    },
});//end of locationStyles 
