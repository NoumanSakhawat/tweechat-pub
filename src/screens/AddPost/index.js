import React, { createRef, useEffect, useState } from 'react';
import { Button, Dimensions, FlatList, Image, InputAccessoryView, Keyboard, KeyboardAvoidingView, Platform, TextInput as RNTextInput, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { ScrollView } from 'react-native-gesture-handler';
import AutoHeightImage from '../../../lib/react-native-auto-height-image/AutoHeightImage';
import BottomModal from '../../components/BottomModal';
import Text from '../../components/Text';
import TextInput from '../../components/TextInput';
import VectorIcon from '../../components/VectorIcon';
import AppEnum from '../../constants/AppEnum';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';
import Constants from '../../constants/Constants';
import StaticData from '../../constants/StaticData';
import AppFunctions from '../../helper/AppFunctions';
import CameraFunctions from '../../helper/CameraFunctions';
import HelperFunctions, { emptyValidate } from '../../helper/HelperFunctions';
import LocationFunctions from '../../helper/LocationFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import ROUTES from '../../routes/ROUTES';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import TweeHeader from '../../tweeComponents/TweeHeader';
import ScrollPicker from '../../UIComponents/ScrollPicker';
import { footerStyles, gifStyles, headerStyles, imageStyles, locationStyles, pollStyles, styles } from './styles';
import ProgressCircle from '../../../lib/react-native-progress-circle';

const cColor = {
    icon: `rgba(76, 158, 235, 0.5)`,
    iconSelected: `rgba(76, 158, 235, 1)`
}
const bottomData = StaticData.addPostBottomButton();
const inputAccessoryViewID = 'postAccessoryView';

const index = ({ navigation }) => {
    const [gallery, setGallery] = useState([]);
    const [multimedia, setMultimedia] = useState([]);
    const [pollData, setPollData] = useState([]);
    const [pollExtraData, setPollExtraData] = useState(false);
    const [pollInputSelectedIndex, setPollInputSelectedIndex] = useState(-1);

    const [pollLength, setPollLength] = useState('1 day');
    const [pollLengthVisible, setPollLengthVisible] = useState(false);

    const [iconType, setIconType] = useState(null);
    const [locationUsed, setLocationUsed] = useState(false);

    const [gif, setGif] = useState(null);
    const [location, setLocation] = useState(null);

    const [textPercent, setTextPercent] = useState(0);
    const [inputText, setInputText] = useState('');

    useEffect(async () => {
        if (gallery.length < 1) {
            const photosArr = await CameraFunctions.getPhotos();
            if (emptyValidate(photosArr)) {
                setGallery(photosArr.edges)
            }
        }

        return () => {

        };
    }, []);

    const updateMultimedia = (data) => {
        const newData = [...multimedia, ...data];
        setMultimedia(newData);
        setIconType(AppEnum.ADD_POST_TYPE.image);
    };//end of updateMultimedia

    const removeItemMultimedia = (index) => {
        let newData = JSON.parse(JSON.stringify(multimedia));
        newData.splice(index, 1);

        setMultimedia(newData);
        setIconType(null);
    };//end of removeItemMultimedia

    const updateGif = (url) => {
        setGif(url);
        setIconType(AppEnum.ADD_POST_TYPE.gif);
    };//end of updateGif

    const updateLocation = (loc, isUsed = true) => {

        setLocation(loc);
        setLocationUsed(isUsed);
    };//end of updateGif

    const _renderFooter = () => {
        const showImage = (multimedia.length < 1 && iconType === null);
        return (
            <View style={footerStyles.primaryContainer}>


                {showImage ?
                    <FlatList
                        data={gallery}
                        horizontal
                        style={footerStyles.imageFlatlist}
                        keyExtractor={HelperFunctions.keyExtractor}
                        contentContainerStyle={footerStyles.imagesContentContainer}
                        renderItem={({ item, index }) => {

                            return (
                                <>
                                    {index === 0 &&
                                        <TouchableOpacity style={footerStyles.cameraContainer}
                                            onPress={async () => {
                                                const res = await CameraFunctions.camera(false);
                                                if (res) {
                                                    updateMultimedia(res);
                                                }
                                            }}>
                                            <VectorIcon
                                                name={"camera-outline"}
                                                color={colors.primary}
                                                size={24} />
                                        </TouchableOpacity>
                                    }
                                    <TouchableOpacity style={footerStyles.imageContainer}
                                        onPress={async () => {
                                            updateMultimedia([{
                                                "exif": null,
                                                "filename": item.node.image.filename,
                                                "path": item.node.image.uri,
                                                "height": item.node.image.height,
                                                "width": item.node.image.width,
                                                "modificationDate": item.node.timestamp,
                                                "localIdentifier": HelperFunctions.guid(),
                                                "size": item.node.image.fileSize,
                                                "sourceURL": item.node.image.uri,
                                                "mime": "image/jpeg",
                                                "cropRect": null,
                                                "duration": null,
                                                "creationDate": item.node.timestamp,
                                                "id": HelperFunctions.guid()
                                            }])
                                        }}>
                                        <Image
                                            source={{ uri: item.node.image.uri }}
                                            style={footerStyles.image}
                                            borderRadius={8} />

                                    </TouchableOpacity>

                                    {index === gallery.length - 1 &&
                                        <TouchableOpacity style={footerStyles.cameraContainer}
                                            onPress={async () => {
                                                const res = await CameraFunctions.gallery(true, false);
                                                if (res) {
                                                    updateMultimedia(res);
                                                }
                                            }}>
                                            <VectorIcon
                                                name={"image-outline"}
                                                color={colors.primary}
                                                size={24} />
                                        </TouchableOpacity>
                                    }
                                </>
                            )
                        }} />
                    :
                    <View style={footerStyles.imagesHide} />
                }
                <View style={footerStyles.separator} />

                {/* ******************** BOTTOM BUTTON Start ******************** */}

                <ScrollView contentContainerStyle={footerStyles.bottomButtonContainer}
                    style={footerStyles.imageFlatlist}

                    horizontal
                    showsHorizontalScrollIndicator={false}>
                    <TouchableOpacity style={footerStyles.iconContainer}
                        disabled={iconType !== null && iconType !== AppEnum.ADD_POST_TYPE.image}
                        onPress={async () => {
                            const res = await CameraFunctions.gallery(true, false);
                            if (res) {
                                updateMultimedia(res);
                            }
                        }}>
                        <Image
                            source={bottomData[0].image}
                            style={[footerStyles.icon, {
                                tintColor: iconType === null ? cColor.iconSelected : iconType === AppEnum.ADD_POST_TYPE.image ? cColor.iconSelected : cColor.icon
                            }]} />
                    </TouchableOpacity>

                    <TouchableOpacity style={footerStyles.iconContainer}
                        disabled={iconType !== null && (iconType !== AppEnum.ADD_POST_TYPE.gif || emptyValidate(gif))}
                        onPress={() => {
                            navigation.navigate(ROUTES.GIF, {
                                updateGif
                            });
                        }}>
                        <Image
                            source={bottomData[1].image}
                            style={[footerStyles.icon, {
                                tintColor: iconType === null ? cColor.iconSelected : (iconType === AppEnum.ADD_POST_TYPE.gif && !emptyValidate(gif)) ? cColor.iconSelected : cColor.icon
                            }]}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity style={footerStyles.iconContainer}
                        disabled={iconType !== null && iconType !== AppEnum.ADD_POST_TYPE.poll}
                        onPress={async () => {
                            if (pollData.length > 0) return
                            const pollArr = [];
                            for (let i = 0; i < 2; i++) {
                                pollArr.push({
                                    id: i,
                                    input: '',
                                    inputError: '',
                                    ref: createRef(),
                                })
                            }
                            setPollData(pollArr);


                            setIconType(AppEnum.ADD_POST_TYPE.poll);

                            await HelperFunctions.sleep(0.000001);

                            if (emptyValidate(pollArr[0].ref.current)) {
                                setPollInputSelectedIndex(0);
                                pollArr[0].ref.current.focus();
                            }

                        }}>
                        <Image
                            source={bottomData[2].image}
                            style={[footerStyles.icon, {
                                tintColor: iconType === null ? cColor.iconSelected : iconType === AppEnum.ADD_POST_TYPE.poll ? cColor.iconSelected : cColor.icon
                            }]}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity style={footerStyles.iconContainer}
                        onPress={async () => {
                            const res = await LocationFunctions.getCurrentLocation();
                            let latitude = '';
                            let longitude = '';
                            if (res) {
                                latitude = res.coords.latitude;
                                longitude = res.coords.longitude;
                            }
                            navigation.navigate(ROUTES.Location, {
                                updateLocation,
                                latitude,
                                longitude,
                                selectedLocation: null
                            });
                        }}>
                        <Image
                            source={locationUsed ? bottomData[3].imageSelected : bottomData[3].image}
                            style={[footerStyles.icon, {
                                tintColor: cColor.iconSelected
                            }]}
                        />
                    </TouchableOpacity>


                    <View style={footerStyles.iconPrimaryContainerRight}>
                        <View style={footerStyles.rightIconContainer}>
                            <ProgressCircle
                                percent={textPercent}
                                radius={10}
                                borderWidth={3}
                                color={colors.primary}
                                shadowColor="#999"
                                bgColor="#fff"
                            />

                            {/* <FastImage
                                source={bottomData[4].image}
                                style={footerStyles.icon}
                                resizeMode={FastImage.resizeMode.contain} /> */}
                        </View>

                        <View style={footerStyles.divider} />

                        <TouchableOpacity style={footerStyles.rightIconContainer}>
                            <FastImage
                                source={bottomData[5].image}
                                style={footerStyles.icon} />
                        </TouchableOpacity>
                    </View>

                </ScrollView>

                {/* ******************** BOTTOM BUTTON End ******************** */}
            </View>
        )
    };//end of _renderFooter

    const _renderPoll = () => {
        if (iconType !== AppEnum.ADD_POST_TYPE.poll) return <View />
        return (
            <>
                <FlatList
                    data={pollData}
                    keyExtractor={HelperFunctions.keyExtractor}
                    extraData={pollExtraData}
                    style={pollStyles.primaryContainer}
                    scrollEnabled={false}

                    renderItem={({ item, index }) => {
                        return (
                            <KeyboardAvoidingView >
                                <View style={pollStyles.itemPrimaryContainer}>
                                    <View style={{ width: "100%" }}>
                                        <View style={[pollStyles.containerTextinput,
                                        pollStyles.inputContainerStyle, {
                                            borderColor: index === pollInputSelectedIndex ? colors.primary : "#A7AAB1",
                                        },
                                        item.inputError && pollStyles.inputContainerStyleError
                                        ]}>
                                            <RNTextInput
                                                ref={item.ref}
                                                label={IMLocalized(`Choice ${index + 1}`)}
                                                placeholder={IMLocalized(`Choice ${index + 1}`)}
                                                // containerStyle={pollStyles.textinput}
                                                style={[pollStyles.textinput]}
                                                defaultValue={item.input}
                                                maxLength={Constants.POLL_TEXT_MAX_LENGTH}
                                                onChangeText={async (text) => {
                                                    if (text.length < Constants.POLL_TEXT_MAX_LENGTH) {

                                                        if (pollData[index].inputError === true) {
                                                            pollData[index].input = text
                                                            pollData[index].inputError = ''
                                                            setPollData(pollData)
                                                            setPollExtraData(!pollExtraData);
                                                            await HelperFunctions.sleep(0.000001);
                                                            pollData[index].ref.current.focus();
                                                        } else {
                                                            pollData[index].input = text
                                                            pollData[index].inputError = ''
                                                            setPollData(pollData)
                                                        }



                                                    } else {
                                                        pollData[index].inputError = true
                                                        setPollData(pollData);
                                                        setPollExtraData(!pollExtraData);
                                                        await HelperFunctions.sleep(0.000001);
                                                        pollData[index].ref.current.focus();
                                                    }

                                                }}
                                                onFocus={() => {

                                                    pollData[index].ref.current.focus();
                                                    setPollInputSelectedIndex(index);
                                                }}
                                                // onBlur={() => {
                                                //     setPollInputSelectedIndex(-1);
                                                // }}
                                                returnKeyType={index === pollData.length - 1 ? "done" : "next"}
                                                returnKeyLabel={index === pollData.length - 1 ? "done" : "next"}
                                                onSubmitEditing={async () => {
                                                    if (index !== pollData.length - 1) {
                                                        setPollInputSelectedIndex(index + 1);
                                                        await HelperFunctions.sleep(0.1);
                                                        pollData[index + 1].ref.current.focus();
                                                    } else {
                                                        setPollInputSelectedIndex(-1);
                                                    }

                                                }}
                                            />
                                        </View>
                                        {item.inputError ?
                                            <Text style={pollStyles.textinputError}>{IMLocalized(`Max Limit Over.`)}</Text>
                                            :
                                            <View />
                                        }
                                    </View>
                                    {index === 0 &&
                                        <TouchableOpacity style={pollStyles.closeIconContainer} onPress={() => {
                                            setIconType(null);
                                            setPollData([]);
                                            setPollInputSelectedIndex(-1);
                                            setPollExtraData(!pollExtraData);
                                            setPollLength('1 day')
                                        }}>
                                            <VectorIcon name={"close"} color={"#898A8D"} />
                                        </TouchableOpacity>
                                    }
                                    {(index === pollData.length - 1 && pollData.length < Constants.POLL_MAX_LENGTH) &&
                                        <TouchableOpacity style={pollStyles.iconContainer} onPress={() => {

                                            const newData = pollData;
                                            newData.push({
                                                id: pollData.length,
                                                input: '',
                                                inputError: '',
                                                ref: createRef(),
                                            });
                                            setPollData(newData);
                                            setPollInputSelectedIndex(newData.length - 1);
                                            setPollExtraData(!pollExtraData);
                                        }}>
                                            <VectorIcon name={"add"} color={colors.primary} />
                                        </TouchableOpacity>
                                    }
                                </View>
                            </KeyboardAvoidingView>
                        )
                    }}
                    ListFooterComponent={
                        !emptyValidate(pollLength) ? <View /> : (
                            <TouchableOpacity onPress={() => { setPollLengthVisible(true) }}>
                                <Text style={pollStyles.pollLengthText}>{IMLocalized(`Poll length`)}</Text>
                                <View >
                                    <Text style={pollStyles.pollLengthValue}>{pollLength}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    } />

                <BottomModal visible={pollLengthVisible} onDismiss={() => { setPollLengthVisible(false) }}>
                    <ScrollPicker
                        closeBottomSheet={() => { setPollLengthVisible(false) }}
                        setPollLength={({ Days, Hours, Minutes }) => {
                            setPollLength(AppFunctions.getPollLength(Days, Hours, Minutes))
                        }}
                    />
                </BottomModal>
            </>

        )
    };//end of _renderPoll

    const _renderGif = () => {
        if (!emptyValidate(gif)) return <View />
        return (
            <View style={gifStyles.primaryContainer}>
                <AutoHeightImage
                    width={WIDTH - (AppStyles.HORIZONTAL * 3)}
                    source={{ uri: gif }}
                    style={gifStyles.image}
                />
                {/* <Image
                    source={{ uri: gif }}
                    style={gifStyles.image}
                    resizeMode={FastImage.resizeMode.cover} /> */}
                <TouchableOpacity style={gifStyles.circleContainer}
                    onPress={() => {

                        setGif(null);
                        setIconType(null);
                    }}>
                    <VectorIcon
                        name={"close"}
                        size={20} />
                </TouchableOpacity>
            </View>
        )
    };//end of _renderGif

    const _renderLocation = () => {
        if (!emptyValidate(location)) return <View />
        return (
            <TouchableOpacity style={locationStyles.primaryContainer} onPress={() => {
                navigation.navigate(ROUTES.Location, {
                    updateLocation,
                    latitude: "",
                    longitude: "",
                    selectedLocation: location,
                });
            }}>
                <VectorIcon name={"location"} color={"#868990"} />
                <Text style={locationStyles.name}>{location.name}</Text>
            </TouchableOpacity>
        )
    };//end of _renderLocation

    return (
        <View style={styles.primaryContainer}>

            <TweeHeader customHeader={() => _renderHeader(navigation)} />


            <KeyboardAvoidingView style={{ flex: 1, }} behavior="padding" enabled>
                <ScrollView contentContainerStyle={{ flex: 1, }} scrollEnabled={false} nestedScrollEnabled
                    keyboardDismissMode='interactive'
                    keyboardShouldPersistTaps='handled'>

                    {/* <View style={styles.body}> */}

                    <ScrollView style={{
                        ...styles.scrollview,
                        marginBottom: multimedia.length < 1 && iconType === null ? 160 : 80,
                    }} nestedScrollEnabled>
                        <View style={styles.profilePictureContainer} >
                            <ProfilePicture
                                source={{ uri: StaticData.userProfilePicture }}
                                size={32}
                            />
                        </View>

                        <RNTextInput
                            multiline={true}
                            placeholder={iconType === AppEnum.ADD_POST_TYPE.poll ? `${IMLocalized(`Ask a question`)}...` : iconType === AppEnum.ADD_POST_TYPE.gif ? `${IMLocalized(`Add a comment`)}...` : IMLocalized(`What’s happening?`)}
                            placeholderTextColor={"#868990"}
                            onChangeText={(text) => {
                                setInputText(text);
                                const length = (text.length / Constants.ADD_POST_TEXT_LENGTH) * 100;

                                setTextPercent(length)
                            }}
                            autoFocus={true}
                            inputAccessoryViewID={inputAccessoryViewID}
                            style={[styles.textInput, {}]}
                        />
                        {Platform.OS === "ios" &&
                            <InputAccessoryView nativeID={inputAccessoryViewID}>
                                <View style={styles.accessory}>
                                    <Button
                                        onPress={() => Keyboard.dismiss()}
                                        title={IMLocalized(`Done`)}
                                    />
                                </View>
                            </InputAccessoryView>
                        }

                        <FlatList
                            data={multimedia}
                            style={imageStyles.flatlist}
                            contentContainerStyle={imageStyles.contentContainer}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={HelperFunctions.keyExtractor}
                            renderItem={({ item, index }) => _renderImage(item, index, removeItemMultimedia)} />

                        {_renderPoll()}

                        {_renderGif()}

                        {_renderLocation()}
                    </ScrollView>

                    {/* </View> */}


                    {_renderFooter()}

                </ScrollView>
            </KeyboardAvoidingView>


        </View>
    )
}//end of index

export default index;

const WIDTH = Dimensions.get('window').width;
const _renderImage = (item, index, removeItemMultimedia) => {


    return (
        <View style={imageStyles.primaryContainer}>
            <AutoHeightImage
                width={WIDTH - (AppStyles.HORIZONTAL * 3)}
                style={[imageStyles.image]}
                source={{ uri: item.path }}
            />

            {/* <Image
                source={{ uri: item.path }}
                style={[imageStyles.image, {
                    // height: item.height / 5,

                    // // width: item.width,
                    // flex: 1,
                    aspectRatio: 1.5,
                    width: null,
                    height: null,
                    resizeMode: 'contain'
                }]}
                resizeMode="contain"
            /> */}
            <TouchableOpacity style={imageStyles.circleContainer}
                onPress={() => {

                    removeItemMultimedia(index);
                }}>
                <VectorIcon
                    name={"close"}
                    size={20} />
            </TouchableOpacity>
        </View>
    )
}//end of _renderImage



const _renderHeader = (navigation) => {
    return (
        <View style={headerStyles.primaryContainer}>
            <View style={headerStyles.cancelPrimaryContainer}>
                <TouchableOpacity style={headerStyles.cancelContainer}
                    onPress={() => { navigation.pop() && navigation.goBack(); }}>
                    <Text style={headerStyles.cancelText}>{IMLocalized(`Cancel`)}</Text>
                </TouchableOpacity>
            </View>

            <View style={headerStyles.postPrimaryContainer}>
                <TouchableOpacity style={headerStyles.postContainer}>
                    <Text style={headerStyles.postText}>{IMLocalized(`Post`)}</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}//end of _renderHeaderLeft
