import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const WIDTH = Dimensions.get('window').width;

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    body: {
        flex: 1,
    },
    heading: {
        color: "#0F1117",
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 40,
        paddingHorizontal: 20,
    },
    heading1: {
        color: "#2F3137",
        fontSize: 16,
        textAlign: "center",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 16,
    },

    //DROP DOWN STYLING
    dropdown: {
        // width: "86%",
        width: WIDTH - AppStyles.HORIZONTAL * 2,
        marginHorizontal: AppStyles.HORIZONTAL,
        borderColor: "#A7AAB1",
        borderWidth: 1,
    },

    dropdownContainerStyle: {
        marginTop: 32,
    },
    dropdownBoxContainerStyle: {
        // width: "86%",
        width: WIDTH - AppStyles.HORIZONTAL * 2,
        marginHorizontal: AppStyles.HORIZONTAL,
        borderColor: "#A7AAB1",
        borderWidth: 1,
    },
    itemSeparatorStyle: {
        backgroundColor: `rgba(0,0,0,0.1)`,
    },
    searchContainerStyle: {
        borderBottomColor: "#dfdfdf",
    },
    searchTextInputStyle: {
        borderColor: "#A7AAB1",
        borderWidth: 1,
    },
    //DROP DOWN STYLING END


    textinputContainer: {
        flexDirection: "row",
        // alignItems: "center",
        marginTop: 16,
        marginHorizontal: AppStyles.HORIZONTAL,
    },

    //DIAL CODE
    dialCodeContainerTextinput: {
        width: 70,
    },
    dialCodeInputContainerStyle: {
        borderColor: "#A7AAB1",
        borderWidth: 1,
        borderRadius: 8,
    },
    dialCodeTextinput: {

    },

    //PHONE NUMBER
    numberContainerTextinput: {
        // width: 70,
        flex: 1,
        marginLeft: 8,
    },
    numberInputContainerStyle: {
        // borderColor: "#A7AAB1",
        // borderWidth: 1,
        // borderRadius: 8,
    },
    numberTextinput: {

    },

    //BUTTON

    buttonContainer: {
        marginTop: 24,
        marginHorizontal: AppStyles.HORIZONTAL,
        marginBottom: 16,

        position: "absolute",
        bottom: 0,
        // width: "88%",

        width: Dimensions.get("screen").width - (AppStyles.HORIZONTAL * 2),
    },
    button: {
        marginHorizontal: AppStyles.HORIZONTAL,
        alignSelf: "center",
    },

});//end of styles