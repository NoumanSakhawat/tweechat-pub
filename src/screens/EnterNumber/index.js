import React, { useEffect, useState } from 'react';
import { Keyboard, KeyboardAvoidingView, ScrollView, View } from 'react-native';
import Text from '../../components/Text';
import { IMLocalized } from '../../locales/IMLocalization';
import { styles } from './styles';
import DropDownPicker from 'react-native-dropdown-picker';
import TextInput from '../../components/TextInput';
import Country from '../../constants/Country';
import { Platform } from 'react-native';
import { emptyValidate } from '../../helper/HelperFunctions';
import * as RNLocalize from "react-native-localize";
import { PhoneNumberUtil, PhoneNumberFormat } from "google-libphonenumber";
import Button from '../../components/Button';
import colors from '../../constants/colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ROUTES from '../../routes/ROUTES';

const phoneUtil = PhoneNumberUtil.getInstance();

const index = ({ navigation }) => {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState(Country.get());

    const [dialCode, setDialCode] = useState('');

    const [number, setNumber] = useState(__DEV__ ? '3055536641' : '');
    const [numberError, setNumberError] = useState('');

    const [searchInputFocus, setSearchInputFocus] = useState(false);

    useEffect(async () => {
        const userCountry = RNLocalize.getCountry();
        if (userCountry) {
            const country = Country.getCountry(userCountry);
            setDialCode(country.dialCode)
            setValue(country.isoCode)
            setItems(Country.get(country.isoCode));
        }

    }, [])

    const formatText = (text) => {
        return text.replace(/[^+\d]/g, '');
    };//end of formatText

    const isValidNumber = (number) => {
        try {
            const country = Country.getCountry(value);
            const parsedNumber = phoneUtil.parse(number, country.dialCode);
            return phoneUtil.isValidNumber(parsedNumber);
        } catch (err) {
            return false;
        }
    };//end of isValidNumber

    const getNumberAfterPossiblyEliminatingZero = (number, code) => {

        if (number.length > 0 && number.startsWith("0")) {
            number = number.substr(1);
            return { number, formattedNumber: code ? `${code}${number}` : number };
        } else {
            return { number, formattedNumber: code ? `${code}${number}` : number };
        }
    };//end of getNumberAfterPossiblyEliminatingZero

    const onChangeNumberText = (text) => {
        setNumberError('');
        setNumber(text)
    };//end of onChangeNumberText

    const onSubmitNumberEditing = () => {
        const isValid = isValidNumber(`${dialCode}${number}`);

        if (isValid) {
            const country = Country.getCountry(value);
            const rawNumber = phoneUtil.parseAndKeepRawInput(number, value);
            const format = phoneUtil.format(rawNumber, PhoneNumberFormat.NATIONAL);
            const formattedNumber = getNumberAfterPossiblyEliminatingZero(format, country.dialCode);
            setNumber(formattedNumber.number);
        } else {
            setNumberError(IMLocalized(`Please, enter valid number.`))
        }
    };//end of onSubmitNumberEditing

    onNextPress = () => {
        const isValid = isValidNumber(`${dialCode}${number}`);
        if (isValid) {
            //GOOD TO GO!
            const country = Country.getCountry(value);
            const rawNumber = phoneUtil.parseAndKeepRawInput(number, value);
            const format = phoneUtil.format(rawNumber, PhoneNumberFormat.INTERNATIONAL);

            navigation.navigate({
                name: ROUTES.OTP,
                params: {
                    number: number,
                    formattedNumber: format,
                    country,
                }
            })
        } else {
            setNumberError(IMLocalized(`Please, enter valid number.`))
        }
    };//end of onNextPress

    const onSearchFocus = () => {
        setSearchInputFocus(true);
    };//end of onSearchFocus

    const onSearchBlur = () => {
        setSearchInputFocus(false);
    };//end of onSearchBlur


    return (
        <View style={styles.primaryContainer}>

            <KeyboardAvoidingView style={{ flex: 1, }} behavior="padding" enabled>
                <ScrollView contentContainerStyle={{ flex: 1, }} scrollEnabled={false} nestedScrollEnabled keyboardDismissMode='interactive'
                    keyboardShouldPersistTaps='handled'>

                    <View style={styles.body}>

                        <Text style={styles.heading}>{IMLocalized(`Enter your phone number to get started`)}</Text>

                        <Text style={styles.heading1}>{IMLocalized(`You will receive a verification code. Carrier rates may apply.`)}</Text>

                        <DropDownPicker
                            open={open}
                            value={value}
                            items={items}
                            setOpen={(val) => {
                                setOpen(val)
                                Keyboard.dismiss();
                            }}
                            setValue={(val) => {
                                const coun = Country.getCountry(val());
                                if (coun) {
                                    setDialCode(coun.dialCode)
                                }

                                setValue(val());
                                setItems(Country.get(val()));
                            }}
                            setItems={setItems}

                            placeholder={IMLocalized(`Select an country`)}

                            style={[styles.dropdown, {
                                borderColor: open ? colors.primary : "#A7AAB1",
                                ...open && { borderWidth: 2 }
                            }]}
                            containerStyle={[styles.dropdownContainerStyle, {
                                borderColor: open ? colors.primary : "#A7AAB1",
                            }]}



                            //EXPAND LIST
                            itemSeparator
                            itemSeparatorStyle={styles.itemSeparatorStyle}
                            dropDownContainerStyle={[styles.dropdownBoxContainerStyle, {
                                ...open && { borderTopWidth: 1, borderTopColor: colors.primary },
                            }]}

                            selectedItemLabelStyle={{
                                fontWeight: "bold",
                                color: colors.primary,
                            }}

                            //SEARCHABLE
                            searchable
                            searchPlaceholder={`${IMLocalized(`Search country`)}...`}
                            searchContainerStyle={styles.searchContainerStyle}
                            searchTextInputStyle={[styles.searchTextInputStyle, {
                                borderColor: searchInputFocus ? colors.primary : "#A7AAB1",
                            }]}
                            searchTextInputProps={{
                                onFocus: onSearchFocus,
                                onBlur: onSearchBlur,
                            }}
                        />

                        <View style={styles.textinputContainer}>

                            <TextInput
                                editable={false}
                                disabled={true}
                                value={dialCode}
                                label={!emptyValidate(dialCode) ? IMLocalized(`+`) : ''}
                                baseColor={'#A7AAB1'}
                                textColor={'#0F1117'}
                                labelFontSize={12}
                                fontSize={16}
                                lineType={"none"}
                                containerStyle={styles.dialCodeContainerTextinput}
                                inputContainerStyle={styles.dialCodeInputContainerStyle}
                                style={styles.dialCodeTextinput} />


                            <TextInput
                                label={IMLocalized(`Phone Number`)}
                                baseColor={'#A7AAB1'}
                                textColor={'#0F1117'}
                                labelFontSize={12}
                                fontSize={16}
                                showDone
                                formatText={formatText}
                                value={number}
                                onChangeText={(text) => onChangeNumberText(text)}
                                onSubmitEditing={onSubmitNumberEditing}
                                onBlur={onSubmitNumberEditing}
                                keyboardType={Platform.OS === "android" ? "numeric" : "number-pad"}
                                containerStyle={styles.numberContainerTextinput}
                                inputContainerStyle={styles.numberInputContainerStyle}
                                style={styles.numberTextinput}
                                error={numberError}
                            />

                        </View>
                    </View>

                    <View style={styles.buttonContainer}>
                        <Button
                            text={IMLocalized(`Next`)}
                            containerStyle={styles.button}
                            width={"100%"}
                            onPress={this.onNextPress}
                        />
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>



        </View>
    );
}//end of index

export default index;