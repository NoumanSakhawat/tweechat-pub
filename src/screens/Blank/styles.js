import { StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';


export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },

});//end of styles

export const headerStyles = StyleSheet.create({

    primaryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    leftContainer: {
        paddingLeft: AppStyles.HORIZONTAL / 2,
        paddingRight: AppStyles.HORIZONTAL / 2,
        flex: 1,
    },
    leftIcon: {

    },
    titleContainer: {
        flex: 2,
    },
    title: {
        textAlign: 'left',
        fontSize: 20,
        color: "#0F1117",
        fontWeight: "bold",
    },
    rightContainer: {
        flex: 0,
        paddingRight: AppStyles.HORIZONTAL / 2,
    },

});//end of headerStyles

