import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import { IMLocalized } from '../../locales/IMLocalization';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { headerStyles, styles } from './styles';


const index = ({ navigation }) => {

    const _renderHeader = () => {
        return (
            <View style={headerStyles.primaryContainer}>
                <TouchableOpacity onPress={() => {
                    navigation.openDrawer();
                }}
                    style={headerStyles.leftContainer}>
                    <VectorIcon
                        name={"menu"}
                        size={30}
                        color={colors.primary}
                        style={headerStyles.leftIcon}
                    />
                </TouchableOpacity>

                <View style={headerStyles.titleContainer}>
                    <Text style={headerStyles.title}>{IMLocalized(`TweeChat`)}</Text>
                </View>

                <View style={headerStyles.rightContainer} >
                </View>
            </View>
        )
    };//end of _renderHeader



    return (
        <View style={styles.primaryContainer}>
            <TweeHeader customHeader={() => _renderHeader()} bottomBorder />


        </View>
    );
}//end of index

export default index;

