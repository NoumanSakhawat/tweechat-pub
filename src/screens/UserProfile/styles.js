import { Dimensions, StyleSheet, StatusBar } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

export const HEADER_HEIGHT_EXPANDED = 55; //35
export const HEADER_HEIGHT_NARROWED = 90; //90
const HEADER_HEIGHT_NARROWED_SCROLLED = 70;

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    scrollView: {
        zIndex: 3,
        // marginTop: HEADER_HEIGHT_NARROWED,
        // paddingTop: HEADER_HEIGHT_EXPANDED,
        marginTop: HEADER_HEIGHT_NARROWED_SCROLLED,
        paddingTop: HEADER_HEIGHT_EXPANDED,
    },
    bodyPrimaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    userDetailContainer: {
        flex: 1,
        paddingHorizontal: 20,
    },
    userImageContainer: {
        ...AppStyles.shadow,

        flexDirection: "row",
        alignItems: "center",
    },

    editProfileButtonContainer: {
        width: 108,
        minHeight: 32,

        position: "absolute",
        right: 0,
        top: 24,


        borderWidth: 1,
        borderColor: colors.primary,
        borderRadius: 8,
        alignItems: "center",
        justifyContent: "center",
    },
    editProfileButtonText: {
        textAlign: "center",
        fontSize: 14,
        color: colors.primary,
    },


    userImage: (scrollY) => ({
        width: 70,
        height: 70,
        borderRadius: 12,
        borderWidth: 4,
        borderColor: colors.white,
        marginTop: -30,
        transform: [
            {
                scale: scrollY.interpolate({
                    inputRange: [0, HEADER_HEIGHT_EXPANDED],
                    outputRange: [1, 0.6],
                    extrapolate: 'clamp',
                }),
            },
            {
                translateY: scrollY.interpolate({
                    inputRange: [0, HEADER_HEIGHT_EXPANDED],
                    outputRange: [0, 16],
                    extrapolate: 'clamp',
                }),
            },
        ],
    }),

    userNameHeading: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10,
        color: "#221F2E",
    },
    username: {
        fontSize: 16,
        color: '#898A8D',
        marginBottom: 16,

    },
    shortBioHeading: {
        color: "#221F2E",
        marginBottom: 8,
        fontSize: 16,
        fontWeight: 'bold',
    },
    shortBioDescription: {
        color: "#50555C",
        marginBottom: 17,
        fontSize: 14,
    },
    textWithIconContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 16,
    },
    textLeftIcon: {
        height: 20,
        width: 20,
    },
    textWithIcon: {
        color: "#221F2E",
        fontSize: 16,
        marginLeft: 10,
    },
    profileStatPrimaryContainer: {
        flexDirection: 'row',
        alignItems: "center",
        marginBottom: 32,
    },
    profileStatContainer: {
        flexDirection: 'row',
        alignItems: "center",
    },
    profileStatNumber: {
        color: colors.primary,
        fontWeight: 'bold',
        fontSize: 16,
        marginRight: 8,
    },
    profileStatText: {
        color: colors.primary,
        fontSize: 16,
        marginRight: 16,
    },
    myTweetsHeading: {
        color: "#221F2E",
        marginBottom: 12,
        fontSize: 16,
        fontWeight: 'bold',
    },
    contentContainerStyle: {
        paddingBottom: 50,
    },



});//end of styles

export const headerDataStyles = StyleSheet.create({
    container: (insets, scrollY) => ({
        zIndex: 2,
        position: 'absolute',
        top: insets.top + 6,
        left: 0,
        right: 0,
        alignItems: 'center',
        opacity: scrollY.interpolate({
            inputRange: [90, 110],
            outputRange: [0, 1],
        }),
        transform: [
            {
                translateY: scrollY.interpolate({
                    inputRange: [90, 120],
                    outputRange: [30, 0],
                    extrapolate: 'clamp',
                }),
            },
        ],
    }),
    username: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: -3,
        color: colors.white,
    },
    tweetsCount: {
        fontSize: 13,
        color: colors.white,
    },

});//end of headerDataStyles

export const refreshArrowStyles = StyleSheet.create({
    container: (insets, scrollY) => ({
        zIndex: 2,
        position: 'absolute',
        top: insets.top + 13,
        left: 0,
        right: 0,
        alignItems: 'center',
        opacity: scrollY.interpolate({
            inputRange: [-20, 0],
            outputRange: [1, 0],
        }),
        transform: [
            {
                rotate: scrollY.interpolate({
                    inputRange: [-45, -35],
                    outputRange: ['180deg', '0deg'],
                    extrapolate: 'clamp',
                }),
            },
        ],
    }),
});//end of refreshArrowStyles


export const bannerStyles = StyleSheet.create({
    imageBG: (scrollY) => ({
        position: 'absolute',
        left: 0,
        right: 0,
        height: HEADER_HEIGHT_EXPANDED + HEADER_HEIGHT_NARROWED,
        transform: [
            {
                scale: scrollY.interpolate({
                    inputRange: [-200, 0],
                    outputRange: [5, 1],
                    extrapolateLeft: 'extend',
                    extrapolateRight: 'clamp',
                }),
            },
        ],
        backgroundColor: `rgba(0,0,0,0.2)`,
    }),
    blurView: (scrollY) => ({
        ...StyleSheet.absoluteFillObject,
        zIndex: 2,
        opacity: scrollY.interpolate({
            inputRange: [-50, 0, 50, 100],
            outputRange: [1, 0, 0, 1],
        }),
    })
});//end of bannerStyles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        flexDirection: 'row',
        alignItems: "center",
        zIndex: 2,
    },
    backPrimaryContainer: (insets) => ({
        zIndex: 2,
        position: 'absolute',
        top: 0,
        paddingTop: insets.top + 10,
        left: 0,
        paddingLeft: 16,
        paddingRight: 16,
        paddingBottom: 16,
        alignItems: 'center',
        justifyContent: 'center',
    }),
    imageBack: {
        height: 24,
        width: 24,
    },

    optionPrimaryContainer: (insets) => ({
        zIndex: 2,
        position: "absolute",
        top: 0,
        paddingTop: insets.top + 10,
        right: 0,
        paddingLeft: 16,
        paddingRight: 0,
        paddingBottom: 16,
        alignItems: 'center',
        justifyContent: 'center',


    }),
    optionContainer: {
        paddingHorizontal: 16,
    },
    imageOption: {
        height: 24,
        width: 24,
    },
});//end of headerStyles