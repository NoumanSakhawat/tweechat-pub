import { BlurView } from "@react-native-community/blur";
import { CommonActions } from "@react-navigation/native";
import React, { useEffect, useRef, useState } from 'react';
import { Animated, FlatList, ImageBackground, RefreshControl, StatusBar, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Menu, MenuOption, MenuOptions, MenuTrigger } from 'react-native-popup-menu';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import LocalAssets from '../../constants/LocalAssets';
import StaticData from '../../constants/StaticData';
import HelperFunctions, { emptyValidate } from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import ROUTES from '../../routes/ROUTES';
import Post from '../../UIComponents/Post';
import { bannerStyles, headerDataStyles, headerStyles, refreshArrowStyles, styles } from './styles';

const PROFILE_PICTURE_URI =
    'https://pbs.twimg.com/profile_images/975388677642715136/7Hw2MgQ2_400x400.jpg';

const PROFILE_BANNER_URI =
    'https://pbs.twimg.com/profile_banners/3296259169/1438473955/1500x500';

const AnimatedImageBackground = Animated.createAnimatedComponent(
    ImageBackground
);

const AnimatedBlurView = Animated.createAnimatedComponent(BlurView)


const index = ({ navigation }) => {

    const [userProfile, setUserProfile] = useState(null);
    const [data, setData] = useState([]);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(async () => {
        const userProfileRes = StaticData.userProfile();
        console.log(`USER PROFILE==> ${JSON.stringify(userProfileRes)}`);
        setUserProfile(userProfileRes);

        load();
    }, []);

    const load = () => {
        setRefreshing(true);
        const data = StaticData.post(10);
        setData(data);
        setRefreshing(false);
    };//end of load

    let insets = useSafeAreaInsets();
    let scrollY = useRef(new Animated.Value(0)).current;

    const postDetailPress = (item, index) => {
        navigation.navigate({
            name: ROUTES.CommentView,
            params: { post: item, postIndex: index },
        })
    };//end of postDetailPress

    return (
        <View style={styles.primaryContainer}>

            <StatusBar barStyle={"light-content"} />
            {/* Back button */}
            {_renderHeader(navigation, insets)}

            {/* Refresh arrow */}
            {_renderRefreshArrow(insets, scrollY)}

            {/* Name + tweets count */}
            {_renderHeaderData({ count: userProfile?.tweets, name: userProfile?.user.name }, insets, scrollY)}


            {/* Banner */}
            {_renderBanner(userProfile?.user.cover, scrollY)}

            {/* Tweets/profile */}
            <Animated.ScrollView
                showsVerticalScrollIndicator={false}
                onScroll={Animated.event([{
                    nativeEvent: {
                        contentOffset: { y: scrollY },
                    },
                },],
                    { useNativeDriver: true }
                )}
                refreshControl={<RefreshControl onRefresh={load} refreshing={refreshing} />}
                style={styles.scrollView}>
                <View style={styles.bodyPrimaryContainer}>
                    <View style={styles.userDetailContainer}>


                        <View style={styles.userImageContainer}>
                            <Animated.Image
                                source={userProfile?.user.profile}
                                style={styles.userImage(scrollY)}
                            />

                            <TouchableOpacity style={styles.editProfileButtonContainer}>
                                <Text
                                    style={styles.editProfileButtonText}
                                >{IMLocalized(`Edit Profile`)}</Text>
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.userNameHeading}>
                            {userProfile?.user.name}
                        </Text>

                        <Text style={styles.username}>
                            {`@${userProfile?.user.username}`}
                        </Text>

                        <Text style={styles.shortBioHeading}>{`${IMLocalized(`Short Bio`)}:`}</Text>

                        <Text style={styles.shortBioDescription}>{`${userProfile?.bio.description}`}</Text>

                        <View style={styles.textWithIconContainer}>
                            <FastImage
                                source={LocalAssets.UserProfile.dob}
                                style={styles.textLeftIcon}
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={styles.textWithIcon}>{`${IMLocalized(`Born`)} ${userProfile?.details.dob}`}</Text>
                        </View>

                        <View style={styles.textWithIconContainer}>
                            <FastImage
                                source={LocalAssets.UserProfile.place}
                                style={styles.textLeftIcon}
                                resizeMode={FastImage.resizeMode.contain}
                            />
                            <Text style={styles.textWithIcon}>{`${userProfile?.location.cityName} ${userProfile?.location.state}`}</Text>
                        </View>

                        {/* Profile stats */}
                        <View style={styles.profileStatPrimaryContainer}>

                            <TouchableOpacity style={styles.profileStatContainer}>
                                <Text style={styles.profileStatNumber}>{userProfile?.following}</Text>
                                <Text style={styles.profileStatText}>{IMLocalized(`Following`)}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.profileStatContainer}>
                                <Text style={styles.profileStatNumber}>{userProfile?.follower}</Text>
                                <Text style={styles.profileStatText}>{IMLocalized(`Followers`)}</Text>
                            </TouchableOpacity>

                        </View>

                        <Text style={styles.myTweetsHeading}>{`${IMLocalized(`My Tweets`)}`}</Text>

                    </View>

                    {/* <View style={styles.separator} /> */}


                    <FlatList
                        data={data}
                        renderItem={({ item, index }) => {

                            return (
                                <Post
                                    userProfilePressDisable
                                    navigation={navigation}
                                    item={item}
                                    index={index}
                                    viewAllCommentPress={() => { postDetailPress(item, index) }}
                                    onPostPress={() => { postDetailPress(item, index) }}
                                    onMediaItemPress={(isMultiple, multimediaItemIndex) => {
                                        const mulData = HelperFunctions.array_move(item.multimedia, multimediaItemIndex, 0);
                                        navigation.navigate({
                                            name: ROUTES.VideoPlay,
                                            params: {
                                                isMultiple,
                                                data: mulData,
                                                item,
                                                index
                                            }
                                        })
                                    }}
                                />
                            )
                        }}
                        scrollEnabled={false}
                        nestedScrollEnabled={true}
                        contentContainerStyle={styles.contentContainerStyle}
                    />

                </View>
            </Animated.ScrollView>

        </View>
    );
}//end of index

export default index;

const _renderHeaderData = (data = { name: '', count: '' }, insets, scrollY) => {
    return (
        <Animated.View
            style={headerDataStyles.container(insets, scrollY)}>

            {emptyValidate(data.name) ?
                <Text style={headerDataStyles.username}>
                    {data.name}
                </Text>
                : <View />}

            {emptyValidate(data.count) ?
                <Text style={headerDataStyles.tweetsCount}>
                    {`${data.count} ${IMLocalized(`tweets`)}`}
                </Text>
                : <View />}
        </Animated.View>
    )
};//end of _renderHeaderData

const _renderRefreshArrow = (insets, scrollY) => {
    return (
        <Animated.View
            style={refreshArrowStyles.container(insets, scrollY)}>
            <VectorIcon
                iconType="Feather"
                name="arrow-down" color="white" size={25} />
        </Animated.View>

    )
};//end of _renderRefreshArrow

const _renderBanner = (path, scrollY) => {
    return (
        <AnimatedImageBackground
            source={path}
            style={bannerStyles.imageBG(scrollY)}>
            <AnimatedBlurView
                tint="dark"
                intensity={96}
                style={bannerStyles.blurView(scrollY)}
            />

        </AnimatedImageBackground>
    )
};//end of _renderBanner

const _renderHeader = (navigation, insets) => {
    return (
        <View style={headerStyles.primaryContainer}>


            <TouchableOpacity
                style={headerStyles.backPrimaryContainer(insets)}
                onPress={() => {
                    // navigation.pop() && navigation.goBack(); 
                    navigation.dispatch(CommonActions.goBack());
                }}>
                <FastImage
                    source={LocalAssets.ICON.videoPlayBack}
                    style={headerStyles.imageBack}
                    resizeMode={FastImage.resizeMode.contain}
                    tintColor={colors.white} />
            </TouchableOpacity>


            <View style={headerStyles.optionPrimaryContainer(insets)}>
                <Menu>
                    <MenuTrigger style={headerStyles.optionContainer}>
                        <FastImage
                            source={LocalAssets.ICON.videoPlayOption}
                            style={headerStyles.imageOption}
                            resizeMode={FastImage.resizeMode.contain}
                            tintColor={colors.white} />
                    </MenuTrigger>
                    <MenuOptions customStyles={optionsStyles}>
                        <MenuOption
                            onSelect={() => {
                            }}
                            text={IMLocalized(``)} />
                    </MenuOptions>
                </Menu>
            </View>
        </View>
    )
};//end of _renderHeader

const optionsStyles = {
    optionsContainer: {
        backgroundColor: 'transparent',
        paddingRight: 40,
        paddingTop: 10,

    },
    optionsWrapper: {
        backgroundColor: colors.white,
    },
    optionWrapper: {
        backgroundColor: colors.white,
        margin: 5,
    },
    optionTouchable: {
        underlayColor: `rgba(0,0,0,0.04)`,
        activeOpacity: 70,
    },
    optionText: {
        color: colors.black,
        fontSize: 12,
    },
};
