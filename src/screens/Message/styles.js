import { StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';


export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },


});//end of styles

export const itemStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
    },
    body: {
        flexDirection: 'row',
        marginLeft: AppStyles.HORIZONTAL,
        paddingVertical: 12,
    },
    textContainer: {
        paddingRight: AppStyles.HORIZONTAL,
        paddingLeft: 8,
        flex: 1,
    },

    userContainer: {
    },
    nameDateContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    userNameContainer: {
        flex: 1,
        alignItems: "flex-start",
        marginRight: 4,
    },
    userName: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#2F3137",
    },

    dateContainer: {
        flex: 1,
        alignItems: "flex-end",
    },
    date: {
        fontSize: 16,
        color: "#87888B",
    },




    user: {
        fontSize: 16,
        color: "#87888B",
    },



    text: {
        color: "#87888B",
        fontSize: 14,
        // paddingRight: AppStyles.HORIZONTAL * 2,
    },

    border: {
        ...AppStyles.border,
    },
});//end of itemStyles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
    },
    textinputContainer: {

    },
    textinput: {
        backgroundColor: "#EAECF0",
        marginVertical: 12,
        marginHorizontal: 16,

        borderRadius: 16,
        minHeight: 32,

        paddingHorizontal: 12,
        paddingVertical: 10,
        paddingLeft: 36
    },
    searchIcon: {
        position: "absolute",
        top: 20,
        left: 28,
    },



    secondaryContainer: {
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    leftContainer: {
        paddingLeft: AppStyles.HORIZONTAL / 2,
        paddingRight: AppStyles.HORIZONTAL / 2,
        flex: 1,
    },
    leftIcon: {

    },
    titleContainer: {
        flex: 2,
    },
    title: {
        textAlign: 'left',
        fontSize: 20,
        color: "#0F1117",
        fontWeight: "bold",
    },
    rightContainer: {
        flex: 0,
        paddingRight: AppStyles.HORIZONTAL / 2,
    },

});//end of headerStyles

