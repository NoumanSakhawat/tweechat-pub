import React, { useState } from 'react';
import { FlatList, TextInput, TouchableOpacity, View } from 'react-native';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import StaticData from '../../constants/StaticData';
import HelperFunctions, { emptyValidate } from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import ROUTES from '../../routes/ROUTES';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { headerStyles, itemStyles, styles } from './styles';


const index = ({ navigation }) => {

    const [data, setData] = useState(StaticData.messages());
    const [metadata, setMetadata] = useState(false);

    const _renderHeader = () => {
        return (
            <View style={headerStyles.primaryContainer}>
                <View style={headerStyles.secondaryContainer}>
                    <TouchableOpacity onPress={() => {
                        navigation.openDrawer();
                    }}
                        style={headerStyles.leftContainer}>
                        <VectorIcon
                            name={"menu"}
                            size={30}
                            color={colors.primary}
                            style={headerStyles.leftIcon}
                        />
                    </TouchableOpacity>

                    <View style={headerStyles.titleContainer}>
                        <Text style={headerStyles.title}>{IMLocalized(`Messages`)}</Text>
                    </View>

                    <View style={headerStyles.rightContainer} >
                    </View>
                </View>

                <View style={headerStyles.textinputContainer}>
                    <TextInput
                        style={headerStyles.textinput}
                        placeholder={IMLocalized(`Search for people and groups`)}
                        placeholderTextColor={"#868990"}
                        keyboardType={"web-search"}
                        autoCorrect={false}
                        spellCheck={false}
                        returnKeyType={"search"}
                        returnKeyLabel={"search"}
                    />

                    <View style={headerStyles.searchIcon}>
                        <VectorIcon
                            name={"search"}
                            size={20}
                            color={"#606268"}
                        />
                    </View>
                </View>
            </View>
        )
    };//end of _renderHeader

    const onMessagePress = (item, index) => {
        console.log(item);
        navigation.navigate(ROUTES.Chat, {
            user: item?.user,
        })
    };//end of onMessagePress
    const _renderItem = ({ item, index }) => {
        const raTime = HelperFunctions.timestampToReadable(item.timeago);
        const body = item.lastMessageByYou ? `You: ${item.lastMessage}` : `${item.lastMessage}`;
        return (
            <TouchableOpacity style={[itemStyles.primaryContainer]}
                onPress={() => {
                    onMessagePress(item, index)
                }}>
                <View style={itemStyles.body}>

                    <ProfilePicture
                        source={item.user.profile}
                        size={48} />


                    <View style={itemStyles.textContainer}>

                        <View style={itemStyles.userContainer}>

                            <View style={itemStyles.nameDateContainer}>
                                <View style={itemStyles.userNameContainer}>
                                    <Text style={itemStyles.userName}>{item.user.name}</Text>
                                </View>

                                <View style={itemStyles.dateContainer}>
                                    <Text style={itemStyles.date}>{`${raTime.month}/${raTime.date}/${raTime.year}`}</Text>
                                </View>
                            </View>

                            <Text style={itemStyles.user}>{`@${item.user.username}`}</Text>


                        </View>

                        <Text style={itemStyles.text}>{body}</Text>

                    </View>
                </View>

                <View style={itemStyles.border} />
            </TouchableOpacity>
        )
    };//end of _renderItem

    return (
        <View style={styles.primaryContainer}>
            <TweeHeader customHeader={() => _renderHeader()} bottomBorder />

            <FlatList
                data={data}
                extraData={metadata}
                keyExtractor={HelperFunctions.keyExtractor}
                renderItem={_renderItem} />

        </View>
    );
}//end of index

export default index;

