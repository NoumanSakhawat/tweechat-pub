import { Platform, StyleSheet } from 'react-native';
import { getBottomSpace } from '../../../lib/react-native-iphone-x-helper';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const SEND_IMAGE_SIZE = 30;
export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
        paddingBottom: getBottomSpace(),
    },

    textInput: {
        borderRadius: 20,
        paddingHorizontal: 20,
        paddingTop: 10,
        backgroundColor: '#EDF0F7',
        alignItems: 'center',
        justifyContent: 'center'

    },
    attachmentContainer: {
        alignSelf: "center",
        marginRight: -8,
        marginLeft: 16,
    },
    attachmentImage: {
        height: 24,
        width: 24,
    },
    accessory: {
        ...AppStyles.accessory,
    },

    sendContainer: {
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        height: SEND_IMAGE_SIZE,
        width: SEND_IMAGE_SIZE,
        borderRadius: SEND_IMAGE_SIZE,
        marginRight: 16,
        marginLeft: 8,
        backgroundColor: colors.primary,
    },
    sendImage: {
        height: SEND_IMAGE_SIZE / 2,
        width: SEND_IMAGE_SIZE / 2,
    },
});//end of styles

const ATTACHMENT_IMAGE_SIZE = 80;
export const attachmentStyles = StyleSheet.create({
    flatList: {
        flexGrow: 0,

    },
    contentContainerStyle: {
        alignItems: "center",
        paddingHorizontal: 26,
        paddingVertical: 16,
        justifyContent: "space-evenly",

    },
    primaryContainer: {
        alignItems: "center",
        justifyContent: "center",
        height: ATTACHMENT_IMAGE_SIZE,
        width: ATTACHMENT_IMAGE_SIZE,
        borderRadius: ATTACHMENT_IMAGE_SIZE,
        marginRight: 16,
    },
    image: {
        height: ATTACHMENT_IMAGE_SIZE / 3,
        width: ATTACHMENT_IMAGE_SIZE / 3,
    },
    text: {
        marginTop: 6,
        fontSize: 14,
        color: colors.white,
    },
});//end of attachmentStyles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
        flexDirection: "row",
        alignItems: "center",
    },
    backPrimaryContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
    },
    backContainer: {
        marginLeft: AppStyles.HORIZONTAL,
        paddingVertical: 7,
    },
    imageBack: {
        height: 24,
        width: 24,
    },
    optionPrimaryContainer: {
        flex: 1,
        alignItems: "flex-end",
        marginRight: AppStyles.HORIZONTAL / 2,
    },
    imageOption: {
        height: 24,
        width: 24,
    },

    //USER DETAILS
    userImageContainer: {
        paddingLeft: 14,
        paddingRight: 6,
    },
    username: {
        color: "#221F2E",
        fontSize: 16,
        fontWeight: Platform.OS === "ios" ? "600" : "bold",
    },
    userStatus: {
        color: "#00A707",
        fontSize: 12,
    },

});//end of headerStyles