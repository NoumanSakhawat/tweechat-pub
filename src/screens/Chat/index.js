import React, { useCallback, useEffect, useRef, useState, createRef } from 'react';
import { Button, FlatList, InputAccessoryView, Keyboard, Modal, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { GiftedChat } from 'react-native-gifted-chat';
import { Menu, MenuOptions, MenuTrigger } from 'react-native-popup-menu';
import BottomModal from '../../components/BottomModal';
import Statusbar from '../../components/Statusbar';
import Text from '../../components/Text';
import colors from '../../constants/colors';
import LocalAssets from '../../constants/LocalAssets';
import StaticData from '../../constants/StaticData';
import HelperFunctions, { printText, printTextWithLimit } from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { attachmentStyles, headerStyles, styles } from './styles';

const staticData = StaticData.chat();
const attachmentData = StaticData.attachment();

const index = ({ navigation, route }) => {
    let giftedChatRef = useRef().current;
    let attachmentRef = useRef();

    const user = route?.params?.user;
    const [data, setData] = useState(staticData);
    const [metadata, setMetadata] = useState(false);

    const showAttachment = () => {
        attachmentRef?.showStateVisible();
    };//end of showAttachment

    const hideAttachment = () => {
        attachmentRef?.hideStateVisible();
    };//end of hideAttachment

    useEffect(() => {
        return () => {
        }
    }, [])

    const _renderHeader = (user, userCurrentStatus = 'Away') => {
        return (
            <View style={headerStyles.primaryContainer}>
                <View style={headerStyles.backPrimaryContainer}>
                    <TouchableOpacity style={headerStyles.backContainer}
                        onPress={() => { navigation.pop() && navigation.goBack(); }}>
                        <FastImage
                            source={LocalAssets.ICON.videoPlayBack}
                            style={headerStyles.imageBack}
                            resizeMode={FastImage.resizeMode.contain} />
                    </TouchableOpacity>

                    <View style={headerStyles.userImageContainer}>
                        <ProfilePicture source={user?.profile} />
                    </View>

                    <View>
                        <Text style={headerStyles.username}>{`${printTextWithLimit(user?.name, "", 35)}`}</Text>
                        <Text style={headerStyles.userStatus}>{`${printText(userCurrentStatus)}`}</Text>
                    </View>

                </View>



                <Menu style={headerStyles.optionPrimaryContainer}>
                    <MenuTrigger>
                        <FastImage
                            source={LocalAssets.ICON.videoPlayOption}
                            style={headerStyles.imageOption}
                            resizeMode={FastImage.resizeMode.contain} />
                    </MenuTrigger>
                    <MenuOptions
                        customStyles={optionsStyles}>



                    </MenuOptions>
                </Menu>

            </View>
        )
    };//end of _renderHeader

    const onSend = () => {
        const text = giftedChatRef?.state.text;
        console.log(`On send text is==> ${text}`);
        giftedChatRef?.textInput.clear();
    };//end of onSend



    return (
        <View style={styles.primaryContainer}>
            <Statusbar barStyle={"dark-content"} backgroundColor={colors.statusbar} />
            <TweeHeader customHeader={() => _renderHeader(user, data.user?.statusText)} bottomBorder />


            <GiftedChat
                ref={r => giftedChatRef = r}
                style={{ flex: 1 }}
                messages={data.chat}

                user={{
                    _id: data.user.myUserID,
                }}

                key={HelperFunctions.keyExtractor()}
                bottomOffset={50}

                textInputStyle={styles.textInput}
                textInputProps={{

                    autoFocus: false,
                    autoCompleteType: "off",

                    enablesReturnKeyAutomatically: false,
                    underlineColorAndroid: "transparent",

                    keyboardType: "default",

                    returnKeyType: "next",
                    returnKeyLabel: "enter",

                    autoCorrect: false,
                    inputAccessoryViewID: "doneKeyboardButton",

                    placeholder: IMLocalized(`Ask your question`),
                    placeholderTextColor: "#666666",

                    autoFocus: true,

                }}

                renderTime={() => null}

                renderActions={() => {
                    return (
                        <TouchableOpacity
                            onPress={showAttachment}
                            style={styles.attachmentContainer}>
                            <FastImage
                                source={LocalAssets.ICON.attachment}
                                style={styles.attachmentImage}
                                resizeMode={FastImage.resizeMode.contain} />
                        </TouchableOpacity>
                    )
                }}
                renderSend={() => {
                    return (
                        <TouchableOpacity style={styles.sendContainer}
                            onPress={onSend}>
                            <FastImage
                                source={LocalAssets.ICON.send}
                                style={styles.sendImage}
                                resizeMode={FastImage.resizeMode.contain}
                                tintColor={colors.white} />
                        </TouchableOpacity>
                    )
                }}

                isCustomViewBottom={true}

                isKeyboardInternallyHandled
            />

            {/* ******************** ATTACHMENT MODEL Start ******************** */}
            <BottomModal
                ref={r => attachmentRef = r}
                useRefState>
                <FlatList
                    data={attachmentData}
                    numColumns={3}
                    style={attachmentStyles.flatList}
                    contentContainerStyle={attachmentStyles.contentContainerStyle}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity style={[attachmentStyles.primaryContainer, {
                                backgroundColor: item.color,
                            }]}
                                onPress={() => {
                                    hideAttachment();
                                }}>
                                <FastImage
                                    source={item.image}
                                    style={attachmentStyles.image}
                                    resizeMode={FastImage.resizeMode.contain}
                                    tintColor={colors.white} />
                                <Text style={attachmentStyles.text}>{item.title}</Text>
                            </TouchableOpacity>
                        )
                    }} />
            </BottomModal>

            {/* ******************** ATTACHMENT MODEL End ******************** */}

            {/* ******************** KEYBOARD DONE BUTTON Start ******************** */}
            <InputAccessoryView nativeID={"doneKeyboardButton"}>
                <View style={styles.accessory}>
                    <Button
                        onPress={() => Keyboard.dismiss()}
                        title={IMLocalized(`Done`)}
                    />
                </View>
            </InputAccessoryView>

            {/* ******************** KEYBOARD DONE BUTTON End ******************** */}

        </View>
    );
}//end of index

export default index;


const optionsStyles = {
    optionsContainer: {
        backgroundColor: 'transparent',
        paddingRight: 20,
        paddingTop: 10,
        minWidth: 230,
    },
    optionsWrapper: {
        backgroundColor: colors.white,
    },
    optionWrapper: {
        backgroundColor: colors.white,
        margin: 5,
    },
    optionTouchable: {
        underlayColor: `rgba(0,0,0,0.04)`,
        activeOpacity: 70,
    },
    optionText: {
        color: colors.black,
        fontSize: 12,
    },
};