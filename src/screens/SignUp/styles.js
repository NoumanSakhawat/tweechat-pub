import { StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';


export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    body: {
        flex: 1,
    },
    heading: {
        color: "#221F2E",
        fontSize: 24,
        fontWeight: "bold",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 69,
        paddingHorizontal: 0,
        marginBottom: 20
    },



    //TEXT INPUT
    containerStyle: {
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 8,
        alignItems: "center",
    },
    inputContainerStyle: {
        borderColor: "#A7AAB1",
        borderWidth: 1,
        borderRadius: 8,
    },
    textinput: {
        fontSize: 16,
        color: "#0F1117",
    },


    //BUTTON
    buttonContainer: {
        marginTop: 12,
        marginHorizontal: AppStyles.HORIZONTAL,
    },
    button: {
        alignSelf: "center",
    },

    //BUTTOM BUTTON
    bottomButtonContainer: {
        alignSelf: "center",
        marginBottom: 12,
        flexDirection: "row",
        alignItems: "center",
        marginTop: 12,
    },
    bottomButtonText: {
        color: "#221F2E",
        fontSize: 16,
    },
    bottomButtonText1: {
        color: colors.primary,
        fontSize: 16,
        marginLeft: 4,
    },
});//end of styles

