import React, { useEffect, useState } from 'react';
import { KeyboardAvoidingView, ScrollView, TouchableOpacity, View } from 'react-native';
import ImageSelector from '../../AppComponents/ImageSelector';
import Button from '../../components/Button';
import Text from '../../components/Text';
import TextInput from '../../components/TextInput';
import VectorIcon from '../../components/VectorIcon';
import { IMLocalized } from '../../locales/IMLocalization';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import { styles } from './styles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TextField from '../../../lib/OutlinedTextField/field';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import colors from '../../constants/colors';
import ROUTES from '../../routes/ROUTES';
import Constants from '../../constants/Constants';


const index = ({ navigation }) => {

    const [email, setemail] = useState('');
    const [emailError, setemailError] = useState('');

    const [password, setpassword] = useState('');
    const [passwordError, setpasswordError] = useState('');

    const [rePassword, setRePassword] = useState('');
    const [rePasswordError, setRePasswordError] = useState('');

    const [fullname, setFullname] = useState('');
    const [fullnameError, setFullnameError] = useState('');

    const [secureTextEntry, setSecureTextEntry] = useState(true);

    const [focusedIndex, setFocusedIndex] = useState(-1);

    useEffect(async () => {

    }, [])



    const onAccessoryPress = () => {
        setSecureTextEntry(!secureTextEntry)
    };//end of onAccessoryPress

    const renderPasswordAccessory = () => {
        const name = secureTextEntry ? 'visibility' : 'visibility-off';

        return (
            <MaterialIcons
                size={24}
                name={name}
                color={TextField.defaultProps.baseColor}
                onPress={onAccessoryPress}
                suppressHighlighting={true}
            />
        );
    };//end of renderPasswordAccessory

    const signinPress = () => {
        navigation.pop() && navigation.goBack();
    };//end of signinPress

    const signupPress = () => {
        navigation.navigate(ROUTES.Home);
    };//end of signupPress

    let fullNameRef = React.createRef();
    let emailRef = React.createRef();
    let passwordRef = React.createRef();
    let rePasswordRef = React.createRef();

    return (
        <View style={styles.primaryContainer}>

            <KeyboardAwareScrollView style={{ flex: 1, }}
                keyboardShouldPersistTaps='handled'
                extraScrollHeight={80}>

                <View style={styles.body}>

                    <Text style={styles.heading}>{IMLocalized(`Create\nnew Account`)}</Text>



                    {/* ******************** FULL NAME INPUT Start ******************** */}
                    <TextInput
                        label={''}
                        customRef={fullNameRef}
                        baseColor={'#A7AAB1'}
                        textColor={'#0F1117'}
                        labelFontSize={12}
                        fontSize={16}
                        showDone
                        placeholder={IMLocalized(`Full Name`)}
                        maxLength={Constants.SIGN_UP_LENGTH.FULL_NAME}
                        defaultValue={fullname}
                        onChangeText={(text) => { setFullname(text); setFullnameError('') }}
                        lineType={"none"}
                        containerStyle={styles.containerStyle}
                        onFocus={() => { setFocusedIndex(0) }}
                        onBlur={() => { setFocusedIndex(-1) }}
                        inputContainerStyle={[styles.inputContainerStyle, {
                            borderColor: focusedIndex === 0 ? colors.primary : "#A7AAB1",
                        }]}
                        style={styles.textinput}
                        error={fullnameError}
                        returnKeyType={"next"}
                        onSubmitEditing={() => {
                            if (emailRef !== null)
                                emailRef.current.onPress();
                        }}
                    />

                    {/* ******************** FULL NAME INPUT End ******************** */}

                    {/* ******************** EMAIL INPUT Start ******************** */}
                    <TextInput
                        label={''}
                        customRef={emailRef}
                        baseColor={'#A7AAB1'}
                        textColor={'#0F1117'}
                        labelFontSize={12}
                        fontSize={16}
                        showDone
                        placeholder={IMLocalized(`Email`)}
                        maxLength={Constants.SIGN_UP_LENGTH.EMAIL}
                        defaultValue={email}
                        onChangeText={(text) => { setemail(text); setemailError('') }}
                        lineType={"none"}
                        containerStyle={styles.containerStyle}
                        onFocus={() => { setFocusedIndex(1) }}
                        onBlur={() => { setFocusedIndex(-1) }}
                        inputContainerStyle={[styles.inputContainerStyle, {
                            borderColor: focusedIndex === 1 ? colors.primary : "#A7AAB1",
                        }]}
                        style={styles.textinput}
                        error={emailError}
                        returnKeyType={"next"}
                        onSubmitEditing={() => {
                            if (passwordRef !== null)
                                passwordRef.current.onPress();
                        }}
                    />

                    {/* ******************** EMAIL INPUT End ******************** */}

                    {/* ******************** PASSWORD INPUT Start ******************** */}
                    <TextInput
                        label={''}
                        customRef={passwordRef}
                        baseColor={'#A7AAB1'}
                        textColor={'#0F1117'}
                        labelFontSize={12}
                        fontSize={16}
                        showDone
                        placeholder={IMLocalized(`Password`)}
                        maxLength={Constants.SIGN_UP_LENGTH.PASSWORD}
                        defaultValue={password}
                        onChangeText={(text) => { setpassword(text); setpasswordError('') }}
                        lineType={"none"}
                        containerStyle={styles.containerStyle}
                        onFocus={() => { setFocusedIndex(2) }}
                        onBlur={() => { setFocusedIndex(-1) }}
                        inputContainerStyle={[styles.inputContainerStyle, {
                            borderColor: focusedIndex === 2 ? colors.primary : "#A7AAB1",
                        }]}
                        style={styles.textinput}
                        error={passwordError}
                        secureTextEntry={secureTextEntry}
                        renderRightAccessory={renderPasswordAccessory}
                        returnKeyType={"next"}
                        onSubmitEditing={() => {
                            if (rePasswordRef !== null)
                                rePasswordRef.current.onPress();
                        }}
                    />

                    {/* ******************** PASSWORD INPUT End ******************** */}

                    {/* ******************** RE-PASSWORD INPUT Start ******************** */}
                    <TextInput
                        label={''}
                        customRef={rePasswordRef}
                        baseColor={'#A7AAB1'}
                        textColor={'#0F1117'}
                        labelFontSize={12}
                        fontSize={16}
                        showDone
                        placeholder={IMLocalized(`Re-password`)}
                        maxLength={Constants.SIGN_UP_LENGTH.RE_PASSWORD}
                        defaultValue={rePassword}
                        onChangeText={(text) => { setRePassword(text); setRePasswordError('') }}
                        lineType={"none"}
                        containerStyle={styles.containerStyle}
                        onFocus={() => { setFocusedIndex(3) }}
                        onBlur={() => { setFocusedIndex(-1) }}
                        inputContainerStyle={[styles.inputContainerStyle, {
                            borderColor: focusedIndex === 3 ? colors.primary : "#A7AAB1",
                        }]}
                        style={styles.textinput}
                        error={rePasswordError}
                        secureTextEntry={secureTextEntry}
                        renderRightAccessory={renderPasswordAccessory}
                    />

                    {/* ******************** RE-PASSWORD INPUT End ******************** */}

                    {/* ******************** SIGN IN BUTTON Start ******************** */}
                    <View style={styles.buttonContainer}>
                        <Button
                            text={IMLocalized(`Sign Up`)}
                            containerStyle={styles.button}
                            width={"100%"}
                            onPress={signupPress}
                            textTransform={"capitalize"}
                        />
                    </View>

                    {/* ******************** SIGN IN BUTTON End ******************** */}

                    {/* ******************** DO'NT HAVE ACCOUNT Start ******************** */}
                    <View style={styles.bottomButtonContainer}>
                        <Text style={styles.bottomButtonText}>{IMLocalized(`Already have an account?`)}</Text>
                        <TouchableOpacity
                            onPress={signinPress}>
                            <Text style={styles.bottomButtonText1}>{IMLocalized(`Sign In`)}</Text>
                        </TouchableOpacity>
                    </View>

                    {/* ******************** DO'NT HAVE ACCOUNT End ******************** */}

                </View>

            </KeyboardAwareScrollView>


        </View>
    );
}//end of index

export default index;

