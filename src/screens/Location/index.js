import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View, TextInput, FlatList, Image, Keyboard } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import StaticData from '../../constants/StaticData';
import { IMLocalized } from '../../locales/IMLocalization';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { headerStyles, searchItemStyles, styles } from './styles';
import LinearGradient from 'react-native-linear-gradient';
import ENV from '../../utils/ENV';
import AppFunctions from '../../helper/AppFunctions';
import LocationFunctions from '../../helper/LocationFunctions';
import HelperFunctions, { emptyValidate, printText } from '../../helper/HelperFunctions';
import FastImage from 'react-native-fast-image';
import LocalAssets from '../../constants/LocalAssets';
import colors from '../../constants/colors';


const index = ({ navigation, route }) => {
    const [inputSearch, setInputSearch] = useState('');
    const [data, setData] = useState([]);
    const [defaultData, setDefaultData] = useState([]);
    const [metaData, setMetaData] = useState(false);
    const [isSearchUsed, setIsSearchUsed] = useState(false);

    const searchPlace = async (text, type = "explore") => {
        try {
            const URL = `${ENV.FOURSQUARE_URL}venues/${text}&v=${new Date().getTime()}&client_id=${ENV.FOURSQUARE_CLIENT_ID}&client_secret=${ENV.FOURSQUARE_CLIENT_SECRET}`;

            const resJson = await fetch(URL);
            const res = await resJson.json();
            if (res.meta.code !== 200) {
                AppFunctions.errorFlashMessage(res.meta.errorDetail);
                return;
            }
            const filtered = type === "search" ?
                res.response.venues
                :
                res.response.groups.reduce((a, o) => (o.name === "recommended" && a.push(o.items), a), [])[0];

            if (type === "search") {
                setIsSearchUsed(true);
            } else {
                setIsSearchUsed(false);
            }




            let newFilteredArr = [];
            if (route.params.selectedLocation !== null) {

                const selectedArr = [{
                    ...route.params.selectedLocation.data,
                    isSearchUsed: route.params.selectedLocation.isSearchUsed,
                    selected: true,
                }];

                const selectedID = route.params.selectedLocation.id;
                const myArray = route.params.selectedLocation.isSearchUsed ? filtered.filter(i => i.id !== selectedID) :
                    filtered.filter(i => i.venue.id !== selectedID);
                newFilteredArr = [...selectedArr, ...myArray];

            } else {
                newFilteredArr = filtered;
            }

            if (defaultData.length < 1) {
                setDefaultData(newFilteredArr);
            }

            setData(newFilteredArr);
            setMetaData(!metaData);
            flatListRef.current.scrollToOffset({ animated: true, offset: 0 });

        } catch (error) {
            AppFunctions.errorFlashMessage(IMLocalized(`Something went wrong!`), IMLocalized(`Please try again!`));
            console.warn(error);
        }
    };//end of searchPlace

    const setCurrentLocation = async () => {
        const locationRes = await LocationFunctions.getCurrentLocation();
        if (locationRes) {
            const latitude = locationRes.coords.latitude;
            const longitude = locationRes.coords.longitude;
            searchPlace(`explore?ll=${latitude},${longitude}`)
        }

    };//end of setCurrentLocation

    useEffect(async () => {
        setCurrentLocation();

        return () => {
        };
    }, []);

    const _renderHeader = () => {
        return (
            <View style={headerStyles.primaryContainer}>

                <View style={headerStyles.backPrimaryContainer}>
                    <TouchableOpacity style={headerStyles.backContainer}
                        onPress={() => { navigation.pop() && navigation.goBack(); }}>
                        <FastImage
                            source={LocalAssets.ICON.videoPlayBack}
                            style={headerStyles.imageBack}
                            resizeMode={FastImage.resizeMode.contain} />
                    </TouchableOpacity>
                </View>

                <View style={headerStyles.textinputContainer}>
                    <TextInput
                        style={headerStyles.textinput}
                        placeholder={IMLocalized(`Search locations`)}
                        placeholderTextColor={"#666666"}
                        autoCorrect={false}
                        value={inputSearch}
                        onChangeText={(text) => { setInputSearch(text) }}
                        keyboardType={"web-search"}
                        onSubmitEditing={() => {
                            if (emptyValidate(inputSearch)) {
                                searchPlace(`search?near=${inputSearch}`, "search")
                            } else {
                                AppFunctions.warningFlashMessage(IMLocalized(`Please, enter location name to search`));
                            }
                        }}
                    />

                    <TouchableOpacity style={headerStyles.searchIcon}
                        onPress={() => {
                            if (emptyValidate(inputSearch)) {
                                searchPlace(`search?near=${inputSearch}`, "search")
                            } else {
                                AppFunctions.warningFlashMessage(IMLocalized(`Please, enter location name to search`));
                            }

                        }}>
                        <VectorIcon name={"search"} />
                    </TouchableOpacity>

                </View>

                <TouchableOpacity onPress={() => {
                    Keyboard.dismiss();
                    setInputSearch('');
                    setIsSearchUsed(false);
                    if (defaultData.length > 0) {
                        setData(defaultData);
                        setMetaData(!metaData);

                    } else {
                        setData([]);
                        setMetaData(!metaData);
                        setCurrentLocation();
                    }
                    flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
                }}>
                    <VectorIcon
                        style={headerStyles.clearIcon}
                        name={"close"}
                        size={24} />
                </TouchableOpacity>
            </View>
        )
    };//end of _renderHeader

    const flatListRef = React.useRef();

    return (
        <View style={styles.primaryContainer}>
            <View style={headerStyles.shadow}>
                <TweeHeader customHeader={_renderHeader} shadow={false} />
            </View>
            <FlatList
                ref={flatListRef}
                data={data}
                extraData={metaData}
                keyExtractor={HelperFunctions.keyExtractor}
                contentContainerStyle={styles.contentContainerStyle}
                renderItem={({ item, index }) => {
                    let isSelected = "selected" in item ? item.selected : false;
                    const itemIsSearchUsed = isSelected ? item.isSearchUsed : isSearchUsed;

                    return (
                        <TouchableOpacity style={[styles.iContainer,
                        isSelected && styles.iContainerSelected
                        ]}
                            onPress={() => {
                                route.params.updateLocation({
                                    name: itemIsSearchUsed ? printText(item.name) : printText(item.venue.name),
                                    latitude: itemIsSearchUsed ? item.location.lat : item.venue.location.lat,
                                    longitude: itemIsSearchUsed ? item.location.lng : item.venue.location.lng,
                                    id: itemIsSearchUsed ? printText(item.id) : printText(item.venue.id),
                                    data: item,
                                    isSearchUsed: itemIsSearchUsed,
                                }, true);
                                navigation.pop() && navigation.goBack();
                            }}>
                            <View style={{ flex: 1 }}>
                                <Text style={[styles.iName,
                                isSelected && styles.iNameSelected
                                ]}>{itemIsSearchUsed ? printText(item.name) : printText(item.venue.name)}</Text>
                                <Text style={[styles.iLocation,
                                isSelected && styles.iLocationSelected
                                ]}>{itemIsSearchUsed ?
                                    "categories" in item && item.categories.length > 0 ? printText(item.categories[0].name) : ''
                                    :
                                    `${printText(item.venue.location.address)} - ${printText(item.venue.location.distance, 0)}m`}</Text>

                                <View style={styles.iSeperator} />
                            </View>

                            {isSelected &&
                                <TouchableOpacity style={[styles.closeIconContainer]}
                                    onPress={() => {
                                        route.params.updateLocation(null, false);
                                        navigation.pop() && navigation.goBack();
                                    }}>
                                    <VectorIcon
                                        name={"close"}
                                        size={25}
                                        color={colors.white} />
                                </TouchableOpacity>
                            }
                        </TouchableOpacity>

                    )
                }} />
        </View>
    )
}//end of index

export default index;



