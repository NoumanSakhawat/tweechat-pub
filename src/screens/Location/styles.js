import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const WIDTH = Dimensions.get('window').width;

const IMAGE_SIZE = WIDTH / 2.2;

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    contentContainerStyle: {

    },
    iContainer: {
        marginTop: 12,
        marginHorizontal: 12,
    },
    iContainerSelected: {
        marginHorizontal: 0,
        paddingHorizontal: 12,
        backgroundColor: colors.primary,
        flexDirection: 'row',
        alignItems: "center",
    },
    iName: {
        color: "#272729",
        fontWeight: "600",
        fontSize: 16,
    },
    iNameSelected: {
        color: colors.white,
        fontWeight: "600",
        fontSize: 16,
    },
    iLocation: {
        color: "#898A8D",
        fontSize: 12,
        marginBottom: 12,
    },
    iLocationSelected: {
        color: "rgba(255,255,255,0.5)",
        fontSize: 12,
        marginBottom: 12,
    },
    iSeperator: {
        ...AppStyles.border,
    },

    closeIconContainer: {
        paddingLeft: 12,
    },
});//end of styles


export const headerStyles = StyleSheet.create({
    primaryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 12,
        marginBottom: 12,
    },
    backPrimaryContainer: {

    },
    backContainer: {
        marginLeft: 20,
        // paddingVertical: 7,
    },
    imageBack: {
        height: 20,
        width: 20,
    },

    textinputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    textinput: {
        backgroundColor: "#EDF0F7",
        padding: 12,
        marginLeft: 12,
        marginRight: 0,
        borderRadius: 8,
        width: "95%",
        color: "#6B6B6B",
        fontSize: 12,
    },
    clearIcon: {
        paddingRight: 20,
        paddingLeft: 8
    },
    searchIcon: {
        position: "absolute",
        right: 12,
    },
    shadow: {
        shadowColor: 'rgba(0, 0, 0, 0.3)',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    },
});//end of headerStyles
