import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const WIDTH = Dimensions.get('window').width;

const IMAGE_SIZE = WIDTH / 2.2;

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    contentContainerStyle: {
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        marginHorizontal: 16,
    },
    iContainer: {
        borderRadius: 4,
        marginBottom: 16,
        backgroundColor: `rgba(0,0,0,0.3)`,
    },
    image: {
        height: IMAGE_SIZE,
        width: IMAGE_SIZE,
        backgroundColor: `rgba(0,0,0,0.3)`,
    },
    iOverlay: {
        backgroundColor: `rgba(0,0,0,0.5)`,
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
    },
    iTitle: {
        color: colors.white,
        fontSize: 14,
        padding: 12,
        paddingTop: 12,
        fontWeight: "500",
    },
});//end of styles

export const searchItemStyles = StyleSheet.create({
    primaryContainer: {
        borderRadius: 4,
        marginBottom: 16,
        backgroundColor: `rgba(0,0,0,0.3)`,
    },
    image: {
        height: IMAGE_SIZE,
        width: IMAGE_SIZE,
        backgroundColor: `rgba(0,0,0,0.3)`,
    },
});//end of searchItemStyles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 12,
        marginBottom: 12,
    },
    backPrimaryContainer: {

    },
    backContainer: {
        marginLeft: 20,
        // paddingVertical: 7,
    },
    imageBack: {
        height: 20,
        width: 20,
    },

    textinputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    textinputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    textinput: {
        backgroundColor: "#EDF0F7",
        padding: 12,
        marginLeft: 12,

        marginRight: 0,
        borderRadius: 8,
        width: "95%",
        color: "#6B6B6B",
        fontSize: 12,
    },
    clearIcon: {
        paddingRight: 20,
        paddingLeft: 8
    },
    searchIcon: {
        position: "absolute",
        right: 12,
    },
    shadow: {
        shadowColor: 'rgba(0, 0, 0, 0.3)',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    },
});//end of headerStyles
