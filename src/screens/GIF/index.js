import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View, TextInput, FlatList, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import StaticData from '../../constants/StaticData';
import { IMLocalized } from '../../locales/IMLocalization';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { headerStyles, searchItemStyles, styles } from './styles';
import LinearGradient from 'react-native-linear-gradient';
import ENV from '../../utils/ENV';
import AppFunctions from '../../helper/AppFunctions';
import { emptyValidate } from '../../helper/HelperFunctions';
import { Keyboard } from 'react-native';
import FastImage from 'react-native-fast-image';
import LocalAssets from '../../constants/LocalAssets';

const cColor = {
    icon: `rgba(76, 158, 235, 0.5)`,
    iconSelected: `rgba(76, 158, 235, 1)`
}



const index = ({ navigation, route }) => {
    const [inputSearch, setInputSearch] = useState('');
    const [data, setData] = useState(StaticData.gifCategories());
    const [metaData, setMetaData] = useState(false);
    const [isSearched, setIsSearched] = useState(false);


    const searchGif = async (text) => {
        try {
            const URL = `${ENV.GIPHY_URL}${text}`;
            const resJson = await fetch(URL);
            const res = await resJson.json();

            setData(res.data);
            setIsSearched(true);
            setMetaData(!metaData);
            flatListRef.current.scrollToOffset({ animated: true, offset: 0 });

        } catch (error) {
            setIsSearched(false);
            AppFunctions.errorFlashMessage(IMLocalized(`Something went wrong!`), IMLocalized(`Please try again!`));
            console.warn(error);
        }
    };//end of searchGif

    const _renderHeader = () => {
        return (
            <View style={headerStyles.primaryContainer}>
                <View style={headerStyles.backPrimaryContainer}>
                    <TouchableOpacity style={headerStyles.backContainer}
                        onPress={() => { navigation.pop() && navigation.goBack(); }}>
                        <FastImage
                            source={LocalAssets.ICON.videoPlayBack}
                            style={headerStyles.imageBack}
                            resizeMode={FastImage.resizeMode.contain} />
                    </TouchableOpacity>
                </View>

                <View style={headerStyles.textinputContainer}>
                    <TextInput
                        style={headerStyles.textinput}
                        placeholder={IMLocalized(`Search for GIFs`)}
                        placeholderTextColor={"#666666"}
                        autoCorrect={false}
                        value={inputSearch}
                        onChangeText={(text) => { setInputSearch(text) }}
                        keyboardType={"web-search"}
                        onSubmitEditing={() => {
                            if (emptyValidate(inputSearch)) {
                                searchGif(inputSearch)
                            } else {
                                AppFunctions.warningFlashMessage(IMLocalized(`Please, first enter for search`));
                            }
                        }}
                    />

                    <TouchableOpacity style={headerStyles.searchIcon}
                        onPress={() => {
                            if (emptyValidate(inputSearch)) {
                                searchGif(inputSearch)
                            } else {
                                AppFunctions.warningFlashMessage(IMLocalized(`Please, first enter for search`));
                            }

                        }}>
                        <VectorIcon name={"search"} />
                    </TouchableOpacity>

                </View>

                <TouchableOpacity onPress={() => {
                    Keyboard.dismiss();
                    setInputSearch('');
                    setIsSearched(false);
                    setData(StaticData.gifCategories());
                    setMetaData(!metaData);
                    flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
                }}>
                    <VectorIcon
                        style={headerStyles.clearIcon}
                        name={"close"}
                        size={24} />
                </TouchableOpacity>
            </View>
        )
    };//end of _renderHeader

    const _renderSearchItem = (item, index) => {

        return (
            <TouchableOpacity style={[searchItemStyles.primaryContainer, {
                ...index % 2 === 0 && { marginRight: 16, }
            }]}
                onPress={() => {
                    route.params.updateGif(item.images.original.url);
                    navigation.pop() && navigation.goBack();
                }}>
                <Image
                    source={{ uri: item.images.original.url }}
                    style={styles.image}
                    borderRadius={4} />

            </TouchableOpacity>
        )
    };//end of _renderSearchItem

    const flatListRef = React.useRef();

    return (
        <View style={styles.primaryContainer}>
            <View style={headerStyles.shadow}>
                <TweeHeader customHeader={_renderHeader} shadow={false} />
            </View>
            <FlatList
                ref={flatListRef}
                data={data}
                numColumns={2}
                extraData={metaData}
                contentContainerStyle={styles.contentContainerStyle}
                renderItem={({ item, index }) => {
                    if (isSearched) return _renderSearchItem(item, index)
                    return (
                        <TouchableOpacity style={[styles.iContainer, {
                            ...index % 2 === 0 && { marginRight: 16, }
                        }]}
                            onPress={() => {
                                setInputSearch(item.title);
                                searchGif(item.title);
                            }}>
                            <Image
                                source={{ uri: item.thumbnail }}
                                style={styles.image}
                                borderRadius={4} />

                            <LinearGradient
                                // start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }}
                                locations={[0.1, 0.5, 1]}
                                colors={[
                                    `rgba(0,0,0,0.0)`,
                                    `rgba(0,0,0,0.2)`,
                                    `rgba(0,0,0,0.5)`
                                ]} style={styles.iOverlay}>
                                <Text style={styles.iTitle}>{item.title}</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    )
                }} />
        </View>
    )
}//end of index

export default index;



