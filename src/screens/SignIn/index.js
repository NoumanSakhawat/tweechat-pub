import React, { useEffect, useState } from 'react';
import { KeyboardAvoidingView, ScrollView, TouchableOpacity, View } from 'react-native';
import ImageSelector from '../../AppComponents/ImageSelector';
import Button from '../../components/Button';
import Text from '../../components/Text';
import TextInput from '../../components/TextInput';
import VectorIcon from '../../components/VectorIcon';
import { IMLocalized } from '../../locales/IMLocalization';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import { styles } from './styles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TextField from '../../../lib/OutlinedTextField/field';
import ROUTES from '../../routes/ROUTES';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import colors from '../../constants/colors';
import Constants from '../../constants/Constants';


const index = ({ navigation, setIsLoggedIn }) => {

    const [email, setemail] = useState('');
    const [emailError, setemailError] = useState('');

    const [password, setpassword] = useState('');
    const [passwordError, setpasswordError] = useState('');

    const [secureTextEntry, setSecureTextEntry] = useState(true);

    const [focusedIndex, setFocusedIndex] = useState(-1);

    useEffect(async () => {

    }, [])



    const onAccessoryPress = () => {
        setSecureTextEntry(!secureTextEntry)
    };//end of onAccessoryPress

    const renderPasswordAccessory = () => {
        let name = secureTextEntry ?
            'visibility' :
            'visibility-off';

        return (
            <MaterialIcons
                size={24}
                name={name}
                color={TextField.defaultProps.baseColor}
                onPress={onAccessoryPress}
                suppressHighlighting={true}
            />
        );
    };//end of renderPasswordAccessory

    const signinPress = () => {
        setIsLoggedIn(true);
    };//end of signinPress

    const signupPress = () => {
        navigation.navigate(ROUTES.SignUp);
    };//end of signupPress

    const forgotPasswordPress = () => {
        navigation.navigate(ROUTES.ForgotPassword);
    };//end of forgotPasswordPress

    return (
        <View style={styles.primaryContainer}>

            {/* <KeyboardAvoidingView style={{ flex: 1, }}
                behavior="height"
                enabled>
                <ScrollView contentContainerStyle={{ flex: 1, }} scrollEnabled={false}
                    nestedScrollEnabled
                    keyboardDismissMode='interactive'
                    keyboardShouldPersistTaps='handled'> */}
            <KeyboardAwareScrollView style={{ flex: 1, }}
                keyboardShouldPersistTaps='handled'
                extraScrollHeight={100}>

                <View style={styles.body}>

                    <Text style={styles.heading}>{IMLocalized(`Sign In\nyour Account`)}</Text>



                    {/* ******************** EMAIL INPUT Start ******************** */}
                    <TextInput
                        label={''}
                        baseColor={'#A7AAB1'}
                        textColor={'#0F1117'}
                        labelFontSize={12}
                        fontSize={16}
                        showDone
                        placeholder={IMLocalized(`Email`)}
                        maxLength={Constants.SIGN_IN_LENGTH.EMAIL}
                        value={email}
                        onChangeText={(text) => { setemail(text); setemailError('') }}
                        lineType={"none"}
                        containerStyle={styles.containerStyle}
                        onFocus={() => { setFocusedIndex(0) }}
                        onBlur={() => { setFocusedIndex(-1) }}
                        inputContainerStyle={[styles.inputContainerStyle, {
                            borderColor: focusedIndex === 0 ? colors.primary : "#A7AAB1",
                        }]}
                        style={styles.textinput}
                        error={emailError}
                    />

                    {/* ******************** EMAIL INPUT End ******************** */}

                    {/* ******************** PASSWORD INPUT Start ******************** */}
                    <TextInput
                        label={''}
                        baseColor={'#A7AAB1'}
                        textColor={'#0F1117'}
                        labelFontSize={12}
                        fontSize={16}
                        showDone
                        placeholder={IMLocalized(`Password`)}
                        maxLength={Constants.SIGN_IN_LENGTH.PASSWORD}
                        value={password}
                        onChangeText={(text) => { setpassword(text); setpasswordError('') }}
                        lineType={"none"}
                        containerStyle={styles.containerStyle}
                        onFocus={() => { setFocusedIndex(1) }}
                        onBlur={() => { setFocusedIndex(-1) }}
                        inputContainerStyle={[styles.inputContainerStyle, {
                            borderColor: focusedIndex === 1 ? colors.primary : "#A7AAB1",
                        }]}
                        style={styles.textinput}
                        error={passwordError}
                        secureTextEntry={secureTextEntry}
                        renderRightAccessory={renderPasswordAccessory}
                    />

                    {/* ******************** PASSWORD INPUT End ******************** */}


                    {/* ******************** FORGOT PASSWORD Start ******************** */}
                    <TouchableOpacity style={styles.forgotPasswordContainer}
                        onPress={forgotPasswordPress}>
                        <Text style={styles.forgotPassword}>{IMLocalized(`Forget Password?`)}</Text>
                    </TouchableOpacity>

                    {/* ******************** FORGOT PASSWORD End ******************** */}

                    {/* ******************** SIGN IN BUTTON Start ******************** */}
                    <View style={styles.buttonContainer}>
                        <Button
                            text={IMLocalized(`Sign in`)}
                            containerStyle={styles.button}
                            width={"100%"}
                            onPress={signinPress}
                            textTransform={"capitalize"}
                        />
                    </View>

                    {/* ******************** SIGN IN BUTTON End ******************** */}

                    {/* ******************** DO'NT HAVE ACCOUNT Start ******************** */}
                    <View style={styles.bottomButtonContainer}>
                        <Text style={styles.bottomButtonText}>{IMLocalized(`Don’t have an Account?`)}</Text>
                        <TouchableOpacity
                            onPress={signupPress}>
                            <Text style={styles.bottomButtonText1}>{IMLocalized(`Sign Up`)}</Text>
                        </TouchableOpacity>
                    </View>

                    {/* ******************** DO'NT HAVE ACCOUNT End ******************** */}

                </View>

            </KeyboardAwareScrollView>
            {/* </ScrollView>
            </KeyboardAvoidingView> */}


        </View>
    );
}//end of index

export default index;

