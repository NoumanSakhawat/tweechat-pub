import React, { useEffect, useRef, useState } from 'react';
import { KeyboardAvoidingView, ScrollView, TouchableOpacity, View, findNodeHandle, Keyboard } from 'react-native';
import FastImage from 'react-native-fast-image';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImageSelector from '../../AppComponents/ImageSelector';
import Button from '../../components/Button';
import Text from '../../components/Text';
import TextInput from '../../components/TextInput';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import LocalAssets from '../../constants/LocalAssets';
import { IMLocalized } from '../../locales/IMLocalization';
import ROUTES from '../../routes/ROUTES';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { headerStyles, styles } from './styles';

const SCROLL_DIFF = {
    first: 80,
    second: 100,
}

const index = ({ navigation }) => {

    const [firstName, setFirstName] = useState('');
    const [firstNameError, setFirstNameError] = useState('');

    const [lastName, setLastName] = useState('');
    const [lastNameError, setLastNameError] = useState('');

    const [focusedIndex, setFocusedIndex] = useState(-1);

    const [profilePicture, setProfilePicture] = useState(LocalAssets.ICON.setupProfile);

    const [imageSelectorVisible, setImageSelectorVisible] = useState(false);

    const [isKeyboardVisible, setKeyboardVisible] = useState(false);


    const showImageSelector = () => { setImageSelectorVisible(true) };
    const hideImageSelector = () => { setImageSelectorVisible(false) };

    useEffect(async () => {
        const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
            setKeyboardVisible(true); // or some other action
        });

        const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
            setKeyboardVisible(false); // or some other action
        });

        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };
    }, [])

    const onNextPress = () => {
        navigation.navigate(ROUTES.SignIn)
    };//end of onNextPress

    const getImages = (imagesArr) => {
        if (imagesArr.length > 0) {
            setProfilePicture({ uri: imagesArr[0].path })
        }
    };//end of getImages

    let scrollViewRef = useRef();
    let lastNameRef = React.createRef();

    return (
        <View style={styles.primaryContainer}>

            <TweeHeader customHeader={() => _renderHeader(navigation)} />

            <KeyboardAvoidingView style={{ flex: 1, }}
                behavior={"padding"}
                enabled>

                <ScrollView
                    contentContainerStyle={{ flex: 1, }}
                    nestedScrollEnabled
                    keyboardDismissMode='interactive'
                    keyboardShouldPersistTaps='handled'

                    scrollToOverflowEnabled={true}
                    ref={scrollViewRef}
                >


                    <View style={styles.body}>

                        {!isKeyboardVisible &&
                            <Text style={styles.heading}>{IMLocalized(`Set up your profile`)}</Text>
                        }

                        {/* ******************** PROFILE PICTURE Start ******************** */}
                        {!isKeyboardVisible &&
                            <TouchableOpacity style={styles.profilePictureContainer}
                                onPress={showImageSelector}>
                                <ProfilePicture
                                    source={profilePicture}
                                    size={100}
                                />
                                <View style={styles.cameraIconContainer}>
                                    <VectorIcon
                                        name={"camera"}
                                        color={"#4D4F55"}
                                        size={20} />
                                </View>

                            </TouchableOpacity>
                        }

                        {/* ******************** PROFILE PICTURE End ******************** */}

                        {/* ******************** USER NAME Start ******************** */}
                        <Text style={styles.name}>{`${firstName} ${lastName}`}</Text>

                        {/* ******************** USER NAME End ******************** */}

                        {/* ******************** FIRST NAME INPUT Start ******************** */}
                        <TextInput
                            label={''}
                            baseColor={'#A7AAB1'}
                            textColor={'#0F1117'}
                            labelFontSize={12}
                            fontSize={16}
                            showDone

                            placeholder={IMLocalized(`First Name`)}
                            value={firstName}
                            onChangeText={(text) => { setFirstName(text); setFirstNameError('') }}
                            lineType={"none"}
                            containerStyle={styles.containerStyle}
                            onFocus={() => { setFocusedIndex(0); }}
                            onBlur={() => { setFocusedIndex(-1) }}
                            inputContainerStyle={[styles.inputContainerStyle, {
                                borderColor: focusedIndex === 0 ? colors.primary : "#A7AAB1",
                            }]}
                            style={styles.textinput}
                            error={firstNameError}
                            onSubmitEditing={() => {
                                if (lastNameRef !== null) {
                                    lastNameRef.current.onPress();
                                }

                            }}
                            returnKeyType={"next"}
                        />

                        {/* ******************** FIRST NAME INPUT End ******************** */}

                        {/* ******************** LAST NAME INPUT Start ******************** */}
                        <TextInput
                            // ref={lastNameRef}
                            customRef={lastNameRef}
                            label={''}
                            baseColor={'#A7AAB1'}
                            textColor={'#0F1117'}
                            labelFontSize={12}
                            fontSize={16}
                            showDone
                            placeholder={IMLocalized(`Last Name`)}
                            value={lastName}
                            onChangeText={(text) => { setLastName(text); setLastNameError('') }}
                            lineType={"none"}
                            containerStyle={styles.containerStyle}
                            onFocus={() => { setFocusedIndex(1); }}
                            onBlur={() => { setFocusedIndex(-1) }}
                            inputContainerStyle={[styles.inputContainerStyle, {
                                borderColor: focusedIndex === 1 ? colors.primary : "#A7AAB1",
                            }]}
                            style={styles.textinput}
                            error={lastNameError}

                            returnKeyType={"done"}
                        />

                        {/* ******************** LAST NAME INPUT End ******************** */}



                        <View style={styles.buttonContainer}>
                            <Button
                                text={IMLocalized(`Next`)}
                                containerStyle={styles.button}
                                width={"100%"}
                                onPress={onNextPress}
                            />
                        </View>

                    </View>





                </ScrollView>

            </KeyboardAvoidingView>


            <ImageSelector
                visible={imageSelectorVisible}
                dismiss={hideImageSelector}
                multipleAllow={false}
                getImages={getImages}
            />


        </View>
    );
}//end of index

export default index;


const _renderHeader = (navigation) => {
    return (
        <View style={headerStyles.primaryContainer}>
            <View style={headerStyles.backPrimaryContainer}>
                <TouchableOpacity style={headerStyles.backContainer}
                    onPress={() => { navigation.pop() && navigation.goBack(); }}>
                    <FastImage
                        source={LocalAssets.ICON.videoPlayBack}
                        style={headerStyles.imageBack}
                        resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>
            </View>


        </View>
    )
}//end of _renderHeader
