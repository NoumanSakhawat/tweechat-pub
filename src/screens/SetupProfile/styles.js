import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';


export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    body: {
        flex: 1,
    },
    heading: {
        color: "#0F1117",
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 0,
        paddingHorizontal: 20,
    },

    profilePictureContainer: {
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        marginTop: 12,
        marginBottom: 16,
        height: 100,
        width: 100,
    },
    cameraIconContainer: {
        position: "absolute",
        alignItems: "center",
        bottom: 10,
        left: 100 - 25,
        backgroundColor: colors.white,
        borderRadius: 20,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

        padding: 5,
    },
    name: {
        fontSize: 16,
        color: "#0F1117",
        textAlign: "center",
    },

    //TEXT INPUT
    containerStyle: {
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 8,
        alignItems: "center",
    },
    inputContainerStyle: {
        borderColor: "#A7AAB1",
        borderWidth: 1,
        borderRadius: 8,
    },
    textinput: {
        fontSize: 16,
        color: "#0F1117",
    },

    //BUTTON

    buttonContainer: {
        marginTop: 24,
        marginHorizontal: AppStyles.HORIZONTAL,
        marginBottom: 16,

        position: "absolute",
        bottom: 0,
        // width: "88%",
        width: Dimensions.get("screen").width - (AppStyles.HORIZONTAL * 2),
    },
    button: {
        marginHorizontal: AppStyles.HORIZONTAL,
        alignSelf: "center",
    },

});//end of styles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
        flexDirection: "row",
        alignItems: "center",
    },
    backPrimaryContainer: {
        flex: 1,
    },
    backContainer: {
        marginLeft: AppStyles.HORIZONTAL,
        paddingVertical: 7,
    },
    imageBack: {
        height: 24,
        width: 24,
    },

});//end of headerStyles