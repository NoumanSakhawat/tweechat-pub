import React, { useEffect, useState } from 'react';
import { Dimensions, Image, KeyboardAvoidingView, ScrollView, TextInput, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import LocalAssets from '../../constants/LocalAssets';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { footerStyles, headerStyles, styles } from './styles';
import Video from 'react-native-video';
import Slider from '@react-native-community/slider';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import Text from '../../components/Text';
import { IMLocalized } from '../../locales/IMLocalization';
import HelperFunctions, { emptyValidate } from '../../helper/HelperFunctions';
import Faker from '../../helper/Faker';
import Thought from '../../tweeComponents/Thought';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RNTextInput from '../../components/RNTextInput';
import { UIActivityIndicator, } from 'react-native-indicators';
import AppFunctions from '../../helper/AppFunctions';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import AppEnum from '../../constants/AppEnum';
import AutoHeightImage from '../../../lib/react-native-auto-height-image/AutoHeightImage';
import MultimediaFullScreen from '../../tweeComponents/MultimediaFullScreen';
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu';
import CameraFunctions from '../../helper/CameraFunctions';
const { Popover, ContextMenu } = renderers


const WIDTH = Dimensions.get('screen').width;

const index = ({ navigation, route }) => {

    const [isVideo, setIsVideo] = useState(false);
    const [activeIndex, setActiveIndex] = useState(0);
    const [carouselItems, setCarouselItems] = useState([
        {
            title: "Item 1",
            text: "Text 1",
        },
        {
            title: "Item 2",
            text: "Text 2",
        },
        {
            title: "Item 3",
            text: "Text 3",
        },
        {
            title: "Item 4",
            text: "Text 4",
        },
        {
            title: "Item 5",
            text: "Text 5",
        },
    ]);

    useEffect(async () => {
        if (route.params.data.length > 0)
            setIsVideo(route.params.data[0].type === AppEnum.MULTIMEDIA.video)
    }, []);





    const _renderCarouselItem = ({ item, index }) => {

        const isVideo = item.type === AppEnum.MULTIMEDIA.video;
        return (<MultimediaFullScreen
            activeIndex={activeIndex}
            type={item.type}
            path={item.path}
            dotsLength={route.params.data.length}
            index={index}
            thought={
                {
                    isMyLiked: item.isMyLiked,
                    like: item.like,
                    comment: item.comment,
                    isMyRetweet: item.isMyRetweet,
                    retweet: item.retweet,
                }
            }

            views={isVideo ? item.views : 0}
        />)
    };//end of _renderCarouselItem

    const saveImagePress = () => {
        const imageURL = route.params.data[activeIndex].path.uri;
        CameraFunctions.saveToGallery(imageURL);
    };//end of saveImagePRess

    return (
        <View style={styles.primaryContainer}>
            <TweeHeader customHeader={() => _renderHeader(navigation, isVideo, saveImagePress)} />

            <Carousel
                layout={"default"}
                ref={ref => carousel = ref}
                data={route.params.data}
                sliderWidth={WIDTH}
                itemWidth={WIDTH}

                renderItem={_renderCarouselItem}
                onSnapToItem={index => {
                    setActiveIndex(index)
                    setIsVideo(route.params.data[index].type === AppEnum.MULTIMEDIA.video)
                }}
                initialNumToRender={1}
                autoplay={false}
                loop={false}
            />


        </View>
    )
}//end of index

export default index;


const _renderHeader = (navigation, isVideo, saveImagePress) => {
    return (
        <View style={headerStyles.primaryContainer}>
            <View style={headerStyles.backPrimaryContainer}>
                <TouchableOpacity style={headerStyles.backContainer}
                    onPress={() => { navigation.pop() && navigation.goBack(); }}>
                    <FastImage
                        source={LocalAssets.ICON.videoPlayBack}
                        style={headerStyles.imageBack}
                        resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>
            </View>

            <View style={headerStyles.optionPrimaryContainer}>


                <Menu>
                    <MenuTrigger style={headerStyles.optionContainer}>
                        <FastImage
                            source={LocalAssets.ICON.videoPlayOption}
                            style={headerStyles.imageOption}
                            resizeMode={FastImage.resizeMode.contain} />
                    </MenuTrigger>
                    <MenuOptions customStyles={optionsStyles}>
                        <MenuOption
                            onSelect={() => {
                                if (isVideo)
                                    alert('Report Video');
                                else {

                                    saveImagePress()

                                }
                            }}
                            text={isVideo ? IMLocalized(`Report Video`) : IMLocalized(`Save Image`)} />
                    </MenuOptions>
                </Menu>

            </View>
        </View>
    )
}//end of _renderHeaderLeft


const optionsStyles = {
    optionsContainer: {
        backgroundColor: 'transparent',
        paddingRight: 40,
        paddingTop: 10,

    },
    optionsWrapper: {
        backgroundColor: colors.white,
    },
    optionWrapper: {
        backgroundColor: colors.white,
        margin: 5,
    },
    optionTouchable: {
        underlayColor: `rgba(0,0,0,0.04)`,
        activeOpacity: 70,
    },
    optionText: {
        color: colors.black,
        fontSize: 12,
    },
};
