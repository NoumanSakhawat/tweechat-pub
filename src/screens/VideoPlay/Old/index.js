import React, { useEffect, useState } from 'react';
import { Dimensions, Image, KeyboardAvoidingView, ScrollView, TextInput, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import LocalAssets from '../../constants/LocalAssets';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { footerStyles, headerStyles, styles } from './styles';
import Video from 'react-native-video';
import Slider from '@react-native-community/slider';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import Text from '../../components/Text';
import { IMLocalized } from '../../locales/IMLocalization';
import HelperFunctions, { emptyValidate } from '../../helper/HelperFunctions';
import Faker from '../../helper/Faker';
import Thought from '../../tweeComponents/Thought';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RNTextInput from '../../components/RNTextInput';
import { UIActivityIndicator, } from 'react-native-indicators';
import AppFunctions from '../../helper/AppFunctions';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import AppEnum from '../../constants/AppEnum';
import AutoHeightImage from '../../../lib/react-native-auto-height-image/AutoHeightImage';

const WIDTH = Dimensions.get('screen').width;

const thought = {
    isMyLiked: Faker.randomBool(),
    like: Faker.number(0, 100),
    comment: Faker.number(0, 100),
    isMyRetweet: Faker.randomBool(),
    retweet: Faker.number(0, 100),
};

const userProfilePicture = LocalAssets.randomImage();

const LINK = `https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4`


const index = ({ navigation, route }) => {
    const [rate, setRate] = useState(1);
    const [volume, setVolume] = useState(1);
    const [muted, setMuted] = useState(false);
    const [resizeMode, setResizeMode] = useState('cover');
    const [duration, setDuration] = useState(0.0);

    const [playableDuration, setPlayableDuration] = useState(0.0);

    const [currentTime, setCurrentTime] = useState(0.0);
    const [currentTimeText, setCurrentTimeText] = useState(0.0);
    const [paused, setPaused] = useState(false);
    const [videoOverlay, setVideoOverlay] = useState(false);
    const [videoOverlayAlways, setVideoOverlayAlways] = useState(false);
    const [icon, setIcon] = useState();
    const [views, setViews] = useState(Faker.numberWithComma());
    const [isKeyboardOpened, setKeyboardOpened] = useState(false);
    const [comment, setComment] = useState('');
    const [isBuffering, setBuffering] = useState(true);

    const [activeIndex, setActiveIndex] = useState(0);
    const [carouselItems, setCarouselItems] = useState([
        {
            title: "Item 1",
            text: "Text 1",
        },
        {
            title: "Item 2",
            text: "Text 2",
        },
        {
            title: "Item 3",
            text: "Text 3",
        },
        {
            title: "Item 4",
            text: "Text 4",
        },
        {
            title: "Item 5",
            text: "Text 5",
        },
    ]);

    useEffect(async () => {
        const myicon = Icon.getImageSourceSync('circle', 12, colors.primary)

        setIcon({ uri: myicon.uri })

        return () => clearInterval(timerId);
    }, []);

    let timerId = null;
    const toggleVideoOverlay = () => {
        if (!videoOverlay) {
            timerId = setTimeout(() => {
                setVideoOverlay(false);
                clearTimeout(timerId);
            }, 6000)
        } else {
            clearTimeout(timerId);
        }
        setVideoOverlay(!videoOverlay);
    };



    const onLoad = (data) => {
        setVideoOverlayAlways(false);
        setDuration(data.duration);
    };

    const onProgress = (data) => {
        if (parseInt(data.playableDuration) < parseInt(data.currentTime)) {
            setBuffering(true)
        } else {

            setBuffering(false)
        }

        setPlayableDuration(data.playableDuration);
        setCurrentTime(data.currentTime);
        setCurrentTimeText(data.currentTime);
    };

    const onEnd = () => {
        setVideoOverlay(true);
        setVideoOverlayAlways(true);
        setPaused(true);
    };

    const onAudioBecomingNoisy = () => {
        setPaused(true);
    };

    const onAudioFocusChanged = (event) => {
        setPaused(!event.hasAudioFocus);
    };

    const onBuffer = (event) => {
        // setBuffering(event.isBuffering);
    };//end of onBuffer

    const _renderFooter = (showSlider = false) => {
        return (

            <View style={styles.footerPrimaryContainer} >

                <Pagination
                    dotsLength={route.params.data.length}
                    activeDotIndex={activeIndex}
                    containerStyle={styles.paginationContainer}
                    dotColor={'rgba(0, 0, 0, 0.5)'}
                    dotStyle={styles.paginationDot}
                    inactiveDotColor={colors.black}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                    carouselRef={carousel}
                    tappableDots={!!carousel}
                />

                {showSlider &&
                    <Slider
                        style={styles.slider}
                        value={currentTime}
                        minimumValue={0}
                        maximumValue={duration}
                        minimumTrackTintColor={colors.primary}
                        maximumTrackTintColor="rgba(0, 0, 0, 0.24)"
                        thumbTintColor={colors.primary}
                        thumbImage={icon}

                        onSlidingStart={(val) => {
                            setVideoOverlayAlways(false);
                            setPaused(true);
                        }}
                        onSlidingComplete={(val) => {
                            setVideoOverlayAlways(false);
                            setPaused(false);
                            if (player !== null) {
                                player.seek(val)
                            }

                            setBuffering(true);
                        }}
                        onValueChange={(val) => {
                            if (player !== null) {
                                setVideoOverlayAlways(false);

                                // setCurrentTime(val);
                                setCurrentTimeText(val);
                            }

                            setBuffering(true);

                        }}
                    />
                }

                {/* ******************** VIEW's AND TIME Start ******************** */}

                {showSlider &&
                    <View style={footerStyles.viewTimeContainer}>
                        <Text>{`${views} ${IMLocalized(`views`)}`}</Text>

                        <View style={footerStyles.durationContainer}>
                            <Text>{`${HelperFunctions.fancyTimeFormat(currentTimeText)} / ${HelperFunctions.fancyTimeFormat(duration)}`}</Text>
                        </View>
                    </View>
                }

                {/* ******************** VIEW's AND TIME End ******************** */}

                {/* ******************** THOUGHT Start ******************** */}
                <View style={footerStyles.thoughtContainer}>
                    <Thought data={thought} />


                    <TouchableOpacity style={footerStyles.viewAllTextContainer}>
                        <Text style={footerStyles.viewAllText}>{IMLocalized(`View all number Comments`).replace('number', thought.comment)}</Text>
                    </TouchableOpacity>

                </View>

                {/* ******************** THOUGHT End ******************** */}

                {/* ******************** LEAVE COMMENT Start ******************** */}

                <KeyboardAwareScrollView onKeyboardWillShow={(frames) => {
                    setKeyboardOpened(true);
                    setResizeMode('stretch');
                }}
                    scrollEnabled={isKeyboardOpened}
                    onKeyboardWillHide={() => {
                        setKeyboardOpened(false);
                        setResizeMode('cover');
                    }}
                    contentContainerStyle={footerStyles.commentContentContainer}>
                    <View style={footerStyles.commentPrimaryContainer}>
                        <ProfilePicture
                            source={{ uri: userProfilePicture }}
                            size={32}
                        />
                        <RNTextInput
                            // <TextInput
                            //     style={footerStyles.textinput}
                            placeholder={IMLocalized(`Leave your comment`)}
                            placeholderTextColor={"#666666"}
                            defaultValue={comment}
                            onChangeText={(text) => { setComment(text) }}
                        />

                    </View>
                </KeyboardAwareScrollView>
                {/* ******************** LEAVE COMMENT End ******************** */}
            </View>

        )
    };//end of _renderFooter

    let player = null;
    let carousel = null;

    const _renderCarouselItem = ({ item, index }) => {

        return (
            <View style={styles.video}>

                {(item.type === AppEnum.MULTIMEDIA.video) &&
                    index === activeIndex ?
                    _renderVideo(item.path)
                    : <>

                    </>
                }

                {(item.type === AppEnum.MULTIMEDIA.image) &&
                    _renderImage(item.path)
                }

            </View>

        )
    };//end of _renderCarouselItem

    const _renderImage = (photoURL) => {

        return (
            <View style={styles.video}>
                {emptyValidate(photoURL) &&
                    <AutoHeightImage
                        width={WIDTH}
                        source={photoURL}
                        style={styles.image}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                }
            </View>
        )
    };//end of _renderImage

    const _renderVideo = (videoURL) => {

        return (
            <>
                <TouchableOpacity activeOpacity={1} onPress={toggleVideoOverlay}
                    style={styles.video}>
                    <Video
                        source={videoURL}
                        ref={(ref) => {
                            player = ref
                        }}
                        style={styles.video}
                        rate={rate}
                        paused={paused}
                        volume={volume}
                        muted={muted}
                        resizeMode={resizeMode}
                        onBuffer={onBuffer}
                        onLoad={onLoad}
                        onProgress={onProgress}
                        onEnd={onEnd}
                        onAudioBecomingNoisy={onAudioBecomingNoisy}
                        onAudioFocusChanged={onAudioFocusChanged}
                        repeat={false}
                        bufferConfig={{
                            minBufferMs: 15000,
                            maxBufferMs: 50000,
                            bufferForPlaybackMs: 2500,
                            bufferForPlaybackAfterRebufferMs: 5000
                        }}
                    />

                    {videoOverlay ?
                        <>
                            <TouchableOpacity style={styles.overlay} onPress={toggleVideoOverlay} />
                            <TouchableOpacity style={[styles.pauseIcon, {
                                top: isKeyboardOpened ? (WIDTH / 4) / 2.5 : (WIDTH / 4) / 1.3,
                            }]}
                                onPress={() => {
                                    if (videoOverlayAlways) {
                                        setVideoOverlay(false);
                                        setVideoOverlayAlways(false);
                                        setPaused(false);
                                        player.seek(0);
                                        return
                                    }
                                    setPaused(!paused)
                                }}>
                                <VectorIcon
                                    name={videoOverlayAlways ? "reload" : paused ? "play" : "pause"}
                                    size={50}
                                    color={colors.white} />
                            </TouchableOpacity>
                        </>
                        :
                        (isBuffering) &&
                        <>
                            <View style={styles.overlay} />
                            <View style={[styles.pauseIcon, {
                                top: isKeyboardOpened ? (WIDTH / 4) / 2.5 : (WIDTH / 4) / 1.3,
                            }]}>
                                <UIActivityIndicator color='white' />
                            </View>
                        </>

                    }


                </TouchableOpacity>

            </>
        )
    };//end of _renderVideo

    return (
        <View style={styles.primaryContainer}>
            <TweeHeader customHeader={() => _renderHeader(navigation)} />

            <View style={{ flex: isKeyboardOpened ? 0.1 : 0.5 }} />


            <View style={{ flex: isKeyboardOpened ? 0.3 : 1 }}>

                <Carousel
                    layout={"default"}
                    ref={ref => carousel = ref}
                    data={route.params.data}
                    sliderWidth={WIDTH}
                    itemWidth={WIDTH}

                    renderItem={_renderCarouselItem}
                    onSnapToItem={index => setActiveIndex(index)}
                    initialNumToRender={1}
                    autoplay={false}
                    loop={false}
                />

            </View>
            {_renderFooter(true)}

        </View>
    )
}//end of index

export default index;


const _renderHeader = (navigation) => {
    return (
        <View style={headerStyles.primaryContainer}>
            <View style={headerStyles.backPrimaryContainer}>
                <TouchableOpacity style={headerStyles.backContainer}
                    onPress={() => { navigation.pop() && navigation.goBack(); }}>
                    <FastImage
                        source={LocalAssets.ICON.videoPlayBack}
                        style={headerStyles.imageBack}
                        resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>
            </View>

            <View style={headerStyles.optionPrimaryContainer}>
                <TouchableOpacity style={headerStyles.optionContainer}>
                    <FastImage
                        source={LocalAssets.ICON.videoPlayOption}
                        style={headerStyles.imageOption}
                        resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>
            </View>
        </View>
    )
}//end of _renderHeaderLeft
