import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';


const WIDTH = Dimensions.get('screen').width;
const HEIGHT = Dimensions.get('screen').height;

const videoTop = -40;

export const styles = StyleSheet.create({
    anchorStyle: {
        backgroundColor: 'blue',
    },
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    video: {
        flex: 1,
        marginTop: videoTop,
        minHeight: WIDTH / 4,
        width: WIDTH,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
    },
    image: {
        marginTop: videoTop,

        maxHeight: WIDTH / 4,
        width: WIDTH,

        borderRadius: 4,
    },
    overlay: {
        backgroundColor: `rgba(0,0,0,0.4)`,

        position: 'absolute',
        top: videoTop,
        bottom: 0,
        left: 0,
        right: 0,

        zIndex: 1,
    },
    pauseIcon: {

        position: 'absolute',
        top: (WIDTH / 4) / 2,
        flex: 1,
        zIndex: 1,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
    },
    slider: {
        width: WIDTH - 36,
        height: 20,
        alignSelf: "center",
        marginTop: 20,
    },
    footerPrimaryContainer: {
        flex: 1,
        justifyContent: "flex-end",
        // marginBottom: 32,
    },
    bufferImage: {
        height: 50,
        width: 50,
    },


    paginationContainer: {
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8
    }
});//end of styles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
        flexDirection: "row",
        alignItems: "center",
    },
    backPrimaryContainer: {
        flex: 1,
    },
    backContainer: {
        marginLeft: AppStyles.HORIZONTAL,
        paddingVertical: 7,
    },
    imageBack: {
        height: 24,
        width: 24,
    },
    optionPrimaryContainer: {
        flex: 1,
        alignItems: "flex-end",
    },
    optionContainer: {
        paddingHorizontal: 16,
    },
    imageOption: {
        height: 24,
        width: 24,
    },
});//end of headerStyles

export const footerStyles = StyleSheet.create({
    viewTimeContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 12,
        marginHorizontal: 18,
    },
    durationContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end",
    },
    thoughtContainer: {
        marginHorizontal: AppStyles.HORIZONTAL,
    },

    viewAllTextContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 10,
    },
    viewAllText: {
        fontSize: 12,
        color: "#87888B",
    },
    commentContentContainer: {
        zIndex: 100,
    },

    commentPrimaryContainer: {
        flexDirection: "row",
        alignItems: "center",
        paddingTop: 28,
        paddingLeft: 18,
        paddingRight: 18,
        paddingBottom: 32,
    },
    textinput: {
        backgroundColor: "#EDF0F7",
        padding: 12,
        marginLeft: 12,
        borderRadius: 8,
        flex: 1,
        color: "#666666",
        fontSize: 14,

    },
});//end of footerStyles
