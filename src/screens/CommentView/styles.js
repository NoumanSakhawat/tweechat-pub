import { StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    contentContainerStyle: {
        marginTop: 24,
        paddingBottom: 10,
    },
    reactionContainer: {

        marginTop: 18,
    },
    reactionHeading: {
        paddingHorizontal: AppStyles.HORIZONTAL - 4,
        fontSize: 16,
        color: "#333333",
    },
    reactionContentContainerStyle: {
        paddingHorizontal: AppStyles.HORIZONTAL - 4,
        marginTop: 8,
    },
    reactionIContainer: {
        marginRight: 8,
    },
    commentContainer: {
        // marginHorizontal: AppStyles.HORIZONTAL - 4,
        marginTop: 16,
    },
    commentHeading: {
        paddingHorizontal: AppStyles.HORIZONTAL - 4,
        fontSize: 14,
        color: "#333333",
    },
});//end of styles

export const commentStyles = StyleSheet.create({
    primaryContainer: {

    },
    childContainer: {
        marginLeft: 52,
    },
    nestedCildContainer: {
        marginLeft: 104,
    },
});//end of commentStyles

export const headerStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
        flexDirection: "row",
        alignItems: "center",
    },
    backPrimaryContainer: {
        flex: 1,
    },
    backContainer: {
        marginLeft: AppStyles.HORIZONTAL,
        paddingVertical: 7,
    },
    imageBack: {
        height: 24,
        width: 24,
    },
    optionPrimaryContainer: {
        flex: 1,
        alignItems: "flex-end",
    },
    optionContainer: {
        paddingHorizontal: 16,
    },
    imageOption: {
        height: 24,
        width: 24,
    },
    optionImage: {
        height: 22,
        width: 22,
    },
    optionText: {
        color: colors.black,
        fontSize: 12,
        marginLeft: 8,
    },
    shadow: {
        shadowColor: 'rgba(0, 0, 0, 0.3)',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    },
});//end of headerStyles

export const footerStyles = StyleSheet.create({
    viewTimeContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 12,
        marginHorizontal: 18,
    },
    durationContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end",
    },
    thoughtContainer: {
        marginHorizontal: AppStyles.HORIZONTAL,
    },

    viewAllTextContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 10,
    },
    viewAllText: {
        fontSize: 12,
        color: "#87888B",
    },
    commentContentContainer: {
        zIndex: 100,
    },

    commentPrimaryContainer: {
        flexDirection: "row",
        alignItems: "center",
        paddingTop: 28,
        paddingLeft: 18,
        paddingRight: 18,
        paddingBottom: 32,
    },
    textinput: {
        backgroundColor: "#EDF0F7",
        padding: 12,
        marginLeft: 12,
        borderRadius: 8,
        flex: 1,
        color: "#666666",
        fontSize: 14,

    },
});//end of footerStyles
