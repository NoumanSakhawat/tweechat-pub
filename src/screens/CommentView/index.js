import React, { useState } from 'react';
import { FlatList, TouchableOpacity, View, Dimensions, KeyboardAvoidingView } from 'react-native';
import FastImage from 'react-native-fast-image';
import { ScrollView } from 'react-native-gesture-handler';
import LocalAssets from '../../constants/LocalAssets';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { commentStyles, footerStyles, headerStyles, styles } from './styles';
import Post from '../../UIComponents/Post';
import Text from '../../components/Text';
import { IMLocalized } from '../../locales/IMLocalization';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import StaticData from '../../constants/StaticData';
import Comment from "../../UIComponents/Comment";
import RNTextInput from '../../components/RNTextInput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ROUTES from '../../routes/ROUTES';
import HelperFunctions from '../../helper/HelperFunctions';
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu';
import colors from '../../constants/colors';

const WIDTH = Dimensions.get('window').width;
const REACTION_LIMIT = ((WIDTH / 40).toFixed(0));

const index = ({ navigation, route }) => {

    const commentsData = route.params.post.thought.commentDetail;

    let reactionsData = route.params.post.thought.reactions;
    if (reactionsData.length > REACTION_LIMIT - 2) {
        reactionsData = reactionsData.splice(1, REACTION_LIMIT - 3);
    }

    const [comment, setComment] = useState('');

    return (
        <View style={styles.primaryContainer}>
            <View style={headerStyles.shadow}>
                <TweeHeader customHeader={() => _renderHeader(navigation, route.params.post.user.username)} bottomBorder={false} />
            </View>

            <KeyboardAwareScrollView
                contentContainerStyle={styles.contentContainerStyle}
                nestedScrollEnabled={true}>
                <Post
                    onUserProfilePress={(item) => {
                        navigation.navigate(ROUTES.UserProfile)
                    }}
                    showTopBorder={false}
                    showViewAllCommentText={false}
                    showOptionIcon={false}
                    item={route.params.post}
                    index={route.params.postIndex}
                    navigation={navigation}
                    onPostPress={() => {
                        // navigation.navigate(ROUTES.VideoPlay);
                    }}
                    onMediaItemPress={(isMultiple, multimediaItemIndex) => {

                        const mulData = HelperFunctions.array_move(route.params.post.multimedia, multimediaItemIndex, 0);
                        navigation.navigate({
                            name: ROUTES.VideoPlay,
                            params: {
                                isMultiple,
                                data: mulData,
                                item: route.params.post.multimedia,
                                index: route.params.postIndex,
                            }
                        })
                    }}
                />


                {/* ******************** REACTION Start ******************** */}
                {reactionsData.length > 0 &&
                    <View style={styles.reactionContainer}>
                        <Text style={styles.reactionHeading}>{IMLocalized(`Reactions`)}</Text>

                        <FlatList
                            data={reactionsData}
                            horizontal
                            contentContainerStyle={styles.reactionContentContainerStyle}
                            renderItem={({ item, index }) => {

                                return (
                                    <>
                                        <TouchableOpacity style={styles.reactionIContainer}>
                                            <ProfilePicture
                                                source={item.profile}
                                                size={40} />
                                        </TouchableOpacity>
                                        {reactionsData.length - 1 === index &&
                                            <TouchableOpacity style={styles.reactionIContainer}>
                                                <ProfilePicture
                                                    source={LocalAssets.ICON.reactionMore}
                                                    size={40} />
                                            </TouchableOpacity>
                                        }
                                    </>
                                )
                            }} />
                    </View>
                }

                {/* ******************** REACTION End ******************** */}

                {/* ******************** COMMENTS Start ******************** */}
                <View style={styles.commentContainer}>
                    <Text style={styles.commentHeading}>{IMLocalized(`Comments`)}</Text>

                    <FlatList
                        data={commentsData}
                        contentContainerStyle={styles.reactionContentContainerStyle}
                        renderItem={({ item, index }) => {

                            return (
                                <View style={commentStyles.primaryContainer}>

                                    <Comment obj={item} />
                                    {"childern" in item &&
                                        item.childern.map((childItem, childIndex) => {
                                            return (
                                                <>
                                                    <View style={commentStyles.childContainer}>
                                                        <Comment obj={childItem} />
                                                    </View>
                                                    {"childern" in childItem &&
                                                        childItem.childern.map((nestedChildItem, nestedChildIndex) => {

                                                            return (
                                                                <View style={commentStyles.nestedCildContainer}>
                                                                    <Comment obj={nestedChildItem} />
                                                                </View>
                                                            )
                                                        })

                                                    }
                                                </>
                                            )
                                        })

                                    }

                                </View>
                            )
                        }} />


                </View>

                {/* ******************** COMMENTS End ******************** */}


                {/* ******************** LEAVE COMMENT Start ******************** */}

                {/* <KeyboardAvoidingView
                contentContainerStyle={footerStyles.commentContentContainer}> */}
                <View style={footerStyles.commentPrimaryContainer}>
                    <ProfilePicture
                        source={{ uri: StaticData.userProfilePicture }}
                        size={32}
                    />
                    <RNTextInput
                        placeholder={IMLocalized(`Leave your comment`)}
                        placeholderTextColor={"#666666"}
                        defaultValue={comment}
                        onChangeText={(text) => { setComment(text) }}
                    />

                </View>
                {/* </KeyboardAvoidingView> */}
                {/* ******************** LEAVE COMMENT End ******************** */}


            </KeyboardAwareScrollView>


        </View>
    )
}//end of index

export default index;


const _renderHeader = (navigation, username) => {
    return (
        <View style={headerStyles.primaryContainer}>
            <View style={headerStyles.backPrimaryContainer}>
                <TouchableOpacity style={headerStyles.backContainer}
                    onPress={() => { navigation.pop() && navigation.goBack(); }}>
                    <FastImage
                        source={LocalAssets.ICON.videoPlayBack}
                        style={headerStyles.imageBack}
                        resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>
            </View>

            {/* <View style={headerStyles.optionPrimaryContainer}>
                <TouchableOpacity style={headerStyles.optionContainer}>
                    <FastImage
                        source={LocalAssets.ICON.videoPlayOption}
                        style={headerStyles.imageOption}
                        resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>
            </View> */}


            <Menu style={headerStyles.optionPrimaryContainer}>
                <MenuTrigger
                //@ts-ignore
                // style={itemStyles.optionContainer}
                >

                    <FastImage
                        source={LocalAssets.ICON.videoPlayOption}
                        style={headerStyles.imageOption}
                        resizeMode={FastImage.resizeMode.contain} />
                </MenuTrigger>
                <MenuOptions
                    customStyles={optionsStyles}
                >
                    <MenuOption style={{ flexDirection: "row", alignItems: "center" }}>
                        <FastImage
                            source={LocalAssets.PostOption.follow}
                            style={headerStyles.optionImage}
                            resizeMode={FastImage.resizeMode.contain} />
                        <Text style={headerStyles.optionText}>{`${IMLocalized(`Follow`)} @${username}`}</Text>
                    </MenuOption>


                    <MenuOption style={{ flexDirection: "row", alignItems: "center" }}>
                        <FastImage
                            source={LocalAssets.PostOption.mute}
                            style={headerStyles.optionImage}
                            resizeMode={FastImage.resizeMode.contain} />
                        <Text style={headerStyles.optionText}>{`${IMLocalized(`Mute`)} @${username}`}</Text>
                    </MenuOption>


                    <MenuOption style={{ flexDirection: "row", alignItems: "center" }}>
                        <FastImage
                            source={LocalAssets.PostOption.block}
                            style={headerStyles.optionImage}
                            resizeMode={FastImage.resizeMode.contain} />
                        <Text style={headerStyles.optionText}>{`${IMLocalized(`Block`)} @${username}`}</Text>
                    </MenuOption>


                    <MenuOption style={{ flexDirection: "row", alignItems: "center" }}>
                        <FastImage
                            source={LocalAssets.PostOption.report}
                            style={headerStyles.optionImage}
                            resizeMode={FastImage.resizeMode.contain} />
                        <Text style={headerStyles.optionText}>{`${IMLocalized(`Report Tweet`)}`}</Text>
                    </MenuOption>
                </MenuOptions>
            </Menu>

        </View>
    )
};//end of _renderHeader

const optionsStyles = {
    optionsContainer: {
        backgroundColor: 'transparent',
        paddingRight: 20,
        paddingTop: 10,
        minWidth: 230,
    },
    optionsWrapper: {
        backgroundColor: colors.white,
    },
    optionWrapper: {
        backgroundColor: colors.white,
        margin: 5,
    },
    optionTouchable: {
        underlayColor: `rgba(0,0,0,0.04)`,
        activeOpacity: 70,
    },
    optionText: {
        color: colors.black,
        fontSize: 12,
    },
};