import React, { useEffect, useState } from 'react';
import { KeyboardAvoidingView, ScrollView, TouchableOpacity, View } from 'react-native';
import ImageSelector from '../../AppComponents/ImageSelector';
import Button from '../../components/Button';
import Text from '../../components/Text';
import TextInput from '../../components/TextInput';
import VectorIcon from '../../components/VectorIcon';
import { IMLocalized } from '../../locales/IMLocalization';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import { headerStyles, styles } from './styles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TextField from '../../../lib/OutlinedTextField/field';
import FastImage from 'react-native-fast-image';
import LocalAssets from '../../constants/LocalAssets';
import TweeHeader from '../../tweeComponents/TweeHeader';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import colors from '../../constants/colors';
import Statusbar from '../../components/Statusbar';
import Constants from '../../constants/Constants';


const index = ({ navigation }) => {

    const [email, setemail] = useState('');
    const [emailError, setemailError] = useState('');

    const [focusedIndex, setFocusedIndex] = useState(-1);

    useEffect(async () => {

    }, [])

    const sentEmailPress = () => {
    };//end of sentEmailPress

    return (
        <View style={styles.primaryContainer}>

            {/* <TweeHeader customHeader={() => } /> */}
            <Statusbar />
            {_renderHeader(navigation)}

            <KeyboardAwareScrollView
                style={{ flex: 1, }}
                keyboardShouldPersistTaps='handled'
                extraScrollHeight={40}>



                <View style={styles.body}>

                    <FastImage
                        source={LocalAssets.ICON.forgotPassword}
                        style={styles.forgotPasswordImage}
                        resizeMode={FastImage.resizeMode.contain} />

                    <Text style={styles.heading}>{IMLocalized(`Reset password`)}</Text>
                    <Text style={styles.heading1}>{IMLocalized(`Enter the email address asscociated with your account.`)}</Text>



                    {/* ******************** EMAIL INPUT Start ******************** */}
                    <TextInput
                        label={''}
                        baseColor={'#A7AAB1'}
                        textColor={'#0F1117'}
                        labelFontSize={12}
                        fontSize={16}
                        showDone
                        placeholder={IMLocalized(`Email`)}
                        maxLength={Constants.FORGET_PASSWORD_LENGTH.EMAIL}
                        value={email}
                        onChangeText={(text) => { setemail(text); setemailError('') }}
                        lineType={"none"}
                        containerStyle={styles.containerStyle}
                        onFocus={() => { setFocusedIndex(0) }}
                        onBlur={() => { setFocusedIndex(-1) }}
                        inputContainerStyle={[styles.inputContainerStyle, {
                            borderColor: focusedIndex === 0 ? colors.primary : "#A7AAB1",
                        }]}
                        style={styles.textinput}
                        error={emailError}
                    />

                    {/* ******************** EMAIL INPUT End ******************** */}



                    {/* ******************** SENT EMAIL BUTTON Start ******************** */}
                    <View style={styles.buttonContainer}>
                        <Button
                            text={IMLocalized(`Sent Email`)}
                            containerStyle={styles.button}
                            width={"100%"}
                            onPress={sentEmailPress}
                            textTransform={"capitalize"}
                        />
                    </View>

                    {/* ******************** SENT EMAIL BUTTON End ******************** */}



                </View>
            </KeyboardAwareScrollView>


        </View>
    );
}//end of index

export default index;


const _renderHeader = (navigation) => {
    return (<View style={headerStyles.primaryContainer}>

        {/* <View style={headerStyles.backPrimaryContainer}> */}
        <TouchableOpacity style={headerStyles.backContainer}
            onPress={() => { navigation.pop() && navigation.goBack(); }}>
            <FastImage
                source={LocalAssets.ICON.videoPlayBack}
                style={headerStyles.imageBack}
                resizeMode={FastImage.resizeMode.contain} />
        </TouchableOpacity>
        {/* </View> */}


    </View>

    )
}//end of _renderHeader