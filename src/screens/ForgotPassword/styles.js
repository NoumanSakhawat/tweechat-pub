import { Dimensions, StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const WIDTH = Dimensions.get('window').width;
export const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,
        backgroundColor: colors.background,
    },
    body: {
        flex: 1,
    },
    forgotPasswordImage: {
        height: WIDTH / 2,
        width: WIDTH / 2,
        alignSelf: "center",
        marginTop: 20,
    },
    heading: {
        color: "#221F2E",
        fontSize: 24,
        fontWeight: "bold",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 0,
        paddingHorizontal: 0,
        marginBottom: 0,
        textAlign: "center",
    },
    heading1: {
        color: "#2F3137",
        fontSize: 16,
        textAlign: "center",
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 16,
    },



    //TEXT INPUT
    containerStyle: {
        marginHorizontal: AppStyles.HORIZONTAL,
        marginTop: 24,
        alignItems: "center",
    },
    inputContainerStyle: {
        borderColor: "#A7AAB1",
        borderWidth: 1,
        borderRadius: 8,
    },
    textinput: {
        fontSize: 16,
        color: "#0F1117",
    },



    //BUTTON
    buttonContainer: {
        marginHorizontal: AppStyles.HORIZONTAL,
    },
    button: {
        alignSelf: "center",
    },

});//end of styles



export const headerStyles = StyleSheet.create({
    primaryContainer: {
        flexDirection: "row",
        alignItems: "center",
        position: "absolute",
        top: 20,
        left: 0,
        minHeight: 50,
        zIndex: 1,
    },
    backPrimaryContainer: {
        // flex: 1,
    },
    backContainer: {
        paddingHorizontal: AppStyles.HORIZONTAL,
        paddingVertical: 7,

    },
    imageBack: {
        height: 24,
        width: 24,
    },

});//end of headerStyles