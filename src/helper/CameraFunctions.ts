import { Alert, Image, PermissionsAndroid, Platform } from "react-native";
import CameraRoll, { GetPhotosParams, AssetType } from "@react-native-community/cameraroll";
import ImagePickerCrop, { } from 'react-native-image-crop-picker';
import RNFS, { DownloadBeginCallbackResult, DownloadProgressCallbackResult } from "react-native-fs";
import HelperFunctions from "./HelperFunctions";
import RNFetchBlob from "rn-fetch-blob";
import AppFunctions from "./AppFunctions";
import { IMLocalized } from "../locales/IMLocalization";

const app = require('../../app.json');

const guid = () => {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}//end of guid

const hasAndroidPermission = async () => {
    if (Platform.OS !== "android") {
        return '';
    }
    const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;

    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
        return true;
    }

    const status = await PermissionsAndroid.request(permission);
    return status === 'granted';
}//end of hasAndroidPermission

export default {

    getPhotos(first = 20, assetType: AssetType = 'Photos') {
        return new Promise(async (resolve) => {
            await hasAndroidPermission();
            const params: GetPhotosParams = {
                first,
                assetType: assetType
            }

            const photos = await CameraRoll.getPhotos(params);


            for (let i = 0; i < photos.edges.length; i++) {
                Image.getSize(photos.edges[i].node.image.uri, (width, height) => {

                });
            }
            resolve(photos);
        })//end of PROMISE
    },//end of getPhotos

    camera(includeBase64 = false, mediaType: any = "photo", cropping = false) {
        return new Promise((resolve) => {
            ImagePickerCrop.openCamera({
                mediaType: mediaType,
                cropping: cropping,
                includeBase64: includeBase64,
                compressImageMaxHeight: 500,
                compressImageMaxWidth: 500,
                compressImageQuality: 0.9,
                forceJpg: true,

            }).then(async (images: any) => {
                let newImages = [];
                newImages.push(images);
                newImages.map((e: any) => {
                    e.id = guid();
                    return
                })
                const myImageArr = [];
                for (let i = 0; i < newImages.length; i++) {
                    const element = newImages[i];
                    Image.getSize(element.path, (width, height) => {
                        console.log('height ', height);
                        console.log('width ', width);
                    });
                }
                resolve(newImages);
            }).catch((err: any) => {
                if ("message" in err && err.message.includes("User cancelled image selection")) {
                    resolve(undefined);
                }
                else {
                    console.warn("ERROR from CATCH IMAGE PICKER", err.message);
                    Alert.alert("message" in err ? err.message : "Error from Image Picker! ");
                    resolve(undefined);
                }
            });
        })//end of PROMISE
    },

    gallery(multiple = false, includeBase64 = false, mediaType: any = "photo", cropping = false) {
        return new Promise((resolve) => {
            ImagePickerCrop.openPicker({
                multiple: multiple,
                mediaType: mediaType,
                cropping: cropping,
                includeBase64: includeBase64,
                compressImageMaxHeight: 500,
                compressImageMaxWidth: 500,
                compressImageQuality: 0.9,
                forceJpg: true,

            }).then(async (image: any) => {
                let images = null;
                if (!Array.isArray(image)) {
                    images = [];
                    images.push(image);
                } else {
                    images = image;
                }
                images.map((e: any) => {
                    e.id = guid();
                    return
                })


                resolve(images);
            }).catch((err: any) => {
                console.log(err.message);
                if ("message" in err && err.message.includes("User cancelled image selection")) {
                    resolve(undefined);
                }
                else {
                    console.warn("ERROR from CATCH IMAGE PICKER", err.message);
                    Alert.alert("message" in err ? err.message : "Error from Image Picker! ");
                    resolve(undefined);
                }

            });
        })//end of PROMISE
    },


    saveToGallery(fromURL: any = null) {
        RNFetchBlob.config({
            fileCache: true,
            appendExt: 'png',
        })
            .fetch('GET', fromURL)
            .then(response => {

                CameraRoll.save(response.data, { album: app.name, type: "photo" })
                    .then(res => {
                        console.log(res)
                        AppFunctions.successFlashMessage(`${IMLocalized(`Saved successfully`)}!`);
                    })
                    .catch(err => {
                        console.log(err)
                        AppFunctions.errorFlashMessage(`${IMLocalized(`Something went wrong while saving image to gallery`)}!`)
                    })
            }).catch(error => {
                console.log(error);
                AppFunctions.errorFlashMessage(`${IMLocalized(`Something went wrong while saving image to gallery`)}!`)
            });



    },//end of saveToGallery

}//end of EXPORT DEFAULT


