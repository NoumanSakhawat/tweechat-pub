import Geolocation from '@react-native-community/geolocation';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import { Linking, Alert, Platform } from 'react-native';
import { IMLocalized } from '../locales/IMLocalization';

let call = 0;
let callPermission = 0;

export default {
    getCurrentLocation() {
        return new Promise((resolve) => {
            if (Platform.OS === "android") {
                RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
                    .then((data: any) => {
                        Geolocation.getCurrentPosition((position: any) => {

                            resolve(position);

                        }, (error: any) => {
                            if (error.message.includes("Location request timed out")) {
                                if (call < 5) {
                                    this.getCurrentLocation();
                                    call++;
                                }
                            }
                            if (error.message.includes("Location permission was not granted")) {
                                // Permission.LOCATION();
                                if (callPermission < 5) {
                                    this.getCurrentLocation();
                                    callPermission++;
                                }
                            }
                            resolve(undefined);
                        });


                    }).catch((err: any) => {
                        if (err.message.includes("denied")) {
                            this.getCurrentLocation();

                        }
                        resolve(undefined);
                    });
            } else {
                let timer = setTimeout(() => {
                    resolve(undefined);
                }, 5000);
                Geolocation.requestAuthorization();

                Geolocation.getCurrentPosition((position: any) => {

                    clearTimeout(timer);
                    resolve(position);
                }, (error: any) => {

                    if (error.message.includes("Location services disabled.")) {
                        customAlert(`${IMLocalized(`Location services is off`)}!`, IMLocalized(`To use background location, you must enable 'Always' in the Location Service setting.`))


                    }
                    if (error.message.includes("User denied access to location services.")) {
                        customAlert(`${IMLocalized(`Location services access denied`)}!`, IMLocalized(`To use background location, you must enable 'Always' in the Location Service setting.`))
                    }
                    clearTimeout(timer);
                    resolve(undefined);
                });
            }
        })//end of PROMISE
    },//end of getCurrentLocation
}//end of EXPORT DEFAULT

function customAlert(title: any, description: any, onPress = () => { Linking.openSettings(); }) {
    Alert.alert(title, description, [{
        text: IMLocalized(`Cancel`),
        style: 'cancel',
    }, {
        text: IMLocalized(`Settings`),
        style: 'destructive',
        onPress() {
            // Linking.openURL('app-settings');
            onPress();

        }
    }], { cancelable: true });
}