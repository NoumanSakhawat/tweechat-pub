
export function emptyValidate(text: any = "") {
    if (text === "" || text === " " || text === "null" || text === null || text === "undefined" || text === undefined || text === false || text === "false") {
        return false;
    }
    else {
        return true;
    }
};//end of emptyValidate

export function printText(text: any = "", defaultValue: any = "") {
    if (text === "" || text === " " || text === "null" || text === null || text === "undefined" || text === undefined || text === false || text === "false") {
        return defaultValue;
    }
    else {
        return text;
    }
};//end of emptyValidate


export function printTextWithLimit(text: any = "", defaultValue: any = "", len: number = 35) {
    if (text === "" || text === " " || text === "null" || text === null || text === "undefined" || text === undefined || text === false || text === "false") {
        return defaultValue.length < len ? `${defaultValue}` : `${defaultValue.substring(0, (len - 3))}...`;
    } else {
        return text.length < len ? `${text}` : `${text.substring(0, (len - 3))}...`;
    }

};//end of printTextWithLimit


export default {

    randomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },//end of randomInt

    guid() {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    },//end of guid

    timeSince(ts: any) {
        const current: any = new Date().getTime();
        const previous: any = ts;
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;

        var elapsed = current - previous;

        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + ' seconds ago';
        }

        else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        }

        else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        }

        else if (elapsed < msPerMonth) {
            return '' + Math.round(elapsed / msPerDay) + ' days ago';
        }

        else if (elapsed < msPerYear) {
            return '' + Math.round(elapsed / msPerMonth) + ' months ago';
        }

        else {
            return '' + Math.round(elapsed / msPerYear) + ' years ago';
        }
    },//end of timeSince

    keyExtractor() {
        return new Date().getTime().toString() + (Math.floor(Math.random() * Math.floor(new Date().getTime()))).toString();
    },//end of keyExtractor

    numberFixed(num: any, len = 0) {
        num = parseFloat(num);
        return (Math.round(num * 100) / 100).toFixed(len);
    },//end of numberFixedTwo

    millisToMinutesAndSeconds(millis: any) {

        millis = parseInt(millis);
        var minutes = Math.floor(millis / 60000);
        var seconds: any = ((millis % 60000) / 1000).toFixed(0);


        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    },//end of millisToMinutesAndSeconds

    fancyTimeFormat(duration: any) {
        duration = parseInt(duration);
        // Hours, minutes and seconds
        var hrs = ~~(duration / 3600);
        var mins = ~~((duration % 3600) / 60);
        var secs = ~~duration % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";

        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }

        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    },//en dof  fancyTimeFormat


    timestampToReadable(timestamp: any = new Date().getTime()) {
        const date = new Date(timestamp);
        const obj = {
            date: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear(),
            hour: date.getHours(),
            min: date.getMinutes(),
            sec: date.getSeconds(),
            mSec: date.getMilliseconds(),
        }
        return obj;

    },//end of timestampToReadable

    sleep(second: number = 1) {
        return new Promise(resolve => {
            let ms = Number(second) * Number(1000);
            setTimeout(resolve, ms)
        });
    },//end of sleep

    array_move(arr: any, old_index: any, new_index: any) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr; // for testing
    },//end of array_move


}//end of export default