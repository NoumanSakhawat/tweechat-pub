import { showMessage } from "react-native-flash-message";

const DURATION = 1850 * 3;
//COMMENT ADDED FOR BITBUCKET
export default {
    getPollLength(d: any, h: any, m: any) {
        let days = d != 0 ? `${d} ${d > 1 ? 'days' : 'day'}` : '';
        let hours = h != 0 ? `${h} ${h > 1 ? 'hours' : 'hour'}` : '';
        let minutes = m != 0 ? `${m} ${m > 1 ? 'minutes' : 'minute'}` : '';
        let len = days + ' ' + hours + ' ' + minutes;
        return len;
    },//end of getPollLength

    authToken() {
        return new Promise<string | boolean>(async (resolve) => {
            resolve(false);

        })//end of PROMISE
    },//end of authToken

    warningFlashMessage(message = '', description = '') {
        showMessage({
            message: `${message}`,
            description: `${description}`,
            type: "warning",
            duration: DURATION,
            textStyle: {
                textAlign: "left",
            },
            titleStyle: {
                textAlign: "left",
            },
        });
    },//end of warningFlashMessage

    successFlashMessage(message = '', description = '') {
        showMessage({
            message: `${message}`,
            description: `${description}`,
            type: "success",
            duration: DURATION,
            textStyle: {
                textAlign: "left",
            },
            titleStyle: {
                textAlign: "left",
            },
        });
    },//end of successFlashMessage

    errorFlashMessage(message = '', description = '') {
        showMessage({
            message: `${message}`,
            description: `${description}`,
            type: "danger",
            duration: DURATION,
            textStyle: {
                textAlign: "left",
            },
            titleStyle: {
                textAlign: "left",
            },
        });
    },//end of errorFlashMessage

}//end of export DEFAULT