import React, { Component } from 'react';
import { StyleProp, TextStyle } from 'react-native';
import { ViewStyle } from 'react-native';
import { StyleSheet, View, ColorValue, TouchableOpacity, TouchableOpacityProps, ActivityIndicator, } from 'react-native';
import colors from '../constants/colors';
import { emptyValidate } from '../helper/HelperFunctions';
import Text from "./Text";
//END OF IMPORT's


interface componentInterface {
    width?: number | string;
    text?: any;

    border?: ColorValue;
    backgroundColor?: ColorValue;
    textColor?: ColorValue;

    loading?: boolean;

    style?: StyleProp<ViewStyle>;
    containerStyle?: StyleProp<ViewStyle>;
    textStyle?: StyleProp<TextStyle>;

    textTransform?: 'none' | 'capitalize' | 'uppercase' | 'lowercase' | undefined;
}//end of INTERFACE 

interface all extends componentInterface, TouchableOpacityProps { }

export default class Button extends Component<all, any> {

    public static defaultProps = {
        width: "100%",
        text: '',

        border: null,

        loading: false,

        backgroundColor: colors.primary,
        textColor: colors.white,

        textTransform: 'uppercase'
    };//end of DEFAULT PROPS DECLARATION

    render = () => {
        const { width, text, border, backgroundColor, textColor, loading, style, containerStyle, textStyle, textTransform, ...otherProps } = this.props;
        const hasBorder = emptyValidate(border);

        return (

            <TouchableOpacity style={[containerStyle, styles.containerStyle, {
                ...!hasBorder && { backgroundColor: backgroundColor },
                width,
                ...!hasBorder && styles.shadow,
                ...hasBorder && {
                    borderColor: border,
                    borderWidth: 1,
                }
            }, style,]}
                {...otherProps}>
                {loading ?
                    hasBorder ?
                        <ActivityIndicator size={"small"} color={border} />
                        :
                        <ActivityIndicator size={"small"} color={colors.white} />
                    :
                    <Text style={[styles.text, {
                        ...hasBorder ? {
                            color: border,
                        } : {
                            color: textColor,
                        },
                        textTransform: textTransform,
                    }, textStyle]}>{text}</Text>
                }
            </TouchableOpacity>
        )
    } // end of Function Render

} //end of class Button


const styles = StyleSheet.create({
    containerStyle: {
        minHeight: 48,
        borderRadius: 8,
        justifyContent: "center",

    },
    shadow: {
        shadowColor: `rgba(0,0,0,0)`,
        shadowOffset: {
            width: 0,
            height: 20,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    },
    text: {
        color: colors.white,
        textAlign: "center",
        fontSize: 14,

        fontWeight: "bold",
    },
}); //end of StyleSheet STYLES
