import React, { Component } from 'react';
import { KeyboardAvoidingView, Modal as RNModal, ModalProps, StyleSheet, TouchableOpacity, View } from 'react-native';
import AppStyles from '../constants/AppStyles';
// import constants from "expo-constants";
//END OF IMPORT's

interface componentInterface {
    visible?: boolean;
    dismiss?: () => void;
    onDismiss?: () => void;

    width?: any;

    outsideDismissEnable?: boolean;

    useRefState?: boolean;

}//end of INTERFACE 

interface all extends ModalProps, componentInterface { }

export default class BottomModal extends Component<all, any> {
    public static defaultProps = {
        visible: true,
        dismiss() { },

        width: '100%',

        outsideDismissEnable: false,

        useRefState: false,
    };//end of DEFAULT PROPS DECLARATION

    state = {
        stateVisible: false,
    };

    showStateVisible = () => {
        this.setState({ stateVisible: true })
    };

    hideStateVisible = () => {
        this.setState({ stateVisible: false })
    };

    render() {
        const { visible, dismiss, width, outsideDismissEnable, onDismiss, useRefState } = this.props;
        const { ...otherProps } = this.props;
        const modalBackgroundStyle = { backgroundColor: 'rgba(0, 0, 0, 0.5)' };

        if (useRefState) {
            return (
                <RNModal
                    animationType='fade'
                    transparent={true}
                    visible={this.state.stateVisible}
                    onRequestClose={this.hideStateVisible}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={this.hideStateVisible}
                        onPressIn={this.hideStateVisible}
                        onPressOut={this.hideStateVisible}
                        disabled={outsideDismissEnable}
                        style={[styles.container, modalBackgroundStyle, {}]}>
                        {/* <KeyboardAvoidingView> */}
                        <View style={[styles.innerContainerTransparentStyle, { minWidth: width, }]}>
                            {this.props.children}
                        </View>
                        {/* </KeyboardAvoidingView> */}
                    </TouchableOpacity>
                </RNModal>
            )
        }
        else
            return (
                <RNModal
                    animationType='fade'
                    transparent={true}
                    visible={visible}
                    onRequestClose={dismiss}
                    {...otherProps}>


                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={dismiss}
                        onPressIn={dismiss}
                        onPressOut={dismiss}
                        disabled={outsideDismissEnable}
                        style={[styles.container, modalBackgroundStyle, {}]}>
                        {/* <KeyboardAvoidingView> */}
                        <View style={[styles.innerContainerTransparentStyle, { minWidth: width, }]}>
                            {this.props.children}
                        </View>
                        {/* </KeyboardAvoidingView> */}
                    </TouchableOpacity>
                </RNModal>
            )
    } // end of Function Render

} //end of class BottomModal


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 20,
        backgroundColor: '#ecf0f1',
        minHeight: 20,
        // paddingBottom: 20,
    },
    innerContainerTransparentStyle: {
        backgroundColor: '#fff',
        marginHorizontal: 0,//AppStyles.HORIZONTAL,
        borderRadius: 18,
        maxHeight: "90%",
        minHeight: 20,
    },
}); //end of StyleSheet STYLES
