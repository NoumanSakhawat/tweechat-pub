import React, { Component } from 'react';
import { ColorValue } from 'react-native';
import { StyleSheet, View, ActivityIndicator as RNActivityIndicator, ActivityIndicatorProps } from 'react-native';
import colors from '../constants/colors';
//END OF IMPORT's

interface componentInterface {
    color?: ColorValue;
}//end of INTERFACE 

//@ts-ignore
interface all extends ActivityIndicatorProps, componentInterface { }

export default class ActivityIndicator extends Component<all, any> {

    public static defaultProps = {
        color: colors.white,
    };//end of DEFAULT PROPS DECLARATION

    render = () => {
        const { color, ...otherProps } = this.props;
        return (
            <RNActivityIndicator color={color} {...otherProps} />
        )
    } // end of Function Render

} //end of class ActivityIndicator
