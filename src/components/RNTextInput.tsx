import React, { Component } from 'react';
import { Button, Keyboard, Platform } from 'react-native';
import { StyleSheet, View, TextInputProps, TextInput, StyleProp, TextStyle, InputAccessoryView } from 'react-native';
import AppStyles from '../constants/AppStyles';
import { IMLocalized } from '../locales/IMLocalization';
//END OF IMPORT's

const inputAccessoryViewID = 'uniqueID';

interface componentInterface {
    style?: StyleProp<TextStyle>;
    useFlex?: boolean;

    showDone?: boolean;

    customRef?: any;
}//end of INTERFACE 

interface all extends componentInterface, TextInputProps {
}//end of INTERFACE 

export default class RNTextInput extends Component<all, any> {
    public static defaultProps = {
        useFlex: true,

        showDone: false,

        customRef: React.createRef(),
    };//end of DEFAULT PROPS DECLARATION

    render = () => {
        const { style, useFlex, showDone, customRef, ...otherProps } = this.props;
        if (Platform.OS === "ios")
            return (
                <>
                    <InputAccessoryView nativeID={inputAccessoryViewID}>
                        {showDone &&
                            <View style={styles.accessory}>
                                <Button
                                    onPress={() => Keyboard.dismiss()}
                                    title={IMLocalized(`Done`)}
                                />
                            </View>
                        }
                    </InputAccessoryView>

                    <TextInput
                        ref={customRef}
                        style={[styles.textinput, style,
                        useFlex && {
                            flex: 1,
                        }]}
                        autoCorrect={false}
                        inputAccessoryViewID={inputAccessoryViewID}
                        {...otherProps}
                    />


                </>
            )
        return (
            <>
                <TextInput
                    style={[styles.textinput, style,
                    useFlex && {
                        flex: 1,
                    }]}
                    autoCorrect={false}
                    inputAccessoryViewID={inputAccessoryViewID}
                    {...otherProps}
                />
            </>
        )
    } // end of Function Render

} //end of class RNTextInput


const styles = StyleSheet.create({
    textinput: {
        backgroundColor: "#EDF0F7",
        padding: 12,
        marginLeft: 12,
        borderRadius: 8,
        color: "#666666",
        fontSize: 14,

    },
    //@ts-ignore
    accessory: {
        ...AppStyles.accessory,
        zIndex: 999,
        minHeight: 48,
    },
}); //end of StyleSheet STYLES
