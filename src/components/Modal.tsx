import React, { Component } from 'react';
import { KeyboardAvoidingView, Modal as RNModal, ModalProps, StyleSheet, View } from 'react-native';
import KeyboardSpacer from './KeyboardSpacer';
// import constants from "expo-constants";
//END OF IMPORT's

interface componentInterface {
    visible?: boolean;
    dismiss?: () => void;
    width?: any;
}//end of INTERFACE 

interface all extends ModalProps, componentInterface { }

export default class Modal extends Component<all, any> {
    public static defaultProps = {
        visible: false,
        dismiss() { },

        width: '60%'
    };//end of DEFAULT PROPS DECLARATION

    render() {
        const { visible, dismiss, width } = this.props;
        const { ...otherProps } = this.props;
        const modalBackgroundStyle = { backgroundColor: 'rgba(0, 0, 0, 0.5)' };
        return (
            <RNModal
                animationType='fade'
                transparent={true}
                visible={visible}
                onRequestClose={dismiss}
                {...otherProps}>

                <View style={[styles.container, modalBackgroundStyle, {}]}>
                    <KeyboardAvoidingView>
                        <View style={[styles.innerContainerTransparentStyle, { minWidth: width, }]}>

                            {this.props.children}

                        </View>

                    </KeyboardAvoidingView>
                    <KeyboardSpacer />
                </View>
            </RNModal>
        )
    } // end of Function Render

} //end of class Modal


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20,
        backgroundColor: '#ecf0f1',

    },
    innerContainerTransparentStyle: {
        backgroundColor: '#fff',
        minWidth: "60%",
        borderRadius: 5
    },
}); //end of StyleSheet STYLES
