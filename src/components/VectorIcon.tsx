
import React from 'react';
import { StyleProp, StyleSheet, View, ViewStyle } from "react-native";
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Foundation from 'react-native-vector-icons/Foundation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import colors from '../constants/colors';

interface CIInterface {
    iconType: 'Ionicons' | 'AntDesign' | 'Entypo' | 'EvilIcons' | 'Feather' | 'FontAwesome' | 'Fontisto' | 'MaterialCommunityIcons' | 'MaterialIcons' | "Foundation" | "SimpleLineIcons";
    style: StyleProp<ViewStyle>;
    name: any;
    size: number;
    color: any;
    onPress(): any;

    shadow?: boolean;
}

export default class VectorIcon extends React.Component<CIInterface, any> {
    public static defaultProps = {
        iconType: 'Ionicons',
        name: 'ios-camera',
        color: '#000',
        size: 20,
        style: null,
        onPress() { },

        shadow: false,
    };

    constructor(props: CIInterface) {
        super(props);
    }

    icon = () => {
        let { iconType, style, color, size, name } = this.props;

        if (iconType === 'Ionicons') {
            return <Ionicons name={name} color={color} size={size} style={[style]} />
        } else if (iconType === 'AntDesign') {
            return <AntDesign name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'Entypo') {
            return <Entypo name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'EvilIcons') {
            return <EvilIcons name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'Feather') {
            return <Feather name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'FontAwesome') {
            return <FontAwesome name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'Fontisto') {
            return <Fontisto name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'MaterialCommunityIcons') {
            return <MaterialCommunityIcons name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'MaterialIcons') {
            return <MaterialIcons name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'Foundation') {
            return <Foundation name={name} color={color} size={size} style={[style]} />;
        } else if (iconType === 'SimpleLineIcons') {
            return <SimpleLineIcons name={name} color={color} size={size} style={[style]} />;
        }
        else {
            return <Ionicons name={name} color={color} size={size} style={[style]} />;
        }

    };

    render = () => {
        const { shadow, color } = this.props;
        if (shadow) {
            return (
                <View style={[styles.shadow, {
                    shadowColor: color
                }]}>
                    {this.icon()}
                </View>
            )
        }
        return (
            this.icon()
        )
    } // end of Function Render
} //end of class VectorIcon


const styles = StyleSheet.create({
    shadow: {
        shadowColor: colors.primary,
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.90,
        shadowRadius: 12.35,

        elevation: 19,
    }
});