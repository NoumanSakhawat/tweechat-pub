import React, { Component } from 'react';
import { Button, Keyboard, View } from 'react-native';
import { StyleSheet, TextInputProps, StyleProp, ViewStyle, Platform, InputAccessoryView } from 'react-native';
import { OutlinedTextField } from '../../lib/OutlinedTextField';
import Text from '../components/Text';
import AppStyles from '../constants/AppStyles';
import colors from '../constants/colors';
import { emptyValidate } from '../helper/HelperFunctions';
import { IMLocalized } from '../locales/IMLocalization';
//END OF IMPORT's

const inputAccessoryViewID = 'outlineUniqueID';
interface componentInterface {
    containerStyle?: StyleProp<ViewStyle>;
    inputContainerStyle?: StyleProp<ViewStyle>;
    style?: StyleProp<ViewStyle>;
    value?: any;
    error?: any;

    showDone?: boolean;

    customRef?: any;
}//end of INTERFACE 

//@ts-ignore
interface all extends componentInterface, TextInputProps {

}

const cColor = {
    border: "#DADADA",
    placeholder: "#C4C4C4",
}

export default class TextInput extends Component<all, any> {
    fieldRef = React.createRef();

    public static defaultProps = {
        error: '',

        showDone: false,

        customRef: React.createRef(),
    };//end of DEFAULT PROPS DECLARATION

    render() {
        const { containerStyle, inputContainerStyle, style, error, showDone, customRef, ...otherProps } = this.props;
        const isPlaceholder = "value" in this.props ? `${this.props.value}`.length == 0 ? true : false : true;
        const isError = emptyValidate(error) ? true : false;

        return (
            <>
                <OutlinedTextField
                    containerStyle={[styles.containerStyle, containerStyle,
                    ]}
                    inputContainerStyle={[styles.inputContainerStyle, inputContainerStyle,

                    ]}
                    style={[
                        // isPlaceholder ? [styles.placeholderStyle] :

                        styles.textInput, style]}
                    placeholderTextColor={cColor.placeholder}
                    tintColor={isError ? 'red' : colors.primary}
                    baseColor={isError ? 'red' : cColor.border}
                    errorColor={'red'}
                    autoCorrect={false}
                    inputAccessoryViewID={inputAccessoryViewID}
                    error={error}
                    ref={customRef}

                    {...otherProps}
                />
                {/* {isError &&
                    <Text style={styles.errorText}>{error}</Text>
                } */}

                {Platform.OS === "ios" &&
                    <InputAccessoryView nativeID={inputAccessoryViewID}>
                        {showDone &&
                            <View style={styles.accessory}>
                                <Button
                                    onPress={() => Keyboard.dismiss()}
                                    title={IMLocalized(`Done`)}
                                />
                            </View>
                        }
                    </InputAccessoryView>
                }
            </>
        )
    } // end of Function Render

} //end of class TextInput


const styles = StyleSheet.create({
    containerStyle: {
        paddingVertical: 2,
    },
    inputContainerStyle: {
        // ...Platform.OS === "ios" && { borderBottomColor: '#58595B' },
        // ...Platform.OS === "ios" && { borderBottomWidth: 0.25, },
        // height: 10
    },
    placeholderStyle: {
        color: cColor.placeholder,
        fontSize: 12,
    },
    textInput: {
        color: "#414042",
        fontSize: 12,
    },
    errorTextContainer: {
        marginLeft: 4,
        marginTop: 5,
    },
    errorText: {
        color: "red",
        fontSize: 12,
    },

    //@ts-ignore
    accessory: {
        ...AppStyles.accessory,
    },
}); //end of StyleSheet STYLES
