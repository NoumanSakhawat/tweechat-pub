import React, { Component } from 'react';
import { StyleSheet, Text as RText, StyleProp, TextStyle, TextProps } from 'react-native';
//END OF IMPORT's


interface componentInterface {
    style?: StyleProp<TextStyle>;
}//end of INTERFACE 

interface all extends componentInterface, TextProps { }


export default class Text extends Component<all, any> {

    public static defaultProps = {
    };//end of DEFAULT PROPS DECLARATION

    render() {
        let { style, ...otherProps } = this.props;
        return (
            <RText
                style={[styles.text, style]}
                {...otherProps}
            >{this.props.children}</RText>
        )
    } // end of Function Render

} //end of class Text


const styles = StyleSheet.create({
    text: {
        color: "#212121",
        fontSize: 14,
    }
}); //end of StyleSheet STYLES
