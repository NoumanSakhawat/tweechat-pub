import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import Modal from '../components/Modal';
import Text from '../components/Text';
import colors from '../constants/colors';
import CameraFunctions from '../helper/CameraFunctions';
import { IMLocalized } from '../locales/IMLocalization';
//END OF IMPORT's

interface componentInterface {
    visible?: boolean;
    dismiss?: () => void;

    getImages(imagesArr: any): () => void;

    multipleAllow?: boolean;
}//end of INTERFACE 

export default class ImageSelector extends Component<componentInterface, any> {

    public static defaultProps = {
        visible: false,
        dismiss() { },
        getImages(imagesArr: any) { },

        multipleAllow: false,

    };//end of DEFAULT PROPS DECLARATION

    camera = async () => {
        const { getImages, dismiss } = this.props;

        const res: any = await CameraFunctions.camera(true);
        if (res) {
            for (const r of res) {
                console.log('CAMERA RES=> ', JSON.stringify(r.path));
            }
            getImages(res);
            //@ts-ignore
            dismiss();
        }
    }//end of camera

    gallery = async () => {
        const { multipleAllow, getImages, dismiss } = this.props;
        const res: any = await CameraFunctions.gallery(multipleAllow, true);
        if (res) {
            for (const r of res) {
                console.log('GALLERY RES=> ', JSON.stringify(r.path));
            }

            getImages(res);
            //@ts-ignore
            dismiss();
        }
    }//end of gallery

    render = () => {
        const { visible, dismiss } = this.props;
        return (
            <Modal
                visible={visible}
                dismiss={dismiss}
                width={"85%"}>
                <View style={styles.containerStyle}>
                    <Text style={styles.title}>{IMLocalized(`Add Photo!`)}</Text>

                    <TouchableOpacity onPress={this.camera}>
                        <Text style={styles.message}>{IMLocalized(`Choose from camera`)}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.gallery}>
                        <Text style={styles.message}>{IMLocalized(`Choose from gallery`)}</Text>
                    </TouchableOpacity>

                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={styles.buttonNo}
                            onPress={dismiss}>
                            <Text style={styles.buttonNoText}>{IMLocalized(`Cancel`)}</Text>
                        </TouchableOpacity>


                    </View>
                </View>
            </Modal>
        )
    } // end of Function Render

} //end of class ImageSelector


const styles = StyleSheet.create({
    containerStyle: {
        paddingVertical: 16,
        paddingHorizontal: 20,
    },

    title: {
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 20,
        marginBottom: 16,
        color: `rgba(0,0,0,0.7)`,
    },
    message: {
        fontSize: 14,
        marginBottom: 16,
        color: `rgba(0,0,0,0.6)`,
    },

    buttonContainer: {
        marginTop: 20,
    },
    buttonNo: {
        backgroundColor: "#696969",
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 12,
    },
    buttonNoText: {
        fontSize: 14,
        color: `#fff`,
        textAlign: "center",
    },
}); //end of StyleSheet STYLES
