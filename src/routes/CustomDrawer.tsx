import React, { Component } from 'react';
import { Image } from 'react-native';
import { FlatList, Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import CustomStatusBar from '../components/Statusbar';
import Text from '../components/Text';
import CustomIcon from '../components/VectorIcon';
import AppStyles from '../constants/AppStyles';
import colors from '../constants/colors';
import LocalAssets from '../constants/LocalAssets';
import AppFunctions from '../helper/AppFunctions';
import { IMLocalized } from '../locales/IMLocalization';
import ROUTES from './ROUTES';
//END OF IMPORT's

const KEYS = {
    explore: "explore",
    message: "message",
    notification: "notification",
    profile: "profile",
    setting: "setting",
    logout: "logout",
};

const staticData: any = [{
    id: 1,
    name: IMLocalized(`Explore`),
    tintColor: "#808080",
    icon: LocalAssets.Drawer.explore,
    key: KEYS.explore,
}, {
    id: 2,
    name: IMLocalized(`All Messages`),
    tintColor: "#808080",
    icon: LocalAssets.Drawer.message,
    key: KEYS.message,
}, {
    id: 3,
    name: IMLocalized(`Notification`),
    tintColor: "#808080",
    icon: LocalAssets.Drawer.notification,
    key: KEYS.notification,
}, {
    id: 4,
    name: IMLocalized(`My Profile`),
    tintColor: "#808080",
    icon: LocalAssets.Drawer.profile,
    key: KEYS.profile,
}, {
    id: 5,
    name: IMLocalized(`Setting`),
    tintColor: "#808080",
    icon: LocalAssets.Drawer.setting,
    key: KEYS.setting,
}];

interface componentInterface {
    setIsLoggedIn?: (isLoggedIn: boolean) => void;
}//end of INTERFACE 

interface IState {
    userData?: any;
    data?: any;
}
interface all extends componentInterface, IState { }

export default class CustomDrawer extends Component<all, any> {

    public static defaultProps = {

    };//end of DEFAULT PROPS DECLARATION

    state = {
        data: staticData,
    };

    closeDrawer = () => {
        //@ts-ignore
        this.props.navigation.closeDrawer();
    }//end of closeDrawer

    render = () => {
        const { data } = this.state;
        return (
            <View style={{ ...styles.containerStyle }}>
                <CustomStatusBar />

                {/* ******************** LOGO Start ******************** */}
                <View style={styles.userProfilePrimaryContainer}>
                    <View style={styles.logoContainer}>
                        <FastImage
                            source={LocalAssets.Drawer.logo}
                            resizeMode={FastImage.resizeMode.cover}
                            style={styles.logo} />
                    </View>

                    <Text style={styles.appname}>{IMLocalized(`TweeChat`)}</Text>
                </View>

                <View style={styles.border} />
                {/* ******************** LOGO End ******************** */}


                {/* ******************** DRAWER's Item Start ******************** */}
                <FlatList
                    data={data}
                    style={styles.flatlist}
                    renderItem={this._renderItem} />

                {/* ******************** DRAWER's Item End ******************** */}

                <View style={itemStyles.lastIndexPrimaryContainer}>
                    <View style={styles.border} />

                    <TouchableOpacity
                        style={itemStyles.lastIndexContainer}
                        onPress={() => { this.onItemPress(KEYS.logout) }}>
                        <Image
                            source={LocalAssets.Drawer.logout}
                            resizeMode={FastImage.resizeMode.stretch}
                            style={itemStyles.icon} />
                        <Text style={itemStyles.logoutText}>{IMLocalized(`Logout`)}</Text>

                    </TouchableOpacity>

                </View>
            </View>
        )
    } // end of Function Render

    onItemPress = (key: any) => {
        const { navigation }: any = this.props;
        if (key === KEYS.explore) {
            navigation.navigate(ROUTES.SearchTab);
        } else if (key === KEYS.message) {
            navigation.navigate(ROUTES.MessagesTab);
        } else if (key === KEYS.notification) {
            navigation.navigate(ROUTES.NotificationTab);
        } else if (key === KEYS.profile) {

        } else if (key === KEYS.setting) {
            navigation.navigate(ROUTES.Settings);
        } else if (key === KEYS.logout) {
            //@ts-ignore
            this.props.setIsLoggedIn(false);
        }
    }//end of onItemPress

    _renderItem = ({ item, index }: any) => {
        return (
            <TouchableOpacity style={itemStyles.primaryContainer}
                onPress={() => { this.onItemPress(item.key) }}>
                <Image
                    source={item.icon}
                    resizeMode={FastImage.resizeMode.stretch}
                    style={itemStyles.icon} />
                <Text style={itemStyles.text}>{item.name}</Text>

            </TouchableOpacity>
        )
    }//end of _renderItem

} //end of class CustomDrawer

export const itemStyles = StyleSheet.create({
    primaryContainer: {
        flexDirection: "row",
        alignItems: "center",
        paddingTop: 24,
        paddingBottom: 8,
        paddingLeft: 36,
    },
    lastIndexPrimaryContainer: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
    },
    lastIndexContainer: {
        flexDirection: "row",
        alignItems: "center",
        paddingTop: 24,
        paddingBottom: 32,
        paddingLeft: 36,
    },
    icon: {
        height: 20,
        width: 20,
    },
    text: {
        marginLeft: 22,
        fontSize: 16,
        color: "#221F2E",
    },
    logoutText: {
        marginLeft: 22,
        fontSize: 16,
        color: colors.primary,
    },
});//end of  itemStyles

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: colors.background,
    },

    userProfilePrimaryContainer: {
        flexDirection: "row",
        alignItems: "center",
        // justifyContent: "center",
        paddingLeft: 32,
        marginTop: 20,
    },
    logoContainer: {
        alignItems: "center",
    },
    logo: {
        height: 35,
        width: 35,
    },
    appname: {
        fontSize: 24,
        color: colors.primary,
        textAlign: "center",
        marginLeft: 16,
    },
    border: {
        ...AppStyles.border,
        marginTop: 33,
    },

    flatlist: {
        marginTop: 20,
    },

    userNameText: {
        fontSize: 16,
        color: colors.white,
        textAlign: "center",
        marginTop: 8,
        marginBottom: 16,
    },

}); //end of StyleSheet STYLES