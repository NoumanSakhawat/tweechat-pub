export default {
    TabNav: "TabNav",
    DrawerNav: "DrawerNav",

    //DRAWER's Name Start
    Settings: "Settings",


    //TAB's Name Start
    HomeTab: "HomeTab",
    SearchTab: "SearchTab",
    NotificationTab: "NotificationTab",
    MessagesTab: "MessagesTab",

    //TAB's Name End

    SignIn: "SignIn",
    SignUp: "SignUp",
    ForgotPassword: "ForgotPassword",

    Onboarding: "Onboarding",
    EnterNumber: "EnterNumber",
    OTP: "OTP",
    SetupProfile: "SetupProfile",


    Home: "Home",
    AddPost: "AddPost",
    GIF: "GIF",
    Location: "Location",
    VideoPlay: "VideoPlay",
    CommentView: "CommentView",

    Notification: "Notification",
    Message: "Message",
    Chat: "Chat",


    UserProfile: "UserProfile",


    Blank: "Blank",
}//end of EXPORT DEFAULT