import React from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Text from '../components/Text';
import colors from '../constants/colors';
import LocalAssets from "../constants/LocalAssets";
import { emptyValidate } from '../helper/HelperFunctions';
import { IMLocalized } from '../locales/IMLocalization';
import ROUTES from './ROUTES';

export const MyCustomTab = ({ state, descriptors, navigation }: any) => {
    return (
        <View style={styles.primaryContainer}>
            {state.routes.map((route: any, index: any) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    if (route.name === ROUTES.HomeTab) {
                        navigation.navigate(ROUTES.HomeTab);
                    } else if (route.name === ROUTES.SearchTab) {
                        navigation.navigate(ROUTES.SearchTab);
                    } else if (route.name === ROUTES.NotificationTab) {
                        navigation.navigate(ROUTES.NotificationTab);
                    } else if (route.name === ROUTES.MessagesTab) {
                        navigation.navigate(ROUTES.MessagesTab);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                let imageSource = null;
                let customLabel = '';
                if (label === ROUTES.HomeTab) {
                    imageSource = LocalAssets.BottomTab.home;
                } else if (label === ROUTES.SearchTab) {
                    imageSource = LocalAssets.BottomTab.search;
                } else if (label === ROUTES.NotificationTab) {
                    imageSource = LocalAssets.BottomTab.notification;
                } else if (label === ROUTES.MessagesTab) {
                    imageSource = LocalAssets.BottomTab.message;
                }

                return imageSource === null ? (<View key={index} />) : (
                    <TouchableOpacity
                        key={index}
                        accessibilityRole="button"
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        activeOpacity={1}
                        onPress={onPress}
                        style={{
                            ...styles.container,
                            ...styles.shadow,
                        }}>
                        {emptyValidate(imageSource) &&
                            <Image
                                source={imageSource}
                                style={[styles.image, {
                                    tintColor: isFocused ? colors.primary : "#C9C9C9"
                                }]}

                                resizeMode={FastImage.resizeMode.contain}
                            />
                        }
                        {emptyValidate(customLabel) &&
                            <Text style={{
                                ...styles.text,
                                fontSize: 12,
                                color: isFocused ? colors.primary : "#C9C9C9",
                            }}>{customLabel}</Text>
                        }
                    </TouchableOpacity>
                );
            })}
        </View>
    );
}//end of customTab

const styles = StyleSheet.create({
    primaryContainer: {
        flexDirection: 'row',
        backgroundColor: colors.background,

    },
    container: {
        flex: 1,
        backgroundColor: colors.white,


        alignItems: "center",
        justifyContent: "center",

        minHeight: 55,
    },

    shadow: {
        shadowRadius: 2,
        shadowOffset: {
            width: 0,
            height: -5,
        },
        shadowColor: '#000000',
        shadowOpacity: 0.04,
        elevation: 4,
    },
    image: {
        height: 20,
        width: 20,
    },
    text: {
        marginTop: 8,
        marginBottom: 0,
        textAlign: 'center',
    },
})