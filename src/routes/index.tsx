import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import React, { Component, useState } from 'react';
import ROUTES from './ROUTES';
import { MyCustomTab } from './MyCustomTab';
// screens Import
import Onboarding from '../screens/Onboarding';
import EnterNumber from '../screens/EnterNumber';
import OTP from '../screens/OTP';
import SetupProfile from '../screens/SetupProfile';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';
import ForgotPassword from '../screens/ForgotPassword';

import Home from '../screens/home';
import UserProfile from '../screens/UserProfile';
import AddPost from '../screens/AddPost';
import VideoPlay from '../screens/VideoPlay';
import GIF from '../screens/GIF';
import Location from '../screens/Location';
import CommentView from '../screens/CommentView';

import Notification from '../screens/Notification';
import Message from '../screens/Message';
import Chat from '../screens/Chat';

import Settings from '../screens/Settings';


import Blank from '../screens/Blank';
import CustomDrawer from './CustomDrawer';
// screens Import End

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const initialRouteName = ROUTES.Onboarding;

const index = () => {

    const [isLoggedIn, setIsLoggedIn] = useState(true);

    const AuthStack = () => {
        const signInScreen = (props: any) => <SignIn {...props} setIsLoggedIn={setIsLoggedIn} />
        return (
            <Stack.Navigator screenOptions={{ headerShown: false }}>

                <Stack.Screen name={ROUTES.Onboarding} component={Onboarding} />
                <Stack.Screen name={ROUTES.EnterNumber} component={EnterNumber} />
                <Stack.Screen name={ROUTES.OTP} component={OTP} />
                <Stack.Screen name={ROUTES.SetupProfile} component={SetupProfile} />

                <Stack.Screen name={ROUTES.SignIn} component={signInScreen} />
                <Stack.Screen name={ROUTES.SignUp} component={SignUp} />
                <Stack.Screen name={ROUTES.ForgotPassword} component={ForgotPassword} />

            </Stack.Navigator>
        )
    };//end of AuthStack

    const HomeStack = () => {
        return (
            <Stack.Navigator screenOptions={{ headerShown: false }}>

                <Stack.Screen name={ROUTES.Home} component={Home} />
                <Stack.Screen name={ROUTES.AddPost} component={AddPost} />
                <Stack.Screen name={ROUTES.GIF} component={GIF} />
                <Stack.Screen name={ROUTES.Location} component={Location} />
                <Stack.Screen name={ROUTES.VideoPlay} component={VideoPlay} />
                <Stack.Screen name={ROUTES.CommentView} component={CommentView} />

                <Stack.Screen name={ROUTES.UserProfile} component={UserProfile} />

            </Stack.Navigator>
        )
    };//end of HomeStack

    const SearchStack = () => {
        return (
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name={ROUTES.Blank} component={Blank} />
            </Stack.Navigator>
        )
    };//end of SearchStack

    const NotificationStack = () => {
        return (
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name={ROUTES.Notification} component={Notification} />
            </Stack.Navigator>
        )
    };//end of NotificationStack

    const MessagesStack = () => {
        return (
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name={ROUTES.Message} component={Message} />
                <Stack.Screen name={ROUTES.Chat} component={Chat} />
            </Stack.Navigator>
        )
    };//end of MessagesStack

    const TabNav = () => {
        return (
            <Tab.Navigator
                tabBar={props => <MyCustomTab {...props} />}
                lazy>
                <Tab.Screen name={ROUTES.HomeTab} component={HomeStack} />
                <Tab.Screen name={ROUTES.SearchTab} component={SearchStack} />
                <Tab.Screen name={ROUTES.NotificationTab} component={NotificationStack} />
                <Tab.Screen name={ROUTES.MessagesTab} component={MessagesStack} />

            </Tab.Navigator>
        )
    };//end of TabNav

    const SettingStack = () => {
        return (
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name={ROUTES.Settings} component={Settings} />
            </Stack.Navigator>
        )
    };//end of SettingStack
    const DrawerNav = () => {
        return (
            <Drawer.Navigator
                drawerContent={(props) => <CustomDrawer {...props} setIsLoggedIn={setIsLoggedIn} />}>
                <Drawer.Screen name={ROUTES.TabNav} component={TabNav} />
                <Drawer.Screen name={ROUTES.Settings} component={SettingStack} />
            </Drawer.Navigator>
        )
    };//end of DrawerNav

    return (
        <NavigationContainer independent={true}>
            {isLoggedIn ?
                DrawerNav()
                :
                AuthStack()
            }

        </NavigationContainer>

    )
}//end of index

export default index;