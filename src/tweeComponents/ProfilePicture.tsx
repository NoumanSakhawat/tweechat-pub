import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import FastImage, { Source } from 'react-native-fast-image';
import colors from '../constants/colors';
import { emptyValidate } from '../helper/HelperFunctions';
//END OF IMPORT's


interface componentInterface {
    source: Source,
    hasBorder?: boolean;
    size: number;
}//end of INTERFACE 
const IMAGE_SIZE = 32;
const BORDER_DIFF = 4;

export default class ProfilePicture extends Component<componentInterface, any> {
    public static defaultProps = {
        source: null,
        hasBorder: false,
        size: 32,
    };//end of DEFAULT PROPS DECLARATION

    render = () => {
        const { source, hasBorder, size } = this.props;
        if (hasBorder) {
            return (
                <View style={[styles.containerBorder, {
                    width: size,
                    height: size,
                    borderRadius: size,
                }]}>
                    {emptyValidate(source) &&
                        <FastImage
                            style={[styles.imageBorder, {
                                width: size - BORDER_DIFF,
                                height: size - BORDER_DIFF,
                                borderRadius: size - BORDER_DIFF,
                            }]}
                            source={source}
                            resizeMode={FastImage.resizeMode.contain}
                        />
                    }
                </View>
            )
        } else {
            return (
                <View style={[styles.container, {
                    width: size,
                    height: size,
                    borderRadius: size,
                }]}>
                    <FastImage
                        style={[styles.image, {
                            width: size,
                            height: size,
                            borderRadius: size,
                        }]}
                        source={source}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                </View>
            )
        }

    } // end of Function Render

} //end of class ProfilePicture

const bgColor = `rgba(0,0,0,0.05)`

const styles = StyleSheet.create({
    containerBorder: {
        width: IMAGE_SIZE,
        height: IMAGE_SIZE,
        borderRadius: IMAGE_SIZE,
        borderColor: colors.primary,
        borderWidth: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: bgColor,
    },
    imageBorder: {
        width: IMAGE_SIZE - BORDER_DIFF,
        height: IMAGE_SIZE - BORDER_DIFF,
        borderRadius: IMAGE_SIZE - BORDER_DIFF,
    },
    container: {
        width: IMAGE_SIZE,
        height: IMAGE_SIZE,
        borderRadius: IMAGE_SIZE,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: bgColor,
    },
    image: {
        width: IMAGE_SIZE,
        height: IMAGE_SIZE,
        borderRadius: IMAGE_SIZE,
    },
}); //end of StyleSheet STYLES
