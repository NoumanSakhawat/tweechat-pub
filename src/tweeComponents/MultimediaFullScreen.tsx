import Slider from '@react-native-community/slider';
import React, { Component } from 'react';
import { Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native';
import FastImage, { Source } from 'react-native-fast-image';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Pagination } from 'react-native-snap-carousel';
import AutoHeightImage from '../../lib/react-native-auto-height-image/AutoHeightImage';
import Text from '../components/Text';
import colors from '../constants/colors';
import LocalAssets from '../constants/LocalAssets';
import Icon from 'react-native-vector-icons/FontAwesome';
import { IMLocalized } from '../locales/IMLocalization';
import HelperFunctions from '../helper/HelperFunctions';
import Thought from './Thought';
import Faker from '../helper/Faker';
import AppStyles from '../constants/AppStyles';
import ProfilePicture from './ProfilePicture';
import RNTextInput from '../components/RNTextInput';
import Video from 'react-native-video';
import VectorIcon from '../components/VectorIcon';
import { UIActivityIndicator } from 'react-native-indicators';
//END OF IMPORT's

const WIDTH = Dimensions.get('screen').width;
const HEIGHT = Dimensions.get('screen').height;

interface componentInterface {
    activeIndex: number;
    dotsLength: number;

    index: number;

    type?: "image" | "video";

    path?: Source | number;

    views?: number | string;

    thought: {
        isMyLiked: boolean,
        like: number | string,
        comment: number | string,
        isMyRetweet: boolean,
        retweet: number | string,
    }
}//end of INTERFACE 

const userProfilePicture = `https://i.picsum.photos/id/1067/560/560.jpg?hmac=Ll-JQdFwVR5GpCPebIkJK1N77MsHLwskZBJKUjS8yhI`;
const videoURL = `https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4`;





export default class MultimediaFullScreen extends Component<componentInterface, any> {
    public static defaultProps = {
        activeIndex: -1,
        dotsLength: 0,

        index: -1,

        type: null,
        path: null,

        thought: {
            isMyLiked: false,
            like: 0,
            comment: 0,
            isMyRetweet: false,
            retweet: 0,
        },
        views: 0,
    };//end of DEFAULT PROPS DECLARATION

    timerId: any = null;
    player: any = null;
    carousel: any = null;
    state = {
        icon: { uri: Icon.getImageSourceSync('circle', 12, colors.primary).uri },
        rate: 1,
        volume: 1,
        muted: false,
        resizeMode: 'cover',

        duration: 0.0,
        playableDuration: 0.0,

        currentTime: 0.0,
        currentTimeText: 0.0,

        paused: false,
        videoOverlay: false,
        videoOverlayAlways: false,
        isBuffering: false,

        comment: '',
    }

    componentDidMount = () => {
        const myicon = Icon.getImageSourceSync('circle', 12, colors.primary)
        this.setState({
            icon: { uri: myicon.uri }
        })
    };//end of 

    _renderFooter = (showSlider = false) => {
        const { currentTimeText, currentTime, duration, comment } = this.state;
        const { activeIndex, dotsLength, thought, views } = this.props;

        return (

            <View style={footerStyles.footerPrimaryContainer} >

                <Pagination
                    dotsLength={dotsLength}
                    activeDotIndex={activeIndex}
                    containerStyle={footerStyles.paginationContainer}
                    // dotStyle={footerStyles.paginationDot}
                    dotColor={'rgba(0, 0, 0, 0.5)'}
                    inactiveDotColor={colors.black}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                    carouselRef={this.carousel}
                    tappableDots={!!this.carousel}
                />

                {showSlider &&
                    <Slider
                        style={footerStyles.slider}
                        value={currentTime}
                        minimumValue={0}
                        maximumValue={duration}
                        minimumTrackTintColor={colors.primary}
                        maximumTrackTintColor="rgba(0, 0, 0, 0.24)"
                        thumbTintColor={colors.primary}
                        // thumbImage={this.state.icon}

                        onSlidingStart={(val) => {
                            this.setState({
                                videoOverlayAlways: false,
                                paused: true,
                            });
                        }}
                        onSlidingComplete={(val) => {
                            this.setState({
                                videoOverlayAlways: false,
                                paused: false,
                                isBuffering: true,
                            });
                            if (this.player !== null) {
                                this.player.seek(val)
                            }
                        }}
                        onValueChange={(val) => {
                            if (this.player !== null) {
                                this.setState({
                                    videoOverlayAlways: false,
                                    currentTimeText: val,
                                });
                            }

                            this.setState({
                                isBuffering: true,
                            });

                        }}
                    />
                }

                {/* ******************** VIEW's AND TIME Start ******************** */}

                {showSlider &&
                    <View style={footerStyles.viewTimeContainer}>
                        <Text>{`${views} ${IMLocalized(`views`)}`}</Text>

                        <View style={footerStyles.durationContainer}>
                            <Text>{`${HelperFunctions.fancyTimeFormat(currentTimeText)} / ${HelperFunctions.fancyTimeFormat(duration)}`}</Text>
                        </View>
                    </View>
                }

                {/* ******************** VIEW's AND TIME End ******************** */}

                {/* ******************** THOUGHT Start ******************** */}
                <View style={footerStyles.thoughtContainer}>
                    <Thought data={thought} />


                    <TouchableOpacity style={footerStyles.viewAllTextContainer}>
                        <Text style={footerStyles.viewAllText}>{IMLocalized(`View all number Comments`).replace('number', thought.comment)}</Text>
                    </TouchableOpacity>

                </View>

                {/* ******************** THOUGHT End ******************** */}

                {/* ******************** LEAVE COMMENT Start ******************** */}


                <View style={footerStyles.commentPrimaryContainer}>
                    <ProfilePicture
                        source={{ uri: userProfilePicture }}
                        size={32}
                    />
                    <RNTextInput
                        placeholder={IMLocalized(`Leave your comment`)}
                        placeholderTextColor={"#666666"}
                        defaultValue={comment}
                        onChangeText={(text) => {
                            this.setState({
                                comment: text
                            })
                        }}
                    />

                </View>

                {/* ******************** LEAVE COMMENT End ******************** */}
            </View>

        )
    };//end of _renderFooter


    toggleVideoOverlay = () => {

        const { videoOverlay } = this.state;
        if (!videoOverlay) {

            this.timerId = setTimeout(() => {
                this.setState({ videoOverlay: false })

                clearTimeout(this.timerId);
            }, 6000)
        } else {
            clearTimeout(this.timerId);
        }
        this.setState({ videoOverlay: !videoOverlay })
    };//end of toggleVideoOverlay

    onLoad = (data: any) => {
        this.setState({
            videoOverlayAlways: false,
            duration: data.duration,
        })
    };//end of onLoad

    onProgress = (data: any) => {
        if (parseInt(data.playableDuration) < parseInt(data.currentTime)) {
            this.setState({
                isBuffering: true
            });
        } else {
            this.setState({
                isBuffering: false
            });
        }

        this.setState({
            playableDuration: data.playableDuration,
            currentTime: data.currentTime,
            currentTimeText: data.currentTime,
        });
    };//end of onProgress


    onEnd = () => {
        this.setState({
            videoOverlay: true,
            videoOverlayAlways: true,
            paused: true,
        });
    };//end of onEnd

    onAudioBecomingNoisy = () => {
        this.setState({
            paused: true,
        });
    };//end of onAudioBecomingNoisy

    onAudioFocusChanged = (event: any) => {
        this.setState({
            paused: !event.hasAudioFocus,
        });
    };//end of onAudioFocusChanged

    onBuffer = (event: any) => {
        // setBuffering(event.isBuffering);
    };//end of onBuffer

    _renderVideo = () => {
        const { rate, paused, volume, muted, resizeMode, videoOverlay, videoOverlayAlways, isBuffering } = this.state;

        const { path, } = this.props;

        return (
            <>
                <TouchableOpacity activeOpacity={1} onPress={this.toggleVideoOverlay}
                    style={videoStyles.primaryContainer}>
                    <Video
                        source={path}
                        ref={(ref: any) => {
                            this.player = ref
                        }}
                        style={videoStyles.video}
                        rate={rate}
                        paused={paused}
                        volume={volume}
                        muted={muted}
                        resizeMode={resizeMode}
                        onBuffer={this.onBuffer}
                        onLoad={this.onLoad}
                        onProgress={this.onProgress}
                        onEnd={this.onEnd}
                        onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                        onAudioFocusChanged={this.onAudioFocusChanged}
                        repeat={false}
                        bufferConfig={{
                            minBufferMs: 15000,
                            maxBufferMs: 50000,
                            bufferForPlaybackMs: 2500,
                            bufferForPlaybackAfterRebufferMs: 5000
                        }}
                    />

                    {videoOverlay ?
                        <>
                            <TouchableOpacity style={videoStyles.overlay} onPress={this.toggleVideoOverlay} />
                            <TouchableOpacity style={[videoStyles.pauseIcon]}
                                onPress={() => {
                                    if (videoOverlayAlways) {
                                        this.setState({
                                            videoOverlay: false,
                                            videoOverlayAlways: false,
                                            paused: false,
                                        })
                                        this.player.seek(0);
                                        return
                                    }
                                    this.setState({
                                        paused: !paused,
                                    })

                                }}>
                                <VectorIcon
                                    name={videoOverlayAlways ? "reload" : paused ? "play" : "pause"}
                                    size={50}
                                    color={colors.white} />
                            </TouchableOpacity>
                        </>
                        :
                        (isBuffering) &&
                        <>
                            <View style={videoStyles.overlay} />
                            <View style={[videoStyles.pauseIcon]}>
                                <UIActivityIndicator color='white' />
                            </View>
                        </>

                    }


                </TouchableOpacity>

            </>
        )
    };//end of _renderVideo

    render = () => {
        const { type, path, index, activeIndex } = this.props;
        return (
            <KeyboardAwareScrollView>
                <View style={{ flex: 1 }} />
                <View style={[styles.primaryContainer, styles.containerStyle]} >

                    {type === "image" &&
                        <AutoHeightImage
                            width={WIDTH}
                            source={path}
                            style={styles.image}
                        // resizeMode={FastImage.resizeMode.contain}
                        />
                    }

                    {(type === "video") &&
                        index === activeIndex ?
                        this._renderVideo()
                        : <View />
                    }



                </View>
                {this._renderFooter(type === "video" ? true : false)}
            </KeyboardAwareScrollView>
        )
    } // end of Function Render

} //end of class MultimediaFullScreen
const multimediaMarginVertical = 40;
const multimediaBG = `rgba(0,0,0,0.05)`;
const styles = StyleSheet.create({
    primaryContainer: {
        flex: 1,

    },
    containerStyle: {
        // flex: 1,
        alignItems: "center",
        justifyContent: "center",
        // paddingBottom: 40,
    },
    image: {
        marginTop: multimediaMarginVertical * 2,
        marginBottom: multimediaMarginVertical / 3,
        backgroundColor: multimediaBG,
        // maxHeight: HEIGHT / 2,
        width: WIDTH,

        borderRadius: 4,
    },
}); //end of StyleSheet STYLES


const footerStyles = StyleSheet.create({
    footerPrimaryContainer: {
        flex: 1,
        alignItems: "flex-start",
        // justifyContent: "center",
        // justifyContent: "flex-end",
    },
    paginationContainer: {
        marginTop: 6,
        paddingVertical: 8,
        alignSelf: "center",
        maxWidth: 50,
        width: 50,
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8
    },
    slider: {
        width: WIDTH - 36,
        height: 20,
        alignSelf: "center",
        marginTop: 20,
    },

    viewTimeContainer: {
        flexDirection: "row",
        alignItems: "center",
        alignSelf: "center",
        marginLeft: 18,
        marginTop: 12,
        marginHorizontal: 18,
    },
    durationContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end",
    },
    thoughtContainer: {
        marginHorizontal: AppStyles.HORIZONTAL * 1,

    },
    viewAllTextContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 10,
        // alignSelf: "center",
    },
    viewAllText: {
        fontSize: 12,
        color: "#87888B",
    },

    commentPrimaryContainer: {
        alignSelf: "center",
        flexDirection: "row",
        alignItems: "center",
        paddingTop: 28,
        paddingLeft: 18,
        paddingRight: 18,
        paddingBottom: 32,
    },
});//end of footerStyles

const videoTop = -40;

const videoStyles = StyleSheet.create({
    primaryContainer: {
        marginTop: multimediaMarginVertical * 2,
        marginBottom: multimediaMarginVertical / 3,
    },
    video: {
        minHeight: HEIGHT / 2,
        width: WIDTH,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
        backgroundColor: multimediaBG,
    },
    overlay: {
        backgroundColor: `rgba(0,0,0,0.4)`,

        position: 'absolute',

        top: 0,
        bottom: 0,
        left: 0,
        right: 0,

        zIndex: 1,
    },
    pauseIcon: {

        position: 'absolute',
        top: (HEIGHT / 2) / 2,
        flex: 1,
        zIndex: 1,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center",
    },
});