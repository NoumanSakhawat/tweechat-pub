import React, { Component } from 'react';
import { Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native';
import Statusbar from '../components/Statusbar';
import Text from '../components/Text';
import VectorIcon from '../components/VectorIcon';
import AppStyles from '../constants/AppStyles';
import colors from '../constants/colors';
import { emptyValidate } from '../helper/HelperFunctions';
//END OF IMPORT's

interface componentInterface {

    left?: boolean;
    leftIcon?: any;
    leftIconType?: 'Ionicons' | 'AntDesign' | 'Entypo' | 'EvilIcons' | 'Feather' | 'FontAwesome' | 'Fontisto' | 'MaterialCommunityIcons' | 'MaterialIcons' | "Foundation" | "SimpleLineIcons";
    leftPress?: () => void;
    leftCustom?: React.ReactElement;

    back?: boolean;
    backPress?: () => void;

    menu?: boolean;
    menuPress?: () => void;

    title?: any;

    right?: boolean;
    rightIcon?: any;
    rightIconType?: 'Ionicons' | 'AntDesign' | 'Entypo' | 'EvilIcons' | 'Feather' | 'FontAwesome' | 'Fontisto' | 'MaterialCommunityIcons' | 'MaterialIcons' | "Foundation" | "SimpleLineIcons";
    rightPress?: () => void;
    rightCustom?: React.ReactElement;

    bottomBorder?: boolean;

    customHeader?: React.ReactElement;

    shadow?: boolean;
}//end of INTERFACE 

export default class TweeHeader extends Component<componentInterface, any> {

    constructor(props: componentInterface) {
        super(props);
        this.state = {
        };
    }//end of CONSTRUCTOR

    public static defaultProps = {
        back: false,
        menu: false,

        backPress() { },
        menuPress() { },

        title: 'TweeChat',

        right: false,
        rightIcon: "arrow-forward",
        rightIconType: "Ionicons",

        rightPress() { },
        rightCustom: null,

        left: false,
        leftIcon: "arrow-forward",
        leftIconType: "Ionicons",

        leftPress() { },
        leftCustom: null,

        bottomBorder: false,

        customHeader: null,

        shadow: false,
    };//end of DEFAULT PROPS DECLARATION

    render = () => {
        const { back, backPress, menu, menuPress, title, right, rightIcon, rightIconType, rightPress, rightCustom,
            left, leftCustom, leftIcon, leftIconType, leftPress, bottomBorder, customHeader,
            shadow
        } = this.props;
        return (
            <View style={shadow && {
                overflow: 'hidden', paddingBottom: 5
            }}>
                <Statusbar />
                <View style={[styles.containerStyle, {
                    ...shadow && {
                        shadowColor: '#000',
                        shadowOffset: { width: 1, height: 1 },
                        shadowOpacity: 0.4,
                        shadowRadius: 3,
                        elevation: 5,
                    },
                }]}>
                    {emptyValidate(customHeader) ?
                        //@ts-ignore
                        customHeader() :
                        <>
                            {/* ******************** BACK & MENU Start ******************** */}
                            <View style={styles.leftContainer}>
                                {left ?
                                    emptyValidate(leftCustom) ?
                                        // @ts-ignore
                                        leftCustom()
                                        :
                                        left &&
                                        <TouchableOpacity onPress={leftPress}>
                                            <VectorIcon
                                                onPress={leftPress}
                                                name={leftIcon}
                                                iconType={leftIconType}
                                                size={30}
                                                color={colors.primary}
                                                style={styles.leftIcon}
                                            />
                                        </TouchableOpacity>

                                    :
                                    back || menu &&
                                    <TouchableOpacity onPress={back ? backPress : menu ? menuPress : backPress}>
                                        <VectorIcon
                                            onPress={back ? backPress : menu ? menuPress : backPress}
                                            name={back ? "arrow-back" : menu ? "menu" : "arrow-back"}
                                            size={30}
                                            color={colors.primary}
                                            style={styles.leftIcon}
                                        />
                                    </TouchableOpacity>
                                }
                            </View>

                            {/* ******************** BACK & MENU End ******************** */}

                            {/* ******************** BODY Start ******************** */}
                            <View style={styles.bodyContainer}>
                                <Text style={styles.text}>{title}</Text>
                            </View>

                            {/* ******************** BODY End ******************** */}

                            {/* ******************** RIGHT Start ******************** */}
                            <View style={styles.rightContainer}>
                                {emptyValidate(rightCustom) ?
                                    // @ts-ignore
                                    rightCustom()
                                    :
                                    right &&
                                    <TouchableOpacity onPress={rightPress}>
                                        <VectorIcon
                                            onPress={rightPress}
                                            name={rightIcon}
                                            iconType={rightIconType}
                                            size={30}
                                            color={colors.white}
                                            style={styles.rightIcon}
                                        />
                                    </TouchableOpacity>
                                }
                            </View>

                            {/* ******************** RIGHT End ******************** */}

                        </>
                    }
                </View>


                {bottomBorder &&
                    <View style={styles.border} />
                }
            </View>
        )
    } // end of Function Render

} //end of class TweeHeader

const WIDTH = Dimensions.get('screen').width;

const styles = StyleSheet.create({
    border: {
        backgroundColor: `rgba(0,0,0,0.1)`,
        height: 1,
    },
    containerStyle: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: colors.header,
        minHeight: 50,
    },
    leftContainer: {
        width: 60,
        justifyContent: "center",
    },
    leftIcon: {
        paddingLeft: AppStyles.HORIZONTAL / 2,
        // paddingLeft: 12,
    },

    bodyContainer: {
        width: WIDTH - 120,
        alignItems: "center",
        justifyContent: "center",
    },
    text: {
        color: colors.white,
        fontSize: 16,
        fontWeight: "600",
    },
    rightContainer: {
        width: 60,
        alignItems: "flex-end",
        justifyContent: "center",
    },
    rightIcon: {
        paddingRight: 12,
    },
}); //end of StyleSheet STYLES
