import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Text from '../components/Text';
import colors from '../constants/colors';
import LocalAssets from '../constants/LocalAssets';
import AppEnum from '../constants/AppEnum';
//END OF IMPORT's

enum THOUGHT {
    "isMyLiked" = "isMyLiked",
    "like" = "like",
    "comment" = "comment",
    "isMyRetweet" = "isMyRetweet",
    "retweet" = "retweet",
}

interface componentInterface {
    data: any,
}//end of INTERFACE 

const thoughtColor = {
    unselected: `#87888B`,
    like: `#DC3131`
}


export default class Thought extends Component<componentInterface, any> {

    constructor(props: componentInterface) {
        super(props);
        this.state = {
        };
    }//end of CONSTRUCTOR

    public static defaultProps = {

    };//end of DEFAULT PROPS DECLARATION

    render() {
        const thought: any = this.props.data;
        return (
            <View style={thoughtStyles.primaryContainer}>

                {/* ******************** LIKE Start ******************** */}
                <TouchableOpacity style={thoughtStyles.container}>
                    <FastImage
                        source={thought.isMyLiked ? LocalAssets.ICON.redHeart : LocalAssets.ICON.heart}
                        style={thoughtStyles.image}
                        tintColor={thought.isMyLiked ? thoughtColor.like : thoughtColor.unselected}
                        resizeMode={FastImage.resizeMode.contain} />

                    <Text style={{
                        ...thoughtStyles.text,
                        color: thought.isMyLiked ? thoughtColor.like : thoughtColor.unselected,
                    }}>{thought.like}</Text>
                </TouchableOpacity>

                {/* ******************** LIKE End ******************** */}

                {/* ******************** RETWEET Start ******************** */}
                <TouchableOpacity style={thoughtStyles.container2}>
                    <FastImage
                        source={LocalAssets.ICON.retweet}
                        style={thoughtStyles.image}
                        tintColor={thoughtColor.unselected}
                        resizeMode={FastImage.resizeMode.contain} />
                    <Text style={{
                        ...thoughtStyles.text,
                        color: thoughtColor.unselected,
                    }}>{thought.retweet}</Text>
                </TouchableOpacity>

                {/* ******************** RETWEET End ******************** */}

                {/* ******************** COMMENT Start ******************** */}
                <TouchableOpacity style={thoughtStyles.container2}>
                    <FastImage
                        source={LocalAssets.ICON.messageSquare}
                        style={thoughtStyles.image}
                        tintColor={thoughtColor.unselected}
                        resizeMode={FastImage.resizeMode.contain} />
                    <Text style={{
                        ...thoughtStyles.text,
                        color: thoughtColor.unselected,
                    }}>{thought.comment}</Text>
                </TouchableOpacity>


                {/* ******************** COMMENT End ******************** */}

                {/* ******************** SHARE Start ******************** */}
                <TouchableOpacity style={thoughtStyles.container2}>
                    <FastImage
                        source={LocalAssets.ICON.share}
                        style={thoughtStyles.image}
                        tintColor={thoughtColor.unselected}
                        resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>

                {/* ******************** SHARE End ******************** */}

            </View>
        )
    } // end of Function Render

} //end of class Thought


const thoughtStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
        flexDirection: "row",
        alignItems: "center",
        marginTop: 16,
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',

    },
    container2: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 22,
    },
    text: {
        fontSize: 14,
        marginLeft: 7,
    },
    image: {
        width: 22,
        height: 22,
    },
    retweetIcon: {
        transform: [{ rotateY: '180deg' }]
    },

});//end of thoughtStyles
