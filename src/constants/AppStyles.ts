import { Dimensions } from "react-native";
import colors from "./colors";

export default {
    HORIZONTAL: 24,

    border: {
        backgroundColor: `rgba(0,0,0,0.1)`,
        height: 1,
    },
    accessory: {
        width: Dimensions.get('window').width,
        height: 48,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: '#F8F8F8',
        paddingHorizontal: 8
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    menuOptionsStyles: {
        optionsContainer: {
            backgroundColor: 'transparent',
            paddingRight: 40,
            paddingTop: 10,

        },
        optionsWrapper: {
            backgroundColor: colors.white,
        },
        optionWrapper: {
            backgroundColor: colors.white,
            margin: 5,
        },
        optionTouchable: {
            underlayColor: `rgba(0,0,0,0.04)`,
            activeOpacity: 70,
        },
        optionText: {
            color: colors.black,
            fontSize: 12,
        },
    },//end of menuOptionsStyles

}//end of EXPORT DEFAULT