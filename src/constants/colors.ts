const PRIMARY = "#3689FF";

export default {
    primary: PRIMARY,
    background: "#FFFFFF",

    statusbar: "#FFFFFF",
    header: "#FFFFFF",

    white: "#FFFFFF",
    black: "#000",
}//end of EXPORT DEFAULT