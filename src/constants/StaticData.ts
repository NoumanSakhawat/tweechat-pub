import Faker from "../helper/Faker"
import loremipsum from "../helper/Faker/loremipsum";
import AppEnum from "./AppEnum";
import LocalAssets from "./LocalAssets";

export default {
    stories(maxlen = 20) {
        const len = Faker.getInt(5, maxlen);
        const data: any = [];
        for (let i = 0; i < len; i++) {
            data.push({
                id: i,
                name: Faker.name(),
                profile: { uri: LocalAssets.randomProfilePicture(), },
                isMyStory: false,
            })
        }//end of LOOP
        return data;
    },//end of stories

    post(maxlen = 20) {
        const len = Faker.getInt(5, maxlen);
        const data: any = [];
        for (let i = 0; i < len; i++) {
            const multimediaArr = [];

            const mulLen = Math.random() < 0.9 ? Faker.getInt(0, 10) : 0;
            for (let j = 0; j < mulLen; j++) {
                const isVideo = Math.random() < 0.3;
                const duration = isVideo ? Faker.getDuration() : '';
                multimediaArr.push({
                    id: j,
                    type: isVideo ? AppEnum.MULTIMEDIA.video : AppEnum.MULTIMEDIA.image,
                    path: {
                        uri: isVideo ? LocalAssets.randomVideo() : LocalAssets.randomImage(true)
                    },
                    ...isVideo && {
                        thumbnail: { uri: LocalAssets.randomImage(true) },
                        duration: duration,
                        views: Faker.numberWithComma()
                    },
                    isMyLiked: Faker.randomBool(),
                    like: Faker.number(0, 100),
                    comment: Faker.number(0, 100),
                    isMyRetweet: Faker.randomBool(),
                    retweet: Faker.number(0, 100),
                })
            }//end of LOOP MUL

            const commentCount = Faker.number(2, 6);
            const commentArr = [];
            for (let i = 0; i < commentCount; i++) {
                const commentChildArr = [];
                const childCount = Faker.getInt(1, 10);
                const nestedChildCount = Faker.getInt(1, 10);

                if (Faker.randomBool(0.3)) {
                    for (let j = 0; j < childCount; j++) {
                        const nestedChildArr = [];
                        if (Faker.randomBool(0.3)) {
                            for (let k = 0; k < nestedChildCount; k++) {
                                nestedChildArr.push({
                                    id: k,
                                    user: {
                                        name: Faker.name(),
                                        profile: { uri: LocalAssets.randomProfilePicture(), },
                                        username: Faker.username(),
                                    },
                                    createdAt: new Date().getTime(),
                                    updatedAt: Faker.randomTimestamp(),

                                    body: loremipsum.paragraph(),
                                    likeCount: Faker.getInt(),
                                    isLiked: Faker.randomBool(),
                                    replyCount: Faker.getInt(),
                                    timeago: Faker.randomTimestamp(),
                                })
                            }//end of K
                        }//end of CREATING NESTED CHILD

                        commentChildArr.push({
                            id: i,
                            user: {
                                name: Faker.name(),
                                profile: { uri: LocalAssets.randomProfilePicture(), },
                                username: Faker.username(),
                            },
                            createdAt: new Date().getTime(),
                            updatedAt: Faker.randomTimestamp(),

                            body: loremipsum.paragraph(),
                            likeCount: Faker.getInt(),
                            isLiked: Faker.randomBool(),
                            replyCount: Faker.getInt(),
                            timeago: Faker.randomTimestamp(),
                            ...nestedChildArr.length > 0 && { childern: nestedChildArr }
                        })

                    }//end of J
                }

                commentArr.push({
                    id: i,
                    user: {
                        name: Faker.name(),
                        profile: { uri: LocalAssets.randomProfilePicture(), },
                        username: Faker.username(),
                    },
                    createdAt: new Date().getTime(),
                    updatedAt: Faker.randomTimestamp(),

                    body: loremipsum.paragraph(),
                    likeCount: Faker.getInt(),
                    isLiked: Faker.randomBool(),
                    replyCount: Faker.getInt(),
                    timeago: Faker.randomTimestamp(),

                    ...commentChildArr.length > 0 && { childern: commentChildArr }

                })
            }//end of LOOP COMMENT

            const likeCount = Faker.number(0, 10);
            const isMyLiked = Faker.randomBool();
            const reactionArr = [];
            for (let i = 0; i < likeCount * 5; i++) {
                reactionArr.push({
                    id: i,
                    name: Faker.name(),
                    profile: { uri: LocalAssets.randomProfilePicture(), },
                    username: Faker.username(),
                })
            }//end of LOOP REACTION 
            if (isMyLiked) {
                reactionArr.push({
                    id: reactionArr.length,
                    name: Faker.name(),
                    profile: { uri: LocalAssets.randomProfilePicture(), },
                    username: Faker.username(),
                })
            }

            data.push({
                id: i,
                user: {
                    name: Faker.name(),
                    profile: { uri: LocalAssets.randomProfilePicture(), },
                    username: Faker.username(),
                },
                isFollow: Faker.randomBool(),
                content: loremipsum.paragraph(),
                multimedia: multimediaArr,
                thought: {
                    isMyLiked: isMyLiked,
                    like: likeCount,
                    comment: commentCount,
                    isMyRetweet: Faker.randomBool(),
                    retweet: Faker.number(0, 100),
                    commentDetail: commentArr,
                    reactions: reactionArr,
                },
                createdAt: new Date().getTime(),
                updatedAt: Faker.randomTimestamp(),
            })
        }//end of LOOP
        return data;
    },//end of post

    addPostBottomButton() {
        return [{
            id: 1,
            image: LocalAssets.ICON.gallery,
            name: 'Gallery',
            isRight: false,
        }, {
            id: 2,
            image: LocalAssets.ICON.gif,
            name: 'GIF',
            isRight: false,
        }, {
            id: 3,
            image: LocalAssets.ICON.poll,
            name: 'Poll',
            isRight: false,
        }, {
            id: 4,
            image: LocalAssets.ICON.location,
            imageSelected: LocalAssets.ICON.locationSelected,
            name: 'Location',
            isRight: false,
        }, {
            id: 5,
            image: LocalAssets.ICON.wordCountProgress,
            name: 'Word Count',
            isRight: true,
        }, {
            id: 6,
            image: LocalAssets.ICON.addNewThread,
            name: 'Add new thread',
            isRight: true,
        }]
    },//end of addPostBottomButton

    settingsData() {
        return [{
            id: 1,
            name: 'My Account',
        }, {
            id: 2,
            name: 'Privacy and safety',
        }, {
            id: 3,
            name: 'Notifications',
        }, {
            id: 4,
            name: 'About Tweetchat',
        },]
    },

    userProfilePicture: LocalAssets.randomProfilePicture(),//end of userProfilePicture

    gifCategories() {
        return [{
            id: 1,
            title: 'Agree',
            thumbnail: `https://media.giphy.com/media/l0MYy7QpDDVGVfAAw/giphy.gif`,
        }, {
            id: 2,
            title: 'Applause',
            thumbnail: `https://media.giphy.com/media/5xaOcLDE64VMF4LqqrK/giphy.gif`,
        }, {
            id: 3,
            title: 'Awww',
            thumbnail: `https://media.giphy.com/media/dILZwwhulFUn6/giphy.gif`,
        }, {
            id: 4,
            title: 'Dance',
            thumbnail: `https://media.giphy.com/media/l4Ep3mmmj7Bw3adWw/giphy.gif`,
        }, {
            id: 5,
            title: 'Deal with it',
            thumbnail: `https://media.giphy.com/media/oFI7FttD0iC8V2Iqmy/giphy.gif`,
        }, {
            id: 6,
            title: 'Do not want',
            thumbnail: `https://media.giphy.com/media/3xz2BLBOt13X9AgjEA/giphy.gif`,
        }, {
            id: 7,
            title: 'Eww',
            thumbnail: `https://media.giphy.com/media/10FHR5A4cXqVrO/giphy.gif`,
        }, {
            id: 8,
            title: 'Eye roll',
            thumbnail: `https://media.giphy.com/media/uGTjfGipN0amk/giphy.gif`,
        }, {
            id: 9,
            title: 'Facepalm',
            thumbnail: `https://media.giphy.com/media/WrP4rFrWxu4IE/giphy.gif`,
        }, {
            id: 10,
            title: 'First bump',
            thumbnail: `https://media.giphy.com/media/0nn3OVicjFr6jic3Qc/giphy.gif`,
        }, {
            id: 11,
            title: 'Good luck',
            thumbnail: `https://media.giphy.com/media/l3UcjBJUov1gCRGbS/giphy.gif`,
        }, {
            id: 12,
            title: 'Happy dance',
            thumbnail: `https://media.giphy.com/media/l0amJzVHIAfl7jMDos/giphy.gif`,
        }, {
            id: 13,
            title: 'Hearts',
            thumbnail: `https://media.giphy.com/media/7W1rgKAxlDe3m/giphy.gif`,
        }, {
            id: 14,
            title: 'High five',
            thumbnail: `https://media.giphy.com/media/r2BtghAUTmpP2/giphy.gif`,
        }, {
            id: 15,
            title: 'Hug',
            thumbnail: `https://media.giphy.com/media/qLkmn46mxPgY0/giphy.gif`,
        }, {
            id: 16,
            title: 'IDK',
            thumbnail: `https://media.giphy.com/media/bPTXcJiIzzWz6/giphy.gif`,
        }, {
            id: 17,
            title: 'Kiss',
            thumbnail: `https://media.giphy.com/media/l3vRaluvzEsJVApMI/giphy.gif`,
        }, {
            id: 18,
            title: 'Mic drop',
            thumbnail: `https://media.giphy.com/media/iBifnOQ3ZodBC/giphy.gif`,
        }, {
            id: 19,
            title: 'No',
            thumbnail: `https://media.giphy.com/media/fIkT0LdGUc4GushZ2Q/giphy.gif`,
        }, {
            id: 20,
            title: 'OMG',
            thumbnail: `https://media.giphy.com/media/OYJ2kbvdTPW6I/giphy.gif`,
        }]
    },//end of gifCategories

    attachment() {
        return [{
            id: 1,
            title: 'Camera',
            image: LocalAssets.ICON.attachmentCamera,
            color: `#ff2d74`,
        }, {
            id: 2,
            title: 'Gallery',
            image: LocalAssets.ICON.attachmentGallery,
            color: `#c861fa`,
        }]
    },//end of attachment


    notifications() {
        const data = [];
        const len = Faker.getInt(5, 100);
        for (let i = 0; i < len; i++) {
            data.push({
                id: i,
                user: {
                    name: Faker.name(),
                    profile: { uri: LocalAssets.randomProfilePicture(), },
                    username: Faker.username(),
                },
                createdAt: new Date().getTime(),
                updatedAt: Faker.randomTimestamp(),

                timeago: Faker.randomTimestamp(),
                body: Faker.randomQuote().quote,
                isRead: Faker.randomBool(),
            })
        }
        return data;
    },//end of notifications

    messages() {
        const data = [];
        const len = Faker.getInt(5, 100);
        for (let i = 0; i < len; i++) {

            data.push({
                id: i,
                user: {
                    name: Faker.name(),
                    profile: { uri: LocalAssets.randomProfilePicture(), },
                    username: Faker.username(),
                },
                createdAt: new Date().getTime(),
                updatedAt: Faker.randomTimestamp(),

                timeago: Faker.randomTimestamp(),
                isRead: Faker.randomBool(),
                count: Faker.getInt(),

                lastMessage: Faker.randomQuote().quote,
                lastMessageByYou: Faker.randomBool(),

            })
        }
        return data;
    },//end of messages

    chat() {
        const data = [];
        const len = Faker.getInt(5, 100);
        const myUserID = Faker.guid();
        const name = Faker.name();
        const profile = { uri: LocalAssets.randomProfilePicture(), };
        const username = Faker.username();

        const otherUserID = Faker.guid();
        const otherName = Faker.name();
        const otherProfile = { uri: LocalAssets.randomProfilePicture(), };
        const otherUsername = Faker.username();

        for (let i = 0; i < len; i++) {
            const isMyText = Faker.randomBool();
            data.push({
                id: i,
                createdAt: new Date().getTime(),
                updatedAt: Faker.randomTimestamp(),
                _id: Faker.guid(),
                text: loremipsum.paragraph(),
                // ...Faker.randomBool() && {
                //     image: "https://picsum.photos/id/237/200/300",
                // },
                // ...Faker.randomBool() && {
                //     video: "https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4",
                // },
                user: {
                    _id: isMyText ? myUserID : otherUserID, //CURRENT USER ID MUST GO HERE
                    name: isMyText ? name : otherName,
                    avatar: isMyText ? profile.uri : otherProfile.uri,
                },

            })
        }

        const statusBool = Faker.randomBool();
        return {
            chat: data, user: {
                name,
                profile,
                username,

                status: statusBool ? "online" : "offline",
                statusText: statusBool ? "Active Now" : "Away",

                lastSeen: Faker.randomTimestamp(),

                myUserID
            },
        };
    },//end of chat

    userProfile() {

        const id = Faker.getInt(5, 100);
        const DOB = Faker.randomDOB();
        return {
            id: id,
            user: {
                name: Faker.name(),
                profile: { uri: LocalAssets.randomProfilePicture(), },
                cover: { uri: LocalAssets.randomImage(true), },
                username: Faker.username(),
            },
            bio: {
                description: loremipsum.paragraph(),
            },
            details: {
                dob: DOB.dob,
                date: DOB.date,
                month: DOB.month,
                year: DOB.year,
                age: DOB.age,
            },
            location: Faker.randomAddress(),


            createdAt: new Date().getTime(),
            updatedAt: Faker.randomTimestamp(),


            tweets: Faker.numberWithComma(),
            follower: Faker.getInt(),
            following: Faker.getInt(),


        };
    },//end of userProfile

}//end of EXPORT DEFAULT