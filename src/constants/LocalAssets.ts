import HelperFunctions from "../helper/HelperFunctions";

export default {
    randomProfilePicture() {
        return `https://randomuser.me/api/portraits/men/${HelperFunctions.randomInt(1, 75)}.jpg`;
    },//end of randomProfilePicture

    randomImage(HD: boolean = false) {
        if (HD) {
            return `https://picsum.photos/${HelperFunctions.randomInt(800, 900)}`;
        }
        return `https://picsum.photos/${HelperFunctions.randomInt(200, 300)}`;
    },//end of randomImage

    anyImage() {

        return `https://picsum.photos/${HelperFunctions.randomInt(10, 900)}`;
    },//end of randomImage

    randomVideo() {
        const arr = [
            `https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4`,
            `https://projects.funtash.net/news/public/videos/20210106171151_original_32.mp4`,
            // `https://scontent-lga3-1.xx.fbcdn.net/v/t66.36240-6/10000000_346255027105162_3517624523574675000_n.mp4?_nc_cat=110&ccb=1-3&_nc_sid=985c63&efg=eyJybHIiOjQ1MzYsInJsYSI6NDA5NiwidmVuY29kZV90YWciOiJvZXBfaGQifQ%3D%3D&_nc_ohc=89_g69t5HrUAX92lNH8&rl=4536&vabr=3024&_nc_ht=scontent-lga3-1.xx&edm=AGo2L-IEAAAA&oh=d336066c084e26b2173ae0a3688c1bda&oe=610FADA5`,
            // `https://scontent-vie1-1.xx.fbcdn.net/v/t66.36281-6/10000000_264978911693128_5376235692169278134_n.mp4?_nc_cat=1&ccb=1-3&_nc_sid=985c63&efg=eyJybHIiOjkyMSwicmxhIjo0MDk2LCJ2ZW5jb2RlX3RhZyI6Im9lcF9zZCJ9&_nc_ohc=3aXje2LtwyMAX_XV2WH&rl=921&vabr=512&_nc_ht=scontent-vie1-1.xx&edm=AGo2L-IEAAAA&oh=d44a8d2ccabbe1eee8461f80839048b3&oe=61105768`,
            // `https://scontent-vie1-1.xx.fbcdn.net/v/t66.36240-6/10000000_101828308846431_1062776598814778274_n.mp4?_nc_cat=103&ccb=1-3&_nc_sid=985c63&efg=eyJybHIiOjE1MDAsInJsYSI6NDA5NiwidmVuY29kZV90YWciOiJvZXBfaGQifQ%3D%3D&_nc_ohc=S_-amxxBodYAX9BhzKy&_nc_oc=AQmIrsPpNdX95HNLay83urlVIiTHsZdYo-NTxtPy30LiLeYTHNn-5Y_K_KnKNdERHHY&rl=1500&vabr=431&_nc_ht=scontent-vie1-1.xx&edm=AGo2L-IEAAAA&oh=a430b22f7d72bd6ce85258fe550367e1&oe=610FF8BC`,
            // `https://scontent-vie1-1.xx.fbcdn.net/v/t66.36240-6/10000000_535712537667252_9102435686299052575_n.mp4?_nc_cat=110&ccb=1-3&_nc_sid=985c63&efg=eyJybHIiOjE1MDAsInJsYSI6NDA5NiwidmVuY29kZV90YWciOiJvZXBfaGQifQ%3D%3D&_nc_ohc=IRAYYoIlky4AX_Wh_-X&rl=1500&vabr=613&_nc_ht=scontent-vie1-1.xx&edm=AGo2L-IEAAAA&oh=aa854a1af9e11e2ab37c13753dc9a315&oe=61101CE5`,


        ];
        const min = 0;
        const max = arr.length - 1;

        const index = Math.floor(Math.random() * (max - min + 1)) + min;
        return arr[index];
    },//end of randomVideo

    ICON: {
        "heart": require("../../assets/icon/heart.png"),
        "redHeart": require("../../assets/icon/red-heart.png"),
        "messageSquare": require("../../assets/icon/message-square.png"),
        "retweet": require("../../assets/icon/retweet.png"),
        "share": require("../../assets/icon/share.png"),
        "videoPlay": require("../../assets/icon/videoPlay.png"),

        "gallery": require("../../assets/icon/gallery.png"),
        "poll": require("../../assets/icon/poll.png"),
        "gif": require("../../assets/icon/gif.png"),
        "location": require("../../assets/icon/location.png"),
        "locationSelected": require("../../assets/icon/locationSelected.png"),
        "wordCountProgress": require("../../assets/icon/wordCountProgress.png"),
        "addNewThread": require("../../assets/icon/addNewThread.png"),

        "videoPlayBack": require("../../assets/icon/videoPlayBack.png"),
        "videoPlayOption": require("../../assets/icon/videoPlayOption.png"),
        "seekbarCircle": require("../../assets/icon/seekbarCircle.png"),
        "reactionMore": require("../../assets/icon/reactionMore.png"),
        "onboarding": require("../../assets/icon/onboarding.png"),

        "setupProfile": require("../../assets/icon/setupProfile.png"),
        "forgotPassword": require("../../assets/icon/forgotPassword.png"),

        "attachment": require("../../assets/icon/attachment.png"),
        "attachmentCamera": require("../../assets/icon/attachmentCamera.png"),
        "attachmentGallery": require("../../assets/icon/attachmentGallery.png"),

        "send": require("../../assets/icon/send.png"),


    },

    GIF: {
        "buffering": require("../../assets/gif/buffering.gif"),
    },

    BottomTab: {
        "home": require("../../assets/icon/BottomTab/home.png"),
        "search": require("../../assets/icon/BottomTab/search.png"),
        "notification": require("../../assets/icon/BottomTab/notification.png"),
        "message": require("../../assets/icon/BottomTab/message.png"),
    },

    Drawer: {
        "logo": require("../../assets/icon/Drawer/logo.png"),

        "explore": require("../../assets/icon/Drawer/explore.png"),
        "message": require("../../assets/icon/Drawer/message.png"),
        "notification": require("../../assets/icon/Drawer/notification.png"),
        "profile": require("../../assets/icon/Drawer/profile.png"),
        "setting": require("../../assets/icon/Drawer/setting.png"),
        "logout": require("../../assets/icon/Drawer/logout.png"),
    },

    PostOption: {
        "mute": require("../../assets/icon/PostOptions/mute.png"),
        "follow": require("../../assets/icon/PostOptions/follow.png"),
        "block": require("../../assets/icon/PostOptions/block.png"),
        "report": require("../../assets/icon/PostOptions/report.png"),
    },

    UserProfile: {
        "dob": require("../../assets/icon/UserProfile/dob.png"),
        "place": require("../../assets/icon/UserProfile/place.png"),
    },

}//end of EXPORT DEFAULT