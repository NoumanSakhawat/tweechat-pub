export default {
    POLL_MAX_LENGTH: 4,

    POLL_TEXT_MAX_LENGTH: 21,

    ADD_POST_TEXT_LENGTH: 280,

    SIGN_UP_LENGTH: {
        FULL_NAME: 30,
        EMAIL: 30,
        PASSWORD: 50,
        RE_PASSWORD: 50,
    },

    SIGN_IN_LENGTH: {
        EMAIL: 30,
        PASSWORD: 50,
    },

    FORGET_PASSWORD_LENGTH: {
        EMAIL: 30,
    },

}//end of EXPORT DEFAULT