
enum MULTIMEDIA {
    "video" = "video",
    "image" = "image",
}

enum THOUGHT {
    "isMyLiked" = "isMyLiked",
    "like" = "like",
    "comment" = "comment",
    "isMyRetweet" = "isMyRetweet",
    "retweet" = "retweet",
}

enum ADD_POST_TYPE {
    "image" = "image",
    "gif" = "gif",
    "poll" = "poll",
    "location" = "location"
}

export default {
    MULTIMEDIA,

    THOUGHT,

    ADD_POST_TYPE,
}//end of EXPORT DEFAULT