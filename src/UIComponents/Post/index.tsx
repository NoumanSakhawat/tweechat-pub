import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { FlatList } from 'react-native-gesture-handler';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import AppEnum from '../../constants/AppEnum';
import LocalAssets from '../../constants/LocalAssets';
import HelperFunctions, { emptyValidate } from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import ROUTES from '../../routes/ROUTES';
import PostText from '../../tweeComponents/PostText';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import Thought from '../../tweeComponents/Thought';
import { itemStyles, multimediaStyles, profileImageStyles, styles, thoughtStyles } from './styles';
import { Menu, MenuOptions, MenuOption, MenuTrigger, renderers } from 'react-native-popup-menu';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';
//END OF IMPORT's

const thoughtColor = {
    unselected: `#87888B`,
    like: `#DC3131`
}

const index = ({ item, index, navigation, viewAllCommentPress, onPostPress, onMediaItemPress, showTopBorder = true, showViewAllCommentText = true,
    showOptionIcon = true, onUserProfilePress = () => { }, userProfilePressDisable = false }: any) => {


    const username = item.user.username;

    return (
        <View style={styles.primaryContainer}>

            <View>
                {showTopBorder && <View style={styles.border} />}
                <View style={itemStyles.primaryContainer}>

                    {/* ******************** USER DETAIL Start ******************** */}
                    <View style={itemStyles.userDetailPC}>
                        {/* {_renderProfileImage(item.user.profile)} */}
                        <TouchableOpacity onPress={() => {
                            onUserProfilePress(item)
                        }}
                            disabled={userProfilePressDisable}>
                            <ProfilePicture
                                hasBorder
                                source={item.user.profile} />
                        </TouchableOpacity>

                        <View style={itemStyles.nameUsernameContainer}>

                            <View style={{ flexDirection: "row" }}>

                                <TouchableOpacity onPress={() => {
                                    onUserProfilePress(item)
                                }}
                                    disabled={userProfilePressDisable}>
                                    <Text style={itemStyles.name}>{item.user.name}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={itemStyles.followContainer}>
                                    <Text style={item.isFollow ? itemStyles.following : itemStyles.follow}>
                                        <Text style={itemStyles.followDot}>•</Text>
                                        {item.isFollow ? IMLocalized(`Following`) : IMLocalized(`Follow`)}
                                    </Text>
                                </TouchableOpacity>
                            </View>

                            <TouchableOpacity onPress={() => {
                                onUserProfilePress(item)
                            }}
                                disabled={userProfilePressDisable}>
                                <Text style={itemStyles.username}>{`@${item.user.username}`}</Text>
                            </TouchableOpacity>

                        </View>

                        {showOptionIcon &&
                            // <TouchableOpacity style={itemStyles.optionContainer}
                            //     onPress={() => {  }}>
                            //     <VectorIcon
                            //         name={"dots-vertical"}
                            //         iconType={"MaterialCommunityIcons"}
                            //         color={"#0F1117"}
                            //         size={22}
                            //     />
                            // </TouchableOpacity>


                            <Menu style={itemStyles.optionContainer}>
                                <MenuTrigger
                                //@ts-ignore
                                // style={itemStyles.optionContainer}
                                >

                                    <VectorIcon
                                        name={"dots-vertical"}
                                        iconType={"MaterialCommunityIcons"}
                                        color={"#0F1117"}
                                        size={22}
                                    />
                                </MenuTrigger>
                                <MenuOptions
                                    customStyles={optionsStyles}
                                >
                                    <MenuOption style={{ flexDirection: "row", alignItems: "center" }}>
                                        <FastImage
                                            source={LocalAssets.PostOption.follow}
                                            style={itemStyles.optionImage}
                                            resizeMode={FastImage.resizeMode.contain} />
                                        <Text style={itemStyles.optionText}>{`${IMLocalized(`Follow`)} @${username}`}</Text>
                                    </MenuOption>


                                    <MenuOption style={{ flexDirection: "row", alignItems: "center" }}>
                                        <FastImage
                                            source={LocalAssets.PostOption.mute}
                                            style={itemStyles.optionImage}
                                            resizeMode={FastImage.resizeMode.contain} />
                                        <Text style={itemStyles.optionText}>{`${IMLocalized(`Mute`)} @${username}`}</Text>
                                    </MenuOption>


                                    <MenuOption style={{ flexDirection: "row", alignItems: "center" }}>
                                        <FastImage
                                            source={LocalAssets.PostOption.block}
                                            style={itemStyles.optionImage}
                                            resizeMode={FastImage.resizeMode.contain} />
                                        <Text style={itemStyles.optionText}>{`${IMLocalized(`Block`)} @${username}`}</Text>
                                    </MenuOption>


                                    <MenuOption style={{ flexDirection: "row", alignItems: "center" }}>
                                        <FastImage
                                            source={LocalAssets.PostOption.report}
                                            style={itemStyles.optionImage}
                                            resizeMode={FastImage.resizeMode.contain} />
                                        <Text style={itemStyles.optionText}>{`${IMLocalized(`Report Tweet`)}`}</Text>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>

                        }

                    </View>

                    {/* ******************** USER DETAIL End ******************** */}

                    <TouchableOpacity
                        onPress={() => { onPostPress(item, index) }}
                        onPressIn={() => { onPostPress(item, index) }}
                        onPressOut={() => { onPostPress(item, index) }}
                        activeOpacity={1}

                    >

                        {/* ******************** POST Start ******************** */}
                        {emptyValidate(item.content) &&
                            <TouchableOpacity
                                // onPress={onPostPress}
                                onPress={() => { onPostPress(item, index) }}
                                onPressIn={() => { onPostPress(item, index) }}
                                onPressOut={() => { onPostPress(item, index) }}
                                activeOpacity={1}
                            >
                                {/* @ts-ignore */}
                                <PostText style={itemStyles.postText}
                                >{`${item.content}`}</PostText>
                            </TouchableOpacity>
                        }

                        {/* ******************** POST End ******************** */}

                        {_renderMultimedia(item.multimedia, navigation, onPostPress, onMediaItemPress)}

                        {/* {_renderThoughts(item.thought)} */}
                        <Thought data={item.thought} />

                        {showViewAllCommentText &&
                            <View style={styles.viewAllPrimaryContainer}>
                                <TouchableOpacity onPress={viewAllCommentPress}>
                                    <Text style={styles.viewAllText}>{IMLocalized(`View all number Comments`).replace('number', item.thought.comment)}</Text>
                                </TouchableOpacity>

                                <View style={styles.timeContainer}>

                                    <Text style={styles.time}>{HelperFunctions.timeSince(item.updatedAt)}</Text>
                                </View>

                            </View>
                        }


                    </TouchableOpacity>

                </View>
                <View style={itemStyles.footer} />
            </View>


        </View>
    )
}


const _renderMultimedia = (data: any, navigation: any, onPostPress: any, onMediaItemPress: any) => {
    const isMultiple = data.length > 1 ? true : false;
    const singleImagePath = data.length > 1 ? data[0].path : '';

    const newData = data.slice(0, 4);

    return (
        <View style={multimediaStyles.primaryContainer}>
            {isMultiple ?
                <FlatList
                    data={newData}
                    numColumns={2}
                    contentContainerStyle={multimediaStyles.contentContainerStyle}
                    nestedScrollEnabled={true}
                    scrollEnabled={false}
                    renderItem={({ item, index }) => {

                        return (
                            <TouchableOpacity
                                onPress={() => {
                                    // console.log('item.type', item.type);
                                    onMediaItemPress(isMultiple, index)
                                    // if (isMultiple) {
                                    //     onPostPress();
                                    //     return;
                                    // }
                                    // if (item.type === AppEnum.MULTIMEDIA.video) {
                                    //     navigation.navigate(ROUTES.VideoPlay); 
                                    // }
                                }}>

                                <FastImage
                                    source={{
                                        ...item.type === AppEnum.MULTIMEDIA.video ? item.thumbnail : item.path,
                                        priority: FastImage.priority.high,
                                    }}
                                    style={{
                                        ...(newData.length === 3 && index === newData.length - 1) ? multimediaStyles.thirdImage : multimediaStyles.image
                                    }}
                                    resizeMode={FastImage.resizeMode.cover} />

                                {item.type === AppEnum.MULTIMEDIA.video &&
                                    <>
                                        <View style={multimediaStyles.videoImageContainer}>
                                            <FastImage
                                                source={LocalAssets.ICON.videoPlay}
                                                style={multimediaStyles.videoImage}
                                                resizeMode={FastImage.resizeMode.cover} />
                                        </View>

                                        <View style={multimediaStyles.timeContainer}>
                                            <Text style={multimediaStyles.time}>{item.duration}</Text>
                                        </View>

                                    </>
                                }


                                {(index === newData.length - 1 && data.length > 3) &&
                                    <>
                                        <View style={multimediaStyles.overlay} />
                                        <Text style={multimediaStyles.overlayText}>{`+${data.length}`}</Text>
                                    </>
                                }


                            </TouchableOpacity>
                        )
                    }} />
                :
                emptyValidate(singleImagePath) &&
                <TouchableOpacity>
                    <FastImage
                        source={{
                            ...singleImagePath,
                            priority: FastImage.priority.high,
                        }}

                        style={multimediaStyles.singleImage}
                        resizeMode={FastImage.resizeMode.cover} />
                </TouchableOpacity>

            }
        </View>
    )

}//end of _renderMultimedia

const _renderProfileImage = (path: any) => {
    return (
        <View style={profileImageStyles.container}>
            <FastImage
                style={profileImageStyles.image}
                source={{
                    ...path,
                    priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.contain}
            />
        </View>
    )
}//end of _renderProfileImage

export default index;


const optionsStyles = {
    optionsContainer: {
        backgroundColor: 'transparent',
        paddingRight: 20,
        paddingTop: 10,
        minWidth: 230,
    },
    optionsWrapper: {
        backgroundColor: colors.white,
    },
    optionWrapper: {
        backgroundColor: colors.white,
        margin: 5,
    },
    optionTouchable: {
        underlayColor: `rgba(0,0,0,0.04)`,
        activeOpacity: 70,
    },
    optionText: {
        color: colors.black,
        fontSize: 12,
    },
};
