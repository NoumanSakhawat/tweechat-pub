import { StyleSheet, Dimensions } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const classColor = {
    name: "#0F1117",
    username: "#606268",
    followDot: "#606268",
    followingText: "#0F1117",
}

export const styles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
    },
    contentContainerStyle: {

    },
    border: {
        ...AppStyles.border,
        marginBottom: 20
    },

    viewAllPrimaryContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 10,
    },
    viewAllText: {
        fontSize: 12,
        color: "#87888B",
    },
    timeContainer: {
        flex: 1,
        alignSelf: "flex-end",
        alignItems: "flex-end",
        justifyContent: "flex-end",
    },
    time: {
        fontSize: 12,
        color: "#87888B",
    },
    actionButton: {
        bottom: -20
    },
});//end of styles

export const itemStyles = StyleSheet.create({
    primaryContainer: {
        marginHorizontal: AppStyles.HORIZONTAL,
    },
    userDetailPC: {
        flexDirection: 'row',
        alignItems: "flex-start",
    },
    nameUsernameContainer: {
        marginLeft: 8,
    },
    name: {
        color: classColor.name,
        fontSize: 16,
        fontWeight: "500",
    },
    username: {
        color: classColor.username,
        fontSize: 12,
        marginTop: 2,
    },
    followContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginTop: -8,
    },
    followDot: {
        marginLeft: 8,
        color: classColor.followDot,
        fontSize: 20,
    },
    following: {
        color: classColor.followingText,
        fontSize: 14,
        marginLeft: 4,
    },
    follow: {
        color: colors.primary,
        fontSize: 14,
        marginLeft: 4,
    },
    postText: {
        marginVertical: 16,
    },

    footer: {
        marginBottom: 16,
    },
    optionContainer: {
        position: "absolute",
        right: 0,
        top: 0,
    },
    optionImage: {
        height: 22,
        width: 22,
    },
    optionText: {
        color: colors.black,
        fontSize: 12,
        marginLeft: 8,
    },
});//end of itemStyles

const IMAGE_SIZE = 32;
const BORDER_DIFF = 4;
export const profileImageStyles = StyleSheet.create({
    container: {
        width: IMAGE_SIZE,
        height: IMAGE_SIZE,
        borderRadius: IMAGE_SIZE,
        borderColor: colors.primary,
        borderWidth: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    image: {
        width: IMAGE_SIZE - BORDER_DIFF,
        height: IMAGE_SIZE - BORDER_DIFF,
        borderRadius: IMAGE_SIZE - BORDER_DIFF,
    },
});//end of profileImageStyles

const WIDTH = (Dimensions.get('window').width) - 48;

const SINGLE_IMAGE_WIDTH = WIDTH;
const SINGLE_IMAGE_HEIGHT = WIDTH / 2;

export const multimediaStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
    },
    contentContainerStyle: {
        // alignSelf: "center",
        // alignItems: "center",
        justifyContent: "space-between",

    },
    singleImage: {
        height: SINGLE_IMAGE_HEIGHT,
        width: SINGLE_IMAGE_WIDTH,
        borderRadius: 4,
    },
    thirdImage: {
        height: SINGLE_IMAGE_HEIGHT / 1.3,
        width: SINGLE_IMAGE_WIDTH,
        borderRadius: 4,
    },
    image: {
        height: SINGLE_IMAGE_HEIGHT / 1.3,
        width: (SINGLE_IMAGE_WIDTH) / 2,
        borderRadius: 4,
        marginRight: 4,
        marginBottom: 4,
    },
    overlay: {
        height: SINGLE_IMAGE_HEIGHT / 1.3,
        width: (SINGLE_IMAGE_WIDTH) / 2,
        borderRadius: 4,
        marginRight: 4,
        marginBottom: 4,
        position: "absolute",
        backgroundColor: `rgba(0,0,0,0.39)`,
    },
    overlayText: {
        position: "absolute",
        top: (SINGLE_IMAGE_HEIGHT / 1.3) / 2.5,
        alignItems: "center",
        alignSelf: "center",
        textAlign: "center",
        color: colors.white,
        fontSize: 32,
    },
    videoImageContainer: {
        position: "absolute",
        top: (SINGLE_IMAGE_HEIGHT / 1.3) / 3,
        alignSelf: "center",
    },
    videoImage: {
        height: 48,
        width: 48,
    },
    timeContainer: {
        position: "absolute",
        bottom: 8,
        left: 8,
        backgroundColor: `rgba(15, 17, 23, 0.6)`,
        paddingHorizontal: 6,
        paddingVertical: 3,
        borderRadius: 15,
    },
    time: {
        color: colors.white,
        fontSize: 12,
        textAlign: "center",
    },
});//end of multimediaStyles

export const thoughtStyles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
        flexDirection: "row",
        alignItems: "center",
        marginTop: 16,
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',

    },
    container2: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 22,
    },
    text: {
        fontSize: 14,
        marginLeft: 7,
    },
    image: {
        width: 22,
        height: 22,
    },
    retweetIcon: {
        transform: [{ rotateY: '180deg' }]
    },

});//end of thoughtStyles