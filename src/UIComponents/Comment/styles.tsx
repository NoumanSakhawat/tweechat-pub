import { StyleSheet } from 'react-native';
import colors from '../../constants/colors';

export const styles = StyleSheet.create({
    primaryContainer: {
        marginBottom: 12,
    },
    cardPrimaryContainer: {
        flexDirection: 'row',
    },

    container: {
        flex: 1,
        backgroundColor: "#EAECF0",
        borderRadius: 8,
        marginLeft: 12,
        paddingHorizontal: 12,
        paddingVertical: 8,
    },
    usernameContainer: {
        flexDirection: 'row',
        alignItems: "flex-start",
        justifyContent: "flex-start",

    },
    username: {
        color: "#221F2E",
        fontSize: 16,
        fontWeight: "500",
        textTransform: "capitalize",
        width: "85%",
    },
    optionIconContainer: {
        width: "15%",
        alignItems: "flex-end",
        paddingBottom: 10,
    },
    bodyText: {
        color: `rgba(34, 31, 46, 0.8)`,
        fontSize: 14,
        // marginTop: 5,
    },
    time: {
        color: `#898A8D`,
        fontSize: 12,
        marginTop: 5,
    },
    bottomContainer: {
        marginTop: 5,
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 52,
    },
    bottomText: {
        color: `#898A8D`,
        fontSize: 12,
    },
    bottomSeperator: {
        width: 1,
        minHeight: 13,
        backgroundColor: "#898A8D",
        marginHorizontal: 5,
    },
});//end of styles
