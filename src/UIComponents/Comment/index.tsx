import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View } from 'react-native';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import StaticData from '../../constants/StaticData';
import HelperFunctions, { emptyValidate, printText } from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import PostText from '../../tweeComponents/PostText';
import ProfilePicture from '../../tweeComponents/ProfilePicture';
import { styles } from './styles';


interface COMMENT_INTERFACE {
    likeCount?: number | any;
    isLiked?: boolean;
    replyCount?: number | any;

    body?: string | any;
    timeago?: string | any;

    user?: {
        name: any,
        profile: any,
        username: any,
    };
}


const index = ({ obj = {} }: any) => {
    const item: COMMENT_INTERFACE = obj;
    const likeCount = "likeCount" in item ? item.likeCount : 0;
    const isLiked = "isLiked" in item ? item.isLiked : false;

    const replyCount = "replyCount" in item ? item.replyCount : 0;


    const body = "body" in item ? item.body : '';
    const timeago = "timeago" in item ? item.timeago : '';

    const user = "user" in item ? item.user : {
        name: '',
        profile: '',
        username: '',
    };


    return (
        <View style={styles.primaryContainer}>
            <View style={styles.cardPrimaryContainer}>
                <ProfilePicture
                    source={user?.profile}
                    size={40} />
                <View style={styles.container}>

                    {/* ******************** USER Start ******************** */}
                    <View style={styles.usernameContainer}>

                        <Text style={styles.username}>{`${printText(user?.name)}`}</Text>

                        <TouchableOpacity style={styles.optionIconContainer}>
                            <VectorIcon
                                name={"options-vertical"}
                                iconType={"SimpleLineIcons"}
                                color={"#1E1E1E"}
                                size={15} />
                        </TouchableOpacity>
                    </View>

                    {/* ******************** USER End ******************** */}

                    {/* ******************** BODY Start ******************** */}
                    <PostText
                        //@ts-ignore
                        style={styles.bodyText}>{printText(body)}</PostText>

                    {/* ******************** BODY End ******************** */}

                    {/* ******************** TIME Start ******************** */}
                    {emptyValidate(timeago) && <Text style={styles.time}>{HelperFunctions.timeSince(timeago)}</Text>}

                    {/* ******************** TIME End ******************** */}
                </View>
            </View>

            <View style={styles.bottomContainer}>
                <TouchableOpacity>
                    <Text style={[styles.bottomText, {
                        color: isLiked ? colors.primary : "#898A8D",
                    }]}>{IMLocalized(`Like`)}</Text>
                </TouchableOpacity>

                {likeCount !== 0 &&
                    <Text style={styles.bottomText}>{`. ${likeCount} ${IMLocalized(`likes`)}`}</Text>
                }

                <View style={styles.bottomSeperator} />

                <TouchableOpacity>
                    <Text style={styles.bottomText}>{IMLocalized(`Reply`)}</Text>
                </TouchableOpacity>

                {replyCount !== 0 &&
                    <Text style={styles.bottomText}>{`. ${replyCount} ${IMLocalized(`reply`)}`}</Text>
                }

            </View>

        </View>
    )
}//end of index

export default index;
