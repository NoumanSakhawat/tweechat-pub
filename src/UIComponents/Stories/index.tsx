import React, { Component, useEffect, useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import Text from '../../components/Text';
import VectorIcon from '../../components/VectorIcon';
import colors from '../../constants/colors';
import LocalAssets from '../../constants/LocalAssets';
import StaticData from '../../constants/StaticData';
import HelperFunctions from '../../helper/HelperFunctions';
import { IMLocalized } from '../../locales/IMLocalization';
import { itemStyles, styles } from './styles';
//END OF IMPORT's

const index = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        const myStory = {
            id: HelperFunctions.guid(),
            name: 'Your Story',
            profile: { uri: LocalAssets.randomProfilePicture() },
            isMyStory: true,
        }

        const data: any = [...[myStory], ...StaticData.stories()];
        setData(data);
    }, []);

    return (
        <View style={styles.primaryContainer}>
            <Text style={styles.heading}>{IMLocalized(`Stories`)}</Text>

            <FlatList
                data={data}
                renderItem={_renderItem}
                horizontal
                contentContainerStyle={styles.contentContainerStyle}
                nestedScrollEnabled={true}
                showsHorizontalScrollIndicator={false}
            />

        </View>
    )
}//end of INDEX

const _renderItem = ({ item, index }: any) => {

    return (
        <TouchableOpacity
            style={itemStyles.primaryContainer}>
            {item.isMyStory ? _renderMyStory(item.profile) : _renderImage(item.profile)}

            <Text style={itemStyles.name}>{item.name.length < 11
                ? `${item.name}`
                : `${item.name.substring(0, 6)}...`}</Text>

        </TouchableOpacity>
    )
}//end of renderItem

const _renderImage = (path: any) => {
    return (
        <View style={itemStyles.imageContainer}>
            <FastImage
                style={itemStyles.image}
                source={{
                    ...path,
                    priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.contain}
            />
        </View>
    )
}//end of _renderImage

const _renderMyStory = (path: any) => {
    return (
        <View>
            <FastImage
                style={itemStyles.storyImage}
                source={{
                    ...path,
                    priority: FastImage.priority.high,
                }}
                resizeMode={FastImage.resizeMode.contain}
            />
            <View style={itemStyles.addIconContainer}>
                <VectorIcon
                    name={"add"}
                    color={colors.white}
                    size={20} />
            </View>
        </View>
    )
}//end of _renderMyStory

export default index;
