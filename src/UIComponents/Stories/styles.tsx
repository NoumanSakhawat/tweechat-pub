import { StyleSheet } from 'react-native';
import AppStyles from '../../constants/AppStyles';
import colors from '../../constants/colors';

const classColor = {
    heading: "#0F1117",
    name: "#606268",
}

export const styles = StyleSheet.create({
    primaryContainer: {
        backgroundColor: colors.background,
        marginTop: 16,
    },
    heading: {
        color: classColor.heading,
        fontSize: 16,
        marginLeft: AppStyles.HORIZONTAL,
    },
    contentContainerStyle: {
        marginLeft: AppStyles.HORIZONTAL,
        paddingRight: AppStyles.HORIZONTAL,
        paddingBottom: 8,
    },

});//end of styles

const IMAGE_SIZE = 60;
const BORDER_DIFF = 8;

export const itemStyles = StyleSheet.create({
    primaryContainer: {
        marginRight: 12,
        marginTop: 12,
        marginBottom: 8,
    },

    imageContainer: {
        width: IMAGE_SIZE,
        height: IMAGE_SIZE,
        borderRadius: IMAGE_SIZE,
        borderColor: colors.primary,
        borderWidth: 2,
        alignItems: "center",
        justifyContent: "center",
    },
    image: {
        width: IMAGE_SIZE - BORDER_DIFF,
        height: IMAGE_SIZE - BORDER_DIFF,
        borderRadius: IMAGE_SIZE - BORDER_DIFF,
    },
    storyImage: {
        width: IMAGE_SIZE,
        height: IMAGE_SIZE,
        borderRadius: IMAGE_SIZE,
    },
    addIconContainer: {
        position: "absolute",
        bottom: 0,
        left: IMAGE_SIZE - 18,
        backgroundColor: colors.primary,
        borderRadius: 20
    },
    name: {
        color: classColor.name,
        fontSize: 12,
        marginTop: 9,
        textAlign: "center",
    },
});//end of itemStyles