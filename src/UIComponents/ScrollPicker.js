import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button,
} from "react-native";

import { WheelPicker } from "react-native-wheel-picker-android";


const Days = [...Array(8).keys()].map(String)
const Hours = [...Array(24).keys()].map(String)
const Minutes = [...Array(60).keys()].map(String)

const ScrollPicker = ({ closeBottomSheet, setPollLength }) => {

    const [selectedDay, setSelectedDay] = useState(1);
    const [selectedHour, setSelectedHour] = useState(0);
    const [selectedMinute, setSelectedMinute] = useState(0);

    const onDaySelected = selectedItem => {
        setSelectedDay(selectedItem);
    }

    const onHourSelected = selectedItem => {
        setSelectedHour(selectedItem);
    }

    const onMinuteSelected = selectedItem => {
        setSelectedMinute(selectedItem);
    }

    useEffect(() => {
        if (selectedDay == 7) {
            setSelectedHour(0);
            setSelectedMinute(0);
        }
        if (selectedDay == 0 && selectedHour == 0 && selectedMinute <= 4) {
            setSelectedMinute(5)
        }
    }, [selectedDay, selectedHour, selectedMinute])

    return (
        <>
            <Text style={[styles.heading, { fontSize: 16 }]}>Poll length</Text>
            <View style={styles.wheelPickersContainer}>
                <View style={styles.wheelPickerContainer}>
                    <Text style={styles.heading}>Days</Text>
                    <WheelPicker
                        style={{ width: 100, height: 100 }}
                        selectedItem={selectedDay}
                        data={Days}
                        onItemSelected={onDaySelected}
                    />
                </View>
                <View style={styles.wheelPickerContainer}>
                    <Text style={styles.heading}>Hours</Text>
                    <WheelPicker
                        style={{ width: 100, height: 100 }}
                        selectedItem={selectedHour}
                        data={Hours}
                        onItemSelected={onHourSelected}
                    />
                </View>
                <View style={styles.wheelPickerContainer}>
                    <Text style={styles.heading}>Minutes</Text>
                    <WheelPicker
                        style={{ width: 100, height: 100 }}
                        selectedItem={selectedMinute}
                        data={Minutes}
                        onItemSelected={onMinuteSelected}
                    />
                </View>
            </View>

            <Button
                title={'Confirm'}
                onPress={() => {
                    setPollLength({
                        "Days": selectedDay,
                        "Hours": selectedHour,
                        "Minutes": selectedMinute
                    })
                    closeBottomSheet();
                }}
            />
        </>
    )
};
export default ScrollPicker;

const styles = StyleSheet.create({
    wheelPickersContainer: {
        // flex: 1,
        minHeight: 300,
        backgroundColor: '#fff',
        justifyContent: 'center',
        flexDirection: 'row',
        // alignItems: 'center'
    },
    wheelPickerContainer: {
        marginHorizontal: 8,
        alignItems: 'center'
    },
    heading: {
        fontWeight: 'bold',
        marginBottom: 8,
        marginTop: 8,
        textAlign: 'center',
    },
    buttonContainer: {
        marginTop: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
})