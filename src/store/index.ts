import { createStore, combineReducers } from "redux";

const rootReducer = combineReducers({

})//end of rootReducer

const configureStore = () => createStore(rootReducer);

export default configureStore;