/**
 * @format
 */

import React from "react";
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import 'react-native-gesture-handler';
// import { Provider } from 'react-redux'

// import configureStore from './src/store'

// const store = configureStore()

const reduxSetup = () => {
    return (
        // <Provider store={store}>
        <App />
        // </Provider>
    )
}//end of reduxSetup

AppRegistry.registerComponent(appName, () => reduxSetup);
